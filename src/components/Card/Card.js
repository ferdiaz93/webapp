import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import CSS from 'utils/css/mainCSS';
import colors from 'utils/css/colors';

import LockCircled from 'components/Icons/LockCircled';

import { COMMUNITY_PRIVACIES } from '../../constants';

const Box = styled.div`
  color: ${colors.blackRed};
  background-color: white;
  border-radius: 16px;
  box-shadow: ${CSS.boxShadow};
  display: flex;
  flex-direction: column;
  position: relative;

  a {
    display: flex;
    align-items: center;
  }
`;

const BoxContent = styled.div`
  padding: 0 32px 32px;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const Content = styled.div`
  flex-grow: 1;
`;

const Image = styled.img.attrs(props => ({
  src: props.src,
  alt: `avatar de la comunidad ${props.alt}`,
}))`
  width: 96px;
  height: 96px;
  margin: -48px auto 24px;
  border-radius: 4px;
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.16);
  flex-shrink: 0;
  align-self: center;

  ${props => props.type === 'rounded' && `
    border-radius: 100%;
  `}
`;

const Title = styled.h2`
  font-size: 18px;
  margin: 0 0 16px;
  a {
    color: ${colors.blackRed};
    text-decoration: none;
  }
`;

const Description = styled.span`
  color: ${colors.blackRed};
  font-size: 16px;
  margin: 0 0 24px;
  display: block;
`;

const Actions = styled.div`
  margin-top: auto;
`;

const LockWrapper = styled.div`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 12px;
  right: 12px;
`;

const ConditionalLink = ({ to, children }) => {
  if (to) return <Link to={to}>{children}</Link>;
  return children;
};

ConditionalLink.propTypes = {
  to: PropTypes.string,
  children: PropTypes.node.isRequired,
};

ConditionalLink.defaultProps = {
  to: null,
};

const Card = ({
  title,
  description,
  avatar,
  privacy,
  action,
  link,
}) => (
  <Box>
    {privacy === COMMUNITY_PRIVACIES.PRIVATE && (
      <LockWrapper>
        <LockCircled />
      </LockWrapper>
    )}
    <ConditionalLink to={link}>
      <Image {...avatar} />
    </ConditionalLink>
    <BoxContent>
      <ConditionalLink to={link}>
        <Content>
          <Title>{title}</Title>
          <Description>{description}</Description>
        </Content>
      </ConditionalLink>
      {action && <Actions>{action}</Actions>}
    </BoxContent>
  </Box>
);

Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.node.isRequired,
  avatar: PropTypes.shape({
    src: PropTypes.string,
    alt: PropTypes.string,
    type: PropTypes.string,
  }).isRequired,
  privacy: PropTypes.string,
  action: PropTypes.node,
  link: PropTypes.string,
};

Card.defaultProps = {
  privacy: COMMUNITY_PRIVACIES.PUBLIC,
  link: null,
  action: null,
};

export default Card;
