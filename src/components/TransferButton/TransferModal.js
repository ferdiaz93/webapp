import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as channelSelectors from 'state/channels/selectors';
import * as bankSelectors from 'state/bank/selectors';
import * as appActions from 'state/app/actions';
import * as bankActions from 'state/bank/actions';

import Modal from 'components/Modal';
import Input from 'components/Forms/Input';
import Button from 'components/Button';

import locales from './i18n';
import { BANK_BOX_TYPES } from '../../constants';

const Container = styled.div`
  text-align: center;

  .header {
    font-weight: 600;
    font-size: 32px;
  }

  .amount {
    width: 200px;
    margin: 16px auto 0;

    .fixed-amount {
      font-size: 48px;
      font-weight: 600;
    }

    input {
      font-size: 32px;
      font-weight: 600;
      text-align: center;
      margin: 0;
    }
  }

  .max-min {
    margin-top: 8px;
    font-size: 14px;
    color: ${props => props.theme.colors.secondaryText};
  }

  table {
    text-align: left;
    width: auto;
    margin: 16px auto 32px;
    font-size: 14px;

    tr {
      &:last-child {
        background-color: ${props => props.theme.colors.mainLight};
      }

      td {
        &:first-child {
          padding-right: 32px;
        }

        border-bottom: 1px dotted #AAA;
        padding: 8px;
      }
    }
  }
`;
Container.displayName = 'TransferModalContainer';

const TransferModal = ({
  to, data, close, afterTransfer,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  let selector = userSelectors.selectDisplayName;
  if (to.type === BANK_BOX_TYPES.BOT) selector = channelSelectors.getBotName;

  const displayname = useSelector(state => selector(state, to.id));
  const tax = useSelector(bankSelectors.selectGeneralTax);

  const [transfering, setTransfering] = useState(false);
  const [amount, setAmount] = useState(data.amount);
  const amountChange = useCallback((e) => {
    setAmount(e.target.value);
  }, []);

  const disabled = (data.min && amount < data.min) || (data.max && amount > data.max);

  const transfer = useCallback(async () => {
    try {
      setTransfering(true);

      const payload = { to, amount: parseFloat(amount), data: data.transferData };
      await bankActions.createTransaction(payload);

      if (afterTransfer) await afterTransfer();

      dispatch(appActions.addToast(t('Transaction successful')));
      close();
    } catch (error) {
      setTransfering(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, t, amount, data, to, close, afterTransfer]);

  const actions = [];
  if (!transfering) {
    actions.push();
  }

  const hasMinimumAmount = typeof data.min === 'number';
  const hasMaximumAmount = typeof data.max === 'number';

  return (
    <Modal
      title={t('Transfer Sades to {{displayname}}', { displayname })}
      onCancel={close}
      actions={[
        <Button key="chatmessage-transfer-confirm" loading={transfering} onClick={transfer} disabled={disabled}>{t('global:Confirm')}</Button>,
      ]}
    >
      <Container>
        <div className="header">{t('Amount')}</div>

        <div className="amount">
          {data.fixed
            ? <div className="fixed-amount">{data.amount}</div>
            : <Input type="number" value={amount} onChange={amountChange} min={data.min} max={data.max} />
          }
        </div>

        {(hasMinimumAmount || hasMaximumAmount) && (
          <div className="max-min">
            {`${
              hasMinimumAmount ? `${t('Minimum')}: ${data.min}` : ''
            }${hasMinimumAmount && hasMaximumAmount ? ' - ' : ''}${
              hasMaximumAmount ? `${t('Maximum')}: ${data.max}` : ''
            }`}
          </div>
        )}


        <table>
          <tbody>
            <tr>
              <td>{t('Destinatary will receive')}</td>
              <td>{`§ ${amount}`}</td>
            </tr>
            <tr>
              <td>{t('Taxes ({{tax}}%)', { tax })}</td>
              <td>{`§ ${(amount * (tax / 100)).toFixed(2)}`}</td>
            </tr>
            <tr>
              <td>{t('Total to debit')}</td>
              <td><strong>{`§ ${(amount * ((tax / 100) + 1)).toFixed(2)}`}</strong></td>
            </tr>
          </tbody>
        </table>
      </Container>
    </Modal>
  );
};

TransferModal.propTypes = {
  data: PropTypes.shape({
    fixed: PropTypes.bool.isRequired,
    amount: PropTypes.number.isRequired,
    min: PropTypes.number,
    max: PropTypes.number,
    transferData: PropTypes.shape({}),
  }),
  to: PropTypes.shape({
    type: PropTypes.oneOf(Object.values(BANK_BOX_TYPES)).isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  }).isRequired,
  close: PropTypes.func.isRequired,
  afterTransfer: PropTypes.func,
};

TransferModal.defaultProps = {
  data: {
    fixed: false,
    amount: 10,
  },
  afterTransfer: null,
};

export default TransferModal;
