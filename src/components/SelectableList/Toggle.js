import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import colors from 'utils/css/colors';

import { ChevronRight, ChevronDown } from 'components/Icons';

const Wrapper = styled.div`
  color: ${colors.grey};
  text-align: center;
  padding: 10px 0;
  margin-bottom: 20px;
  cursor: pointer;

  svg {
    width: 16px;
    height: 16px;
  }
`;

const Toggle = ({ title, expanded, onClick }) => (
  <Wrapper onClick={onClick}>
    {expanded ? <ChevronDown color={colors.grey} /> : <ChevronRight color={colors.grey} />}
    {title}
  </Wrapper>
);

Toggle.propTypes = {
  title: PropTypes.string.isRequired,
  expanded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Toggle;
