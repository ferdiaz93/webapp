import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';
import * as channelActions from 'state/channels/actions';

import Busy from 'components/UserDisplayName/Busy';

const DisplayName = styled.span.attrs({
  className: 'displayname',
})`
  position: relative;
`;
DisplayName.displayName = 'DisplayName';

const botEqual = (prevBot, nextBot) => (
  prevBot.id === nextBot.id
  && prevBot.avatar === nextBot.avatar
  && prevBot.loading === nextBot.loading
);

const BotDisplayName = ({ botId }) => {
  const dispatch = useDispatch();

  const bot = useSelector(state => channelSelectors.getBotById(state, botId), botEqual);
  const isLoaded = !!bot;

  useEffect(() => {
    if (!isLoaded && botId) {
      dispatch(channelActions.fetchBotData(botId));
    }
  }, [isLoaded, dispatch, botId]);

  if (!isLoaded) return <Busy className="displayname" />;

  return (
    <DisplayName>
      <span>{bot.name}</span>
    </DisplayName>
  );
};

BotDisplayName.propTypes = {
  botId: PropTypes.string.isRequired,
};

BotDisplayName.defaultProps = {
};

export default BotDisplayName;
