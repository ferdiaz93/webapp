import React from 'react';
import PropTypes from 'prop-types';

import max from './max.png';
import hush from './hush.png';
import lush2 from './lush2.png';
import lush3 from './lush3.png';

const LovenseToy = ({ name }) => {
  if (name === 'max') return <img src={max} alt="Lovense Max" title="Lovense Max" width={16} heigth={16} />;
  if (name === 'hush') return <img src={hush} alt="Lovense Max" title="Lovense Max" width={16} heigth={16} />;
  if (name === 'lush3') return <img src={lush3} alt="Lovense Max" title="Lovense Max" width={16} heigth={16} />;
  return <img src={lush2} alt="Lovense Toy" title="Lovense Toy" width={16} heigth={16} />;
};

LovenseToy.propTypes = {
  name: PropTypes.string.isRequired,
};

export default LovenseToy;
