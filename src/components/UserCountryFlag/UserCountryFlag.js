import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

const UserCountryFlag = ({ userId }) => {
  const user = useSelector(userSelectors.getById(userId), shallowEqual);

  if (!user || !user.country || !user.country.isoCode) return null;
  return (
    <img
      className="country-flag"
      src={`https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${user.country.isoCode.toLowerCase()}.png`}
      alt={user.country.name}
      title={user.country.name}
    />
  );
};

UserCountryFlag.propTypes = {
  userId: PropTypes.number.isRequired,
};

UserCountryFlag.defaultProps = {
};

export default UserCountryFlag;
