import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Img } from 'components/Icons';

const Button = styled.button`
  background: 0;
  width: 24px;
  height: 24px;
  padding: 0;
  margin: 0 12px 0 0;
  border: 0;
  cursor: pointer;
`;

const UploadMediaButton = ({ open, className }) => (
  <Button onClick={open} className={className} type="button"><Img color="#6F6666" /></Button>
);

UploadMediaButton.propTypes = {
  open: PropTypes.func.isRequired,
  className: PropTypes.string,
};

UploadMediaButton.defaultProps = {
  className: '',
};

export default UploadMediaButton;
