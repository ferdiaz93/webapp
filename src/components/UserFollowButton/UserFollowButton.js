import React, { useState, useCallback, useContext } from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeContext } from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import { AccountPlus } from 'components/Icons';

import UsersListsModal from './UsersListsModal';
import locales from './i18n';

const UserFollowButton = ({ userId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();
  const theme = useContext(ThemeContext);

  const myUserId = useSelector(authSelectors.selectUserId);
  const following = useSelector(state => authSelectors.selectAmIFollowing(state, userId));
  const inLists = useSelector(state => authSelectors.selectInListsIds(state, userId), shallowEqual);

  const [loading, setLoading] = useState(false);
  const [showingListsModal, setShowingListsModal] = useState(false);

  const openListsModal = useCallback((e) => {
    e.stopPropagation();
    e.preventDefault();
    setShowingListsModal(true);
  }, []);
  const closeListsModal = useCallback(() => { setShowingListsModal(false); }, []);

  const onClick = useCallback(async () => {
    try {
      if (!myUserId) {
        dispatch(appActions.setMiniprofile(null));
        history.push('/login');
      } else {
        setLoading(true);
        if (following) {
          await dispatch(authActions.unfollow(userId));
        } else {
          await dispatch(authActions.follow(userId));
        }
      }
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setLoading(false);
  }, [dispatch, userId, following, myUserId, history]);

  const saveFromLists = useCallback(async (lists) => {
    try {
      closeListsModal();
      setLoading(true);
      await dispatch(authActions.listsChange(userId, lists));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setLoading(false);
  }, [dispatch, closeListsModal, userId]);

  if (!userId || userId === myUserId) return null;

  return (
    <>
      <Button
        icon={<AccountPlus color={following ? 'black' : 'white'} />}
        onClick={onClick}
        onMoreOptions={myUserId ? openListsModal : undefined}
        color={following ? 'white' : theme.colors.main}
        fontColor={following ? 'black' : 'white'}
        loading={loading}
      >
        {following ? t('global:Following') : t('global:Follow')}
      </Button>

      {showingListsModal && (
        <UsersListsModal
          close={closeListsModal}
          initialLists={inLists}
          onConfirm={saveFromLists}
        />
      )}
    </>
  );
};

UserFollowButton.propTypes = {
  userId: PropTypes.number.isRequired,
};

UserFollowButton.defaultProps = {
};

export default UserFollowButton;
