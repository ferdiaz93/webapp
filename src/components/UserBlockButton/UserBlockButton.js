import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import { AccountCancel } from 'components/Icons';

import locales from './i18n';

const ButtonComponent = styled(Button)`
  margin-bottom: 16px;

  @media(min-width: 767px) {
    margin-bottom: 0;
  }
`;
ButtonComponent.displayName = 'ButtonComponent';

const UserBlockButton = ({ userId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const myUserId = useSelector(authSelectors.selectUserId);
  const blocked = useSelector(state => authSelectors.selectIsBlocked(state, userId));

  const [loading, setLoading] = useState(false);

  const onClick = useCallback(async () => {
    try {
      setLoading(true);
      if (blocked) {
        await dispatch(authActions.unblock(userId));
      } else {
        await dispatch(authActions.block(userId));
      }
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setLoading(false);
  }, [dispatch, userId, blocked]);

  if (!userId || userId === myUserId) return null;

  return (
    <>
      <ButtonComponent
        icon={<AccountCancel color="black" />}
        onClick={onClick}
        color="white"
        fontColor="black"
        loading={loading}
      >
        {blocked ? t('global:Unblock') : t('global:Block')}
      </ButtonComponent>
    </>
  );
};

UserBlockButton.propTypes = {
  userId: PropTypes.number.isRequired,
};

UserBlockButton.defaultProps = {
};

export default UserBlockButton;
