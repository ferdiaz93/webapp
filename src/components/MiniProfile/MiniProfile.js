import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import ismobile from 'ismobilejs';

import * as messengerSelectors from 'state/messengers/selectors';
import * as messengerActions from 'state/messengers/actions';
import * as authSelectors from 'state/auth/selectors';

import { useTranslation } from 'hooks';
import UserType from 'state/users/type';

import GenderAge from 'containers/Profiles/Profile/GenderAge';
import UserTags from 'containers/Profiles/Profile/UserTags';
import Badges from 'containers/Profiles/Profile/Badges';
import profileLocales from 'containers/Profiles/i18n';
import Modal from 'components/Modal';
import UserAvatar from 'components/UserAvatar';
import UserFollowButton from 'components/UserFollowButton';
import Button from 'components/Button';
import Loading from 'components/Loading';
import EmptyState from 'components/EmptyState';

import Counter from './Counter';
import locales from './i18n';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  margin-top: 45px;

  .genderage {
    > span {
      margin: 0 4px;

      svg {
        width: 16px;
        height: 16px;
      }
    }
  }

  .usertags {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;

    > div {
      margin-top: 2px;
    }
  }

  .badges {
    display: flex;
    align-self: center;
    color: white;
    margin-top: 4px;
    font-size: 14px;

    svg {
      width: 48px;
      height: 48px;
    }
  }

  @media(max-height: 640px) {
    margin-top: 0px;

    .location {
      font-size: 14px;
    }

    .basic-data {
      display: flex;
      width: 100%;
      align-items: center;
      justify-content: center;
      margin-bottom: 16px;
      cursor: pointer;

      .avatar {
        position: relative !important;
        width: auto !important;
        height: auto !important;
        top: auto !important;
        left: auto !important;

        img {
          width: 64px;
          height: 64px;
        }
      }

      .user-name {
        margin-left: 8px;
      }
    }

    .counters {
      margin-bottom: 0;
    }
  }
`;

const Displayname = styled.div`
  font-weight: bold;
  font-size: 24px;
`;

const Username = styled.div`
  color: #666;
  margin-bottom: 16px;
`;

const Location = styled.div.attrs({
  className: 'location',
})`
  font-size: 18px;
  margin-bottom: 8px;

  img {
    height: 16px;
    margin-right: 8px;
  }
`;

const Description = styled.div`
  color: #333;

  ${props => props.secondary && `
    color: #999;
    font-size: 15px;
  `}
`;

const Counters = styled.div.attrs({
  className: 'counters',
})`
  margin: 28px 0;
  display: flex;
  justify-content: space-around;
`;

const avatarStyle = {
  position: 'absolute',
  top: '-75px',
  alignSelf: 'center',
  left: '50%',
  transform: 'translateX(-50%)',
};

const MiniProfile = ({ user, close, history }) => {
  const { t } = useTranslation(locales);
  const { t: tP } = useTranslation(profileLocales);

  const dispatch = useDispatch();
  const messengerId = useSelector(state => messengerSelectors.selectByUserId(state, user.id));
  const meId = useSelector(authSelectors.selectId);

  const openProfile = () => {
    history.push(`/@${user.username}`);
    close();
  };

  const openDirectMessage = async () => {
    if (!meId) {
      history.push('/login');
    } else {
      if (messengerId) history.push(`/chat/messengers/${messengerId}`);

      const messenger = await dispatch(messengerActions.create([user.id]));
      history.push(`/chat/messengers/${messenger.id}`);
    }
    close();
  };

  const actions = [];

  if (!user.suspended && !user.blocked && !user.banned) {
    if (meId) {
      actions.push(
        <Button key="miniprofile-gotoprofile" onClick={openProfile} className="empty">{t('View Profile')}</Button>,
      );
    }

    if (!meId || (!user.isOrganization && meId !== user.id)) {
      actions.push(
        <Button key="miniprofile-sendpm" onClick={openDirectMessage} className="empty">{t('Private Message')}</Button>,
      );
    }

    if (!meId || meId !== user.id) {
      actions.push(
        <UserFollowButton key="miniprofile-follow" userId={user.id} />,
      );
    }
  }


  const location = [];
  if (user.city) location.push(user.city.name);
  if (user.region) location.push(user.region.name);
  if (user.country) location.push(user.country.name);

  let content = null;

  if (user.loading) {
    content = <Loading />;
  } else if (user.banned) {
    content = (
      <EmptyState
        uiLeftColumn
        title={tP('This account has been banned')}
        subtitle={tP('The account has been terminated automatically by the system due to multiple user\'s reports of missconduct or violations to terms of service.')}
      />
    );
  } else if (user.suspended) {
    content = (
      <EmptyState
        uiLeftColumn
        title={tP('This account has been suspended')}
        subtitle={tP('The owner of this account has decided to suspend it')}
      />
    );
  } else if (user.blocked) {
    content = (
      <EmptyState
        title={tP('You can\'t view this profile')}
        subtitle={tP('This person has blocked you')}
      />
    );
  } else {
    content = (
      <>
        {!user.isOrganization && (
          <>
            <Location>
              {user.country && user.country.isoCode && <img src={`https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${user.country.isoCode.toLowerCase()}.png`} alt={user.country.name} />}
              {location.join(', ')}
            </Location>

            <UserTags userId={user.id} dark showUpToAmount={ismobile.phone ? 5 : undefined} />

            <GenderAge userId={user.id} />
            <Description secondary>
              {t('Pronoun')}
              {': '}
              {t(`global:PRONOUN.${user.pronoun}`)}
            </Description>
          </>
        )}

        <div className="badges">
          <Badges userId={user.id} />
        </div>

        <Counters>
          {!user.isOrganization && <Counter label={t('global:Known by')} number={user.knowedCount} />}
          {!user.isOrganization && <Counter label={t('global:Events')} number={user.eventCount} />}
          <Counter label={t('global:Followers')} number={user.followersCount} />
          <Counter label={t('global:Following')} number={user.followingCount} />
        </Counters>
      </>
    );
  }

  return (
    <Modal
      onClose={close}
      boxStyle={{ overflow: 'inherit' }}
      actions={actions}
      onlyActions
    >
      <Wrapper>
        <>
          <div className="basic-data">
            <UserAvatar userId={user.id} size="150px" style={avatarStyle} showOnline={false} />
            <div className="user-name">
              <Displayname>{user.displayname}</Displayname>
              <Username>{`@${user.username}`}</Username>
            </div>
          </div>
          {content}
        </>
      </Wrapper>
    </Modal>
  );
};

MiniProfile.propTypes = {
  user: UserType.isRequired,
  close: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

MiniProfile.defaultProps = {
};

export default withRouter(MiniProfile);
