import React, { useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Information from 'components/Icons/Information';
import BoxDivider from 'components/BoxDivider';

import zones from './zones.json';
import BoxWrapper from './BoxWrapper';
import Info from './Info';
import { GUEST_MAX_THREADS } from '../../constants';

const Ad = ({ id, divider }) => {
  const dispatch = useDispatch();
  const elementRef = useRef(null);

  const userIsLoggedIn = useSelector(authSelectors.loggedIn);
  const userHasEmailConfirmed = useSelector(authSelectors.selectIsEmailConfirmed);
  const niches = useSelector(authSelectors.selectNiches, shallowEqual);

  const piece = zones[id];

  const onInfoClick = useCallback(() => {
    dispatch(appActions.openAdInformation());
  }, [dispatch]);

  if (!piece) return null;

  let usertype = 'Unlogg';
  if (userIsLoggedIn) usertype = 'UnConf';
  if (userHasEmailConfirmed) usertype = 'BasicR';
  if (
    !userIsLoggedIn
    && parseInt(Cookies.get('threadsRead'), 10) >= GUEST_MAX_THREADS
    && !/bot|crawler|spider|crawling/i.test(navigator.userAgent)
  ) usertype = 'Lurker';

  const code = piece.code
    .replace(/INSERT_RANDOM_NUMBER_HERE/g, Math.floor(Math.random() * 1000) + 1)
    .replace(/R_USERTYPE/g, usertype)
    .replace(/R_NICHES/g, niches.join(','));

  return (
    <BoxWrapper
      topDivider={divider === 'top'}
      ref={elementRef}
      width={piece.width}
      height={piece.height}
    >
      {divider === 'top' && <BoxDivider top />}

      {/* eslint-disable-next-line react/no-danger */}
      <div dangerouslySetInnerHTML={{ __html: code }} />

      <Info onClick={onInfoClick}>
        Ad
        <Information color="#666" />
      </Info>

      {divider === 'bottom' && <BoxDivider />}
    </BoxWrapper>
  );
};

Ad.propTypes = {
  id: PropTypes.string.isRequired,
  divider: PropTypes.oneOf(['bottom', 'top']),
  data: PropTypes.shape({
    index: PropTypes.number,
  }),
};

Ad.defaultProps = {
  divider: 'bottom',
  data: {},
};

const isSameAd = (prevState, nextState) => {
  if (prevState.id !== nextState.id) return false;
  if (prevState.data && nextState.data && prevState.data.index !== nextState.data.index) {
    return false;
  }
  return true;
};

export default React.memo(Ad, isSameAd);
