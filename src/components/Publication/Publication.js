import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as feedSelectors from 'state/feed/selectors';
import * as userSelectors from 'state/users/selectors';

import {
  Container,
  Actions,
  CommentContainer,
  Footer,
} from './UI';
import Header from './Header';
import MoreContent from './MoreContent';
import Content from './Content';
import Lovense from './Lovense';
import AddComment from './AddComment';
import Comments from './Comments';
import Likers from './Likers';
import Share from './ActionButtons/Share';
import Votes from './ActionButtons/Votes';
import Spank from './ActionButtons/Spank';
import Comment from './ActionButtons/Comment';
import Collected from './ActionButtons/Collected';

const Publication = ({ publicationId, full }) => {
  const authorId = useSelector(
    state => feedSelectors.publications.selectAuthorId(state, publicationId),
  );
  const isBlocked = useSelector(userSelectors.isBlockingMe(authorId));

  const content = useMemo(() => (
    <Container>
      <Header publicationId={publicationId} />

      <MoreContent publicationId={publicationId} />
      <Content publicationId={publicationId} full={full} />

      <Lovense publicationId={publicationId} />

      <Footer>
        <Likers publicationId={publicationId} />
        <Actions>
          <Spank publicationId={publicationId} />
          <Comment publicationId={publicationId} />
          <Votes publicationId={publicationId} />
          <Collected publicationId={publicationId} />
          <Share publicationId={publicationId} />
        </Actions>
      </Footer>
      <CommentContainer>
        <Comments publicationId={publicationId} full={full} />
        <AddComment publicationId={publicationId} />
      </CommentContainer>
    </Container>
  ), [publicationId, full]);

  return isBlocked ? null : content;
};

Publication.propTypes = {
  publicationId: PropTypes.string.isRequired,
  full: PropTypes.bool,
};

Publication.defaultProps = {
  full: false,
};

export default Publication;
