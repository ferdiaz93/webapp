import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import colors from 'utils/css/colors';

import * as feedSelectors from 'state/feed/selectors';

import {
  Earth,
  Contacts,
  AxisArrow,
} from 'components/Icons';

import { PUBLICATION_PRIVACIES } from '../../../constants';

const PrivacyIcon = ({ publicationId }) => {
  const privacy = useSelector(
    state => feedSelectors.publications.selectPrivacy(state, publicationId),
  );

  if (!privacy) return null;

  switch (privacy) {
    case PUBLICATION_PRIVACIES.CONTACTS:
      return <Contacts color={colors.redReactions} title={`global:${privacy}`} />;
    case PUBLICATION_PRIVACIES.LISTS:
      return <AxisArrow color={colors.redReactions} title={`global:${privacy}`} />;
    case PUBLICATION_PRIVACIES.PUBLIC:
    default:
      return <Earth color={colors.redReactions} title={`global:${privacy}`} />;
  }
};

PrivacyIcon.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

PrivacyIcon.defaultProps = {
};

export default PrivacyIcon;
