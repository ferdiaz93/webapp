import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';
import * as feedActions from 'state/feed/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';

import locales from './i18n';

const CloseComments = ({ publicationId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [toggling, setToggling] = useState(false);
  const closed = useSelector(
    state => feedSelectors.publications.commentsBlocked(state, publicationId),
  );

  const toggle = useCallback(async () => {
    try {
      setToggling(true);
      const toast = closed ? 'Comments allowed' : 'Comments disallowed';
      await dispatch(feedActions.toggleBlockedComments(publicationId));
      dispatch(appActions.addToast(t(toast)));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setToggling(false);
  }, [publicationId, dispatch, t, closed]);

  return (
    <Toggle label={t('Allow comments')} active={!closed} onChange={toggle} loading={toggling} />
  );
};

CloseComments.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

CloseComments.defaultProps = {
};

export default CloseComments;
