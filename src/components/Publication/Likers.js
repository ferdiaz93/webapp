import React from 'react';
import PropTypes from 'prop-types';
import { shallowEqual, useSelector } from 'react-redux';

import * as feedSelectors from 'state/feed/selectors';
import * as userSelectors from 'state/users/selectors';
import * as authSelectors from 'state/auth/selectors';

import Busy from 'components/UserDisplayName/Busy';
import SpankListModal from 'components/SpankListModal';

import { useOpenClose, useTranslation } from 'hooks';

import { Likers as Wrapper } from './UI';
import locales from './i18n';

const Likers = ({ publicationId }) => {
  const { t } = useTranslation(locales);

  const [showingSpanksModal, showSpanksModal, hideSpanksModal] = useOpenClose();

  const likers = useSelector(
    state => feedSelectors.publications.selectReactionsList(state, publicationId),
    shallowEqual,
  );

  const followingUsers = useSelector(authSelectors.selectFollowingUsers, shallowEqual);

  const sortedLikersByFollowing = likers.sort((likerA, likerB) => {
    if (followingUsers.includes(likerA)) {
      if (followingUsers.includes(likerB)) {
        return 0;
      }
      return -1;
    }

    if (followingUsers.includes(likerB)) {
      return 1;
    }

    return 0;
  });


  const [firstUser, secondUser] = useSelector(
    userSelectors.getListByIds([sortedLikersByFollowing[0], sortedLikersByFollowing[1]]),
  );

  (firstUser || {}).isLoaded = useSelector(userSelectors.isLoaded(likers[0]));
  (secondUser || {}).isLoaded = useSelector(userSelectors.isLoaded(likers[1]));

  if (!likers || !likers.length) return null;

  let content = null;

  if (likers.length === 1) {
    content = (
      <>
        {firstUser.isLoaded ? firstUser.displayname : <Busy />}
        {' '}
        {t('spanked this', { count: likers.length })}
      </>
    );
  } else if (likers.length === 2) {
    content = (
      <>
        {firstUser.isLoaded ? firstUser.displayname : <Busy />}
        {' '}
        {t('and')}
        {' '}
        {secondUser && secondUser.isLoaded ? secondUser.displayname : <Busy />}
        {' '}
        {t('spanked this', { count: likers.length })}
      </>
    );
  } else if (likers.length > 2) {
    content = (
      <>
        {firstUser.isLoaded ? firstUser.displayname : <Busy />}
        {' '}
        {t('more spanked this', { countLeft: likers.length - 1 })}
      </>
    );
  }


  return content && (
    <>
      <Wrapper onClick={showSpanksModal}>
        {content}
      </Wrapper>
      {showingSpanksModal && <SpankListModal type="publication" entityId={publicationId} close={hideSpanksModal} />}
    </>
  );
};

Likers.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Likers.defaultProps = {
};

export default Likers;
