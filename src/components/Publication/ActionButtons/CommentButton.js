import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import colors from 'utils/css/colors';
import * as feedSelectors from 'state/feed/selectors';

import { Comment } from 'components/Icons';

const CommentButton = ({ publicationId, outline }) => {
  const commentIds = useSelector(
    state => feedSelectors.selectCommentIds(state, publicationId),
    shallowEqual,
  );

  return (
    <span>
      <Comment color={colors.red} outline={outline} />
      {commentIds.length > 0 && <span>{commentIds.length}</span>}
    </span>
  );
};

CommentButton.propTypes = {
  publicationId: PropTypes.string.isRequired,
  outline: PropTypes.bool.isRequired,
};

CommentButton.defaultProps = {
};

export default CommentButton;
