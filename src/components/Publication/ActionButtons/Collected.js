import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import { AlphaSCircle } from 'components/Icons';
import ActionIcon from 'components/ActionIcon';
import Hoverable from 'components/Hoverable';

import { ActionWrapper } from '../UI';
import FundersModal from './FundersModal';
import locales from '../i18n';

const Button = ({ outline, collected }) => {
  const theme = useContext(ThemeContext);
  return (
    <span>
      <AlphaSCircle color={theme.colors.main} outline={outline} />
      {/* (collected % 1) returns 0 if it's an integer or a number > 0 if it's float */}
      {/* based on that we round or not */}
      {collected > 0 && <span>{(collected % 1) ? collected.toFixed(2) : collected}</span>}
    </span>
  );
};

Button.propTypes = {
  outline: PropTypes.bool.isRequired,
  collected: PropTypes.number.isRequired,
};

const Collected = ({ publicationId }) => {
  const { t } = useTranslation(locales);

  const hasFunding = useSelector(
    state => feedSelectors.publications.hasFunding(state, publicationId),
  );
  const isFundedByMe = useSelector(
    state => feedSelectors.publications.isFundedByMe(state, publicationId),
  );
  const collected = useSelector(
    state => feedSelectors.publications.getFundingCollected(state, publicationId),
  );

  const [isModalOpen, openModal, closeModal] = useOpenClose();

  if (!hasFunding) return null;

  return (
    <>
      <ActionWrapper>
        <ActionIcon onClick={openModal}>
          <Hoverable
            normal={<Button outline={!isFundedByMe} collected={collected} />}
            hover={<Button outline={false} collected={collected} />}
            tooltip={t('Sades collected')}
          />
        </ActionIcon>
      </ActionWrapper>

      {/* Modals */}
      {isModalOpen && (
        <FundersModal publicationId={publicationId} onClose={closeModal} />
      )}
    </>
  );
};

Collected.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Collected.defaultProps = {
};

export default Collected;
