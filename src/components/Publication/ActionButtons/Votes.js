import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import * as feedSelectors from 'state/feed/selectors';

import ActionIcon from 'components/ActionIcon';
import PollBox from 'components/Icons/PollBox';

import { ActionWrapper } from '../UI';

const Votes = ({ publicationId }) => {
  const theme = useContext(ThemeContext);

  const poll = useSelector(
    state => feedSelectors.publications.selectPoll(state, publicationId),
    shallowEqual,
  );

  if (!poll) return null;

  return (
    <ActionWrapper>
      <ActionIcon>
        <PollBox outline={!poll.showResults} color={theme.colors.main} />
        <span>{poll.voteCount}</span>
      </ActionIcon>
    </ActionWrapper>
  );
};

Votes.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Votes.defaultProps = {
};

export default Votes;
