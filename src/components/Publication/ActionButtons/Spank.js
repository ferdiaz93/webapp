import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';
import * as feedActions from 'state/feed/actions';
import * as appActions from 'state/app/actions';

import ActionIcon from 'components/ActionIcon';
import Hoverable from 'components/Hoverable';

import SpankButton from './SpankButton';
import { ActionWrapper } from '../UI';
import locales from '../i18n';

const Spank = ({ publicationId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const userHasSpanked = useSelector(
    state => feedSelectors.publications.userHasSpanked(state, publicationId),
  );

  const [spanking, setSpanking] = useState(false);
  const [unspanking, setUnspanking] = useState(false);
  const spanked = !unspanking && (spanking || userHasSpanked);

  const spankClick = useCallback(async () => {
    try {
      if (spanked) {
        setUnspanking(true);
        await dispatch(feedActions.deletePublicationReaction(publicationId));
        setUnspanking(false);
      } else {
        setSpanking(true);
        await dispatch(feedActions.createPublicationReaction(publicationId));
        setSpanking(false);
      }
    } catch (error) {
      setSpanking(false);
      setUnspanking(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, spanked, publicationId]);

  return (
    <ActionWrapper>
      <ActionIcon onClick={spankClick} animate>
        <Hoverable
          normal={<SpankButton publicationId={publicationId} outline={!spanked} />}
          hover={<SpankButton publicationId={publicationId} outline={spanked} />}
          tooltip={t(spanked ? 'global:Unspank' : 'global:Spank')}
        />
      </ActionIcon>
    </ActionWrapper>
  );
};

Spank.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Spank.defaultProps = {
};

export default Spank;
