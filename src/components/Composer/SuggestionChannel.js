import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';

import ChannelAvatar from 'components/ChannelAvatar';

const Wrapper = styled.div`
  display: flex;
  padding: 8px;

  > div:last-child {
    flex: 1;
    margin-left: 8px;

    .displayname {
      color: ${props => props.theme.colors.main};
      font-weight: 500;
      font-size: 14px;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const SuggestionChannel = ({ channel }) => {
  const avatar = useSelector(state => channelSelectors.getAvatar(state, channel.id));

  return (
    <Wrapper>
      <div><ChannelAvatar image={avatar} size="32px" /></div>
      <div>
        <div className="displayname">{channel.name}</div>
      </div>
    </Wrapper>
  );
};

SuggestionChannel.propTypes = {
  channel: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

SuggestionChannel.defaultProps = {
};

export default SuggestionChannel;
