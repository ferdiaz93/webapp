import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import CommunityAvatar from 'components/CommunityAvatar';

const Wrapper = styled.div`
  display: flex;
  padding: 8px;

  > div:last-child {
    flex: 1;
    margin-left: 8px;

    .displayname {
      color: ${props => props.theme.colors.main};
      font-weight: 500;
      font-size: 14px;
    }

    .slug {
      color: ${props => props.theme.colors.secondary};
      font-size: 12px;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const SuggestionCommunity = ({ community }) => (
  <Wrapper>
    <div><CommunityAvatar communityId={community.id} size="32px" /></div>
    <div>
      <div className="displayname">{community.name}</div>
      <div className="slug">{`+${community.slug.toLowerCase()}`}</div>
    </div>
  </Wrapper>
);

SuggestionCommunity.propTypes = {
  community: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
  }).isRequired,
};

SuggestionCommunity.defaultProps = {
};

export default SuggestionCommunity;
