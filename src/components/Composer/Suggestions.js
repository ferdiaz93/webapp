import React, { useLayoutEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import fastdom from 'fastdom';

const Wrapper = styled.div`
  position: absolute;
  background: white;
  z-index: 1000;
  box-shadow: 0px 0px 4px 0px #b9b9b9;

  ${props => (props.position.vertical === 'bottom' ? `
    top: ${props.coords.top + props.lineHeight + props.gap + props.paddingTop}px;
    ` : `
    bottom: ${props.dimensions.height + props.coords.top + props.gap}px;
  `)}

  ${props => (!props.position.forcedRight ? `
    left: ${props.coords.left + props.paddingLeft}px;
  ` : `
    right: 0;
  `)}

  @media(max-width: 768px) {
    max-height: calc(100vh - 180px);
  }
`;
Wrapper.displayName = 'Wrapper';

const Suggestion = styled.div`
  padding: 4px;

  ${props => props.selected && `
    background-color: ${props.theme.colors.mainLight};
  `}

  &:hover {
    cursor: pointer;
    background-color: ${props => props.theme.colors.actionListItemBG};
  }

  > span.emoji {
    display: flex;
    align-items: center;

    > span:first-child {
      margin-right: 4px;
    }
  }
`;
Suggestion.displayName = 'Suggestion';

const Suggestions = ({
  data, onSelect, selectedIndex, coords, lineHeight, dimensions, gap,
}) => {
  const el = useRef(null);
  const [position, setPosition] = useState({ vertical: 'top', forcedRight: false });

  useLayoutEffect(() => {
    if (el.current && dimensions) {
      let forcedRight = false;
      let vertical = 'bottom';

      fastdom.measure(() => {
        if (el.current && (dimensions.width < (el.current.offsetWidth + coords.left))) {
          forcedRight = true;
        }

        const top = (dimensions.element.getBoundingClientRect().top + coords.top);
        const middle = (document.body.offsetHeight / 2);
        if (top > middle) {
          vertical = 'top';
        }

        setPosition({ vertical, forcedRight });
      });
    }
  }, [el, dimensions, coords]);

  const paddingLeft = parseInt(window.getComputedStyle(dimensions.element, null).getPropertyValue('padding-left').replace('px', ''), 10);
  const paddingTop = parseInt(window.getComputedStyle(dimensions.element, null).getPropertyValue('padding-left').replace('px', ''), 10);

  return (
    <Wrapper
      ref={el}
      coords={coords}
      dimensions={dimensions}
      lineHeight={lineHeight}
      position={position}
      paddingLeft={paddingLeft}
      paddingTop={paddingTop}
      gap={gap}
    >
      {data.map((suggestion, index) => (
        <Suggestion
          key={`suggestion-${suggestion.text}`}
          onClick={onSelect(suggestion)}
          selected={index === selectedIndex}
        >
          {suggestion.suggest}
        </Suggestion>
      ))}
    </Wrapper>
  );
};

Suggestions.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    suggest: PropTypes.node,
    text: PropTypes.string,
    mark: PropTypes.node,
    selectedIndex: PropTypes.number,
  })).isRequired,
  onSelect: PropTypes.func.isRequired,
  selectedIndex: PropTypes.number,
  coords: PropTypes.shape({
    left: PropTypes.number.isRequired,
    right: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    bottom: PropTypes.number.isRequired,
  }).isRequired,
  lineHeight: PropTypes.number,
  dimensions: PropTypes.shape({
    left: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    clientWidth: PropTypes.number.isRequired,
    clientHeight: PropTypes.number.isRequired,
    element: PropTypes.shape({
      getBoundingClientRect: PropTypes.func,
    }).isRequired,
  }),
  gap: PropTypes.number,
};

Suggestions.defaultProps = {
  selectedIndex: null,
  lineHeight: 16,
  dimensions: null,
  gap: 8,
};

export default Suggestions;
