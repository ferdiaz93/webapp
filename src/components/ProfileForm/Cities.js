import React, {
  useCallback, useState, useEffect, useMemo,
} from 'react';
import PropTypes from 'prop-types';

import Api from 'state/api';
import { useTranslation } from 'hooks';

import Select from 'components/Select';
import Spinner from 'components/Spinner';

import locales from './i18n';

const Cities = ({
  value, onChange, countryId, regionId, label,
}) => {
  const { t } = useTranslation(locales);

  const [cities, setCities] = useState(null);

  useEffect(() => {
    const load = async () => {
      try {
        setCities(null);
        const { data } = await Api.req.get(`/users/geo/countries/${countryId}/regions/${regionId}/cities`);
        setCities(data);
      } catch (error) {
        if (error.response && error.response.status === 404) {
          setCities([]);
        }
      }
    };

    if (countryId && regionId) {
      load();
    }
  }, [countryId, regionId]);

  const setCity = useCallback((v) => {
    onChange(v.value);
  }, [onChange]);

  const options = useMemo(() => (
    (cities || []).map(c => ({
      label: c.name,
      value: c.id,
    }))
  ), [cities]);

  if (!countryId || (cities && !cities.length)) return null;
  if (cities === null) return <Spinner color="#A8A8A8" />;

  return (
    <>
      {label && <h3>{t('City')}</h3>}
      <Select
        value={options.find(c => c.value === value)}
        onChange={setCity}
        placeholder={t('City')}
        options={options}
      />
    </>
  );
};

Cities.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  countryId: PropTypes.string.isRequired,
  regionId: PropTypes.string.isRequired,
  label: PropTypes.bool,
};

Cities.defaultProps = {
  label: false,
};

export default Cities;
