import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import logo from './logo.svg';

const css = `
body { background-color: #ff3b30; }
body > div,
body > div > div,
body > div > div > div.login-form {
  height: 100%;
}
`;

const Grid = styled.div`
  text-align: center;
  height: 100%;
  vertical-align: middle;
`;

const Column = styled.div`
  max-width: 450px;
`;

const Header = styled.div`
  margin: 10px;
`;

const LoginForm = ({ children }) => (
  <div className="login-form">
    <style>{css}</style>
    <Grid>
      <Column>
        <Header>
          <img src={logo} alt="Logo" />
        </Header>
        {children}
      </Column>
    </Grid>
  </div>
);

LoginForm.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

export default LoginForm;
