import React, { useState, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as bankSelectors from 'state/bank/selectors';
import * as appActions from 'state/app/actions';
import * as bankActions from 'state/bank/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from './i18n';

const Container = styled.div`
  text-align: center;

  .header {
    font-weight: 600;
    font-size: 32px;
  }

  .description {
    margin: 16px auto;

    .fixed-description {
      font-size: 48px;
      font-weight: 600;
    }

    input {
      font-size: 32px;
      font-weight: 600;
      text-align: center;
    }
  }

  .items-summary-title {
    font-weight: 600;
  }

  .items-summary {
    text-align: left;
    width: auto;
    margin: auto;
    margin-bottom: 32px;

    tr {
      &:last-child {
        background-color: ${props => props.theme.colors.mainLight};
      }

      td {
        &:first-child {
          padding-right: 32px;
        }

        border-bottom: 1px dotted #AAA;
        padding: 8px;
      }
    }
  }
`;
Container.displayName = 'PurchaseModalContainer';

const PurchaseModal = ({
  itemName: mainItemName, close, afterPurchase, payloadData, otherItems,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const itemsPrices = useSelector(
    state => bankSelectors.selectBuyableItemsPriceList(
      state,
      [mainItemName, ...otherItems],
    ),
  );

  const [purchasing, setPurchasing] = useState(false);

  const transfer = useCallback(async () => {
    try {
      setPurchasing(true);

      // eslint-disable-next-line no-restricted-syntax
      for (const item of otherItems) {
        if (item.length) {
          // eslint-disable-next-line no-await-in-loop
          await bankActions.createPurchase({ item });
        }
      }

      const payload = { item: mainItemName, payload: payloadData };
      const transaction = await bankActions.createPurchase(payload);

      if (afterPurchase) afterPurchase(transaction);

      dispatch(appActions.addToast(t('Purchase successful')));
      close();
    } catch (error) {
      setPurchasing(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, t, mainItemName, close, afterPurchase, payloadData, otherItems]);

  const itemsSummary = useMemo(() => {
    const summary = {
      [mainItemName]: { amount: 1, unitPrice: itemsPrices[mainItemName] },
    };
    otherItems.forEach((itemName) => {
      if (summary[itemName]) {
        summary[itemName].amount += 1;
      } else {
        summary[itemName] = { amount: 1, unitPrice: itemsPrices[itemName] };
      }
    });
    return summary;
  }, [otherItems, mainItemName, itemsPrices]);

  const totalPrice = useMemo(() => Object.values(itemsSummary).reduce(
    (acc, item) => (acc + item.amount * item.unitPrice),
    0,
  ),
  [itemsSummary]);

  return (
    <Modal
      title={t('Purchase')}
      onCancel={close}
      actions={[
        <Button key="purchase-confirm" loading={purchasing} onClick={transfer} disabled={!itemsPrices[mainItemName]}>{t('Purchase')}</Button>,
      ]}
    >
      <Container>
        <div className="header">{t(`${mainItemName}.name`)}</div>

        <div className="description">{t(`${mainItemName}.description`)}</div>

        <div className="items-summary-title">Esta compra incluye:</div>
        <table className="items-summary">
          <thead>
            <tr>
              <td><strong>Producto</strong></td>
              <td><strong>Cant.</strong></td>
              <td><strong>Precio</strong></td>
            </tr>
          </thead>
          <tbody>
            {Object.keys(itemsSummary).map(itemName => (
              <tr key={itemName}>
                <td>{t(`${itemName}.name`)}</td>
                <td>{itemsSummary[itemName].amount}</td>
                <td>
                  <strong>
                    §
                    {' '}
                    {itemsSummary[itemName].unitPrice}
                  </strong>
                </td>
              </tr>
            ))}
            <tr>
              <td colSpan={2}>{t('Final Value')}</td>
              <td>
                <strong>
                  §
                  {' '}
                  {totalPrice}
                </strong>
              </td>
            </tr>
          </tbody>
        </table>
      </Container>
    </Modal>
  );
};

PurchaseModal.propTypes = {
  itemName: PropTypes.string.isRequired,
  otherItems: PropTypes.arrayOf(PropTypes.string),
  close: PropTypes.func.isRequired,
  afterPurchase: PropTypes.func,
  payloadData: PropTypes.shape({}),
};

PurchaseModal.defaultProps = {
  afterPurchase: null,
  payloadData: {},
  otherItems: [],
};

export default PurchaseModal;
