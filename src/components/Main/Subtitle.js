import PropTypes from 'prop-types';
import styled from 'styled-components';

export const Subtitle = styled.h1`
  color: ${props => props.theme.colors.secondaryText};
  font-size: 16px;
  margin: 0;
  font-weight: normal;
`;

Subtitle.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Subtitle;
