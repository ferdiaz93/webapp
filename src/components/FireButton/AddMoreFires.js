import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Modal from 'components/Modal';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as bankSelectors from 'state/bank/selectors';

import PurchaseButton from 'components/PurchaseButton';
import { Fire } from 'components/Icons';

import locales from './i18n';

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;

  > div {
    text-align: center;

    .title {
      font-weight: bold;
      font-size: 20px;
    }

    .cost {
      color: ${props => props.theme.colors.secondary};
      margin-bottom: 8px;
    }

    svg {
      width: 128px;
      height: 128px;

      path {
        fill: ${props => props.theme.colors.main};
      }
    }

    &:nth-child(1) {
      svg {
        opacity: .25;
      }
    }

    &:nth-child(2) {
      svg {
        opacity: .5;
      }
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const Column = ({ fires, close }) => {
  const { t } = useTranslation(locales);

  const itemName = `FIRE_IGNITES_${fires}`;
  const price = useSelector(state => bankSelectors.selectBuyableItemPrice(state, itemName));

  return (
    <div>
      <div className="title">{t('{{fires}} fires', { fires })}</div>
      <Fire />
      <div className="cost">{t('{{sades}} sades', { sades: price })}</div>
      <PurchaseButton itemName={itemName} afterPurchase={close} />
    </div>
  );
};

Column.propTypes = {
  fires: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
};

const AddMoreFires = ({ close }) => (
  <Modal onCancel={close}>
    <Wrapper>
      <Column fires={3} close={close} />
      <Column fires={10} close={close} />
      <Column fires={20} close={close} />
    </Wrapper>
  </Modal>
);

AddMoreFires.propTypes = {
  close: PropTypes.func.isRequired,
};

AddMoreFires.defaultProps = {
};

export default AddMoreFires;
