import React, { useState, useCallback, useContext } from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeContext } from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as authSelectors from 'state/auth/selectors';
import * as userActions from 'state/users/actions';
import * as appActions from 'state/app/actions';

import { Action } from 'components/Header';
import { Fire as FireIcon } from 'components/Icons';
import Modal from 'components/Modal';
import Button from 'components/Button';
import PurchaseButton from 'components/PurchaseButton';

import AddMoreFires from './AddMoreFires';
import locales from './i18n';

const IgniteWrapper = styled.div`
  display: flex;

  .column {
    flex: 1;
    text-align: center;
    margin: 0 8px;

    ul {
      text-align: left;
      font-size: 14px;
      list-style: none;
      margin: 0 16px 32px;
      min-height: 180px;
    }

    .requests-left {
      color: ${props => props.theme.colors.secondary};
      margin-bottom: 8px;
    }

    button {
      margin: 4px 0;
    }
  }
`;
IgniteWrapper.displayName = 'IgniteWrapper';

const FireButton = ({ userId, color }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);

  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const haveFire = useSelector(state => userSelectors.haveFire(state, userId));
  const displayname = useSelector(userSelectors.getDisplayName(userId));
  const gender = useSelector(userSelectors.getGender(userId));
  const availableFires = useSelector(authSelectors.getFiresLeft);
  const availableSuperFires = useSelector(authSelectors.getSuperFiresLeft);

  const [isAddMoreModalOpen, openAddMoreModal, closeAddMoreModal] = useOpenClose();
  const [isIgniteModalOpen, openIgniteModal, closeIgniteModal] = useOpenClose();
  const [igniting, setIgniting] = useState(false);
  const [superIgniting, setSuperIgniting] = useState(false);
  const [extinguishing, setExtinguishing] = useState(false);

  const ignite = useCallback(async () => {
    try {
      setIgniting(true);
      await dispatch(userActions.igniteFire(userId, false));
      dispatch(appActions.addToast(t('Fire ignited')));
      setIgniting(false);
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    closeIgniteModal();
  }, [dispatch, t, userId, closeIgniteModal]);

  const superIgnite = useCallback(async () => {
    try {
      setSuperIgniting(true);
      await dispatch(userActions.igniteFire(userId, true));
      dispatch(appActions.addToast(t('Super Fire ignited')));
      setSuperIgniting(false);
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    closeIgniteModal();
  }, [dispatch, t, userId, closeIgniteModal]);

  const extinguish = useCallback(async () => {
    try {
      setExtinguishing(true);
      await dispatch(userActions.extinguishFire(userId));
      dispatch(appActions.addToast(t('Fire extinguished')));
      setExtinguishing(false);
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    closeIgniteModal();
  }, [dispatch, t, userId, closeIgniteModal]);

  if (isOrganization || haveFire === null) return null;

  if (haveFire) {
    return (
      <>
        <Action onClick={openIgniteModal}>
          <FireIcon color={theme.colors.main} />
        </Action>

        {/* Modals */}
        {isIgniteModalOpen && (
          <Modal
            title={t('Extinguish Fire')}
            onCancel={closeIgniteModal}
            actions={[
              <Button key="extinguish-fire" onClick={extinguish} loading={extinguishing}>{t('global:Confirm')}</Button>,
            ]}
          >
            {t('Are you sure you want to extinguish fire with this user?')}
          </Modal>
        )}
      </>
    );
  }

  return (
    <>
      <Action onClick={openIgniteModal}>
        <FireIcon color={color} />
      </Action>

      {/* Modals */}
      {isIgniteModalOpen && (
        <Modal
          onCancel={closeIgniteModal}
        >
          <IgniteWrapper>
            <div className="column">
              <h3>{t('Fire')}</h3>
              <ul>
                <li>{t('Fire means you want to have something with this person')}</li>
                <li>{t('Your ignition is anonyomous')}</li>
                <li>{t('{{displayname}} will be notified that someone ignited the fire', { displayname, context: gender })}</li>
                <li>{t('Your user will be revealed only if the fire is mutual')}</li>
                <li>{t('If fire is mutual, you will be able to start a conversation')}</li>
              </ul>
              <div className="requests-left">{t('Available {{left}}', { left: availableFires })}</div>

              <Button disabled={availableFires === 0 || superIgniting} onClick={ignite} loading={igniting}>{t('Ignite Fire')}</Button>
              <Button className="empty" onClick={openAddMoreModal}>{t('Add more')}</Button>
            </div>

            <div className="column">
              <h3>{t('Super Fire')}</h3>
              <ul>
                <li>{t('Fire means you want to have something with this person')}</li>
                <li>{t('{{displayname}} will be immediately notified you like them', { displayname, context: gender })}</li>
                <li>{t('If fire is mutual, you will be able to start a conversation')}</li>
                <li>{t('Your user will be revealed')}</li>
              </ul>
              <div className="requests-left">{t('Available {{left}}', { left: availableSuperFires })}</div>

              <Button disabled={availableSuperFires === 0 || igniting} onClick={superIgnite} loading={superIgniting}>{t('Ignite Super Fire')}</Button>
              <PurchaseButton
                component={<Button className="empty">{t('Add more')}</Button>}
                itemName="SUPERFIRE"
              />
            </div>
          </IgniteWrapper>
        </Modal>
      )}

      {isAddMoreModalOpen && <AddMoreFires close={closeAddMoreModal} />}
    </>
  );
};

FireButton.propTypes = {
  userId: PropTypes.number.isRequired,
  color: PropTypes.string,
};

FireButton.defaultProps = {
  color: 'white',
};

export default FireButton;
