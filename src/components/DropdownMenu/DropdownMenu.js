import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import colors from 'utils/css/colors';

const Wrapper = styled.div`
  background-color: white;
  width: ${props => props.width};
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.08);
  max-height: ${props => (props.maxHeight ? props.maxHeight : '292px')};
  overflow: auto;
  text-transform: none;
  font-weight: normal;

  @media(max-width: 767px) {
    width: 100%;
  }

  > div:not(.nav-dd-content) {
    color: black;
    line-height: 1.2;
    min-height: 48px;
    display: flex;
    align-items: center;
    padding: 0 8px;
    cursor: pointer;

    &:hover {
      color: ${colors.red};
    }
  }
`;

const DropdownMenu = ({ children, width, maxHeight }) => (
  <Wrapper width={width} maxHeight={maxHeight}>
    {children}
  </Wrapper>
);

DropdownMenu.propTypes = {
  children: PropTypes.node.isRequired,
  width: PropTypes.string.isRequired,
  maxHeight: PropTypes.string,
};

DropdownMenu.defaultProps = {
  maxHeight: null,
};

export default DropdownMenu;
