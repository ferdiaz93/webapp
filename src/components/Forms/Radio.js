import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import colors from 'utils/css/colors';

const Selector = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 32px;
  margin-bottom: 32px;
  position: relative;
  cursor: pointer;
  &:before {
    content: '';
    background: white;
    width: 16px;
    height: 16px;
    border-radius: 100%;
    border: 1px solid ${colors.grey};
    position: absolute;
    top: 4px;
    left: 0;
    transition: all 250ms ease-out;
  }
`;

const SelectorChecked = styled(Selector)`
  &:before {
    background: ${colors.red};
    border: 1px solid ${colors.red};
  }
`;

const Label = styled.p`
  position: relative;
  font-weight: bold;
  margin: 0 0 12px;

  ${props => props.disabled && `
    color: #999;
  `}
`;

const Description = styled.p`
  margin: 0;
  color: ${props => (props.disabled ? '#aaa' : colors.redReactions)}
`;


const Radio = ({
  label,
  selected,
  description,
  onSelect,
  disabled,
}) => {
  const SelectorComponent = selected ? SelectorChecked : Selector;

  return (
    <SelectorComponent onClick={evnt => !disabled && onSelect(evnt)}>
      <Label disabled={disabled}>{label}</Label>
      <Description disabled={disabled}>{description}</Description>
    </SelectorComponent>
  );
};

Radio.propTypes = {
  label: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  description: PropTypes.string,
  onSelect: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Radio.defaultProps = {
  disabled: false,
  description: null,
};

export default Radio;
