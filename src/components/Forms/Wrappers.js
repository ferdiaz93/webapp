import styled from 'styled-components';

export const FormWrapper = styled.form`
  background-color: white;
  width: 100%;
  padding: 20px;
  border-radius: 16px;
  box-sizing: border-box;
  margin-top: 16px;

  @media(min-width: 992px) {
    padding: 48px;
  }
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
