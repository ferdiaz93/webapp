import React, { useLayoutEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import colors from 'utils/css/colors';
import * as appActions from 'state/app/actions';

import MobileMenuButton from 'components/MobileMenuButton';

const Container = styled.div`
  margin: 16px;
  display: flex;
  flex-direction: column;
  max-height: calc(100vh - 64px);

  ${props => props.full && `
    height: 100%;
  `}

  ${props => (props.height && props.height > 150) && `
    height: ${props.height}px !important;
  `}
`;

const Wrapper = styled.div`
  text-align: center;
  flex: 1;
  display: flex;

  div {
    align-self: center;
    width: 100%;
  }
`;

const Title = styled.div`
  font-size: 32px;
  font-weight: 200;
  margin-bottom: 8px;
  margin-top: 16px;
`;

const Subtitle = styled.div`
  color: ${colors.redReactions};
  margin-bottom: 64px;
`;

const Content = styled.div``;

const EmptyState = ({
  title,
  subtitle,
  children,
  onMenuClick,
  uiLeftColumn,
  full,
}) => {
  const dispatch = useDispatch();
  const height = useRef(null);
  const element = useRef(null);

  useLayoutEffect(() => {
    if (uiLeftColumn) {
      dispatch(appActions.uiLeftColumn(true));
    }

    if (element.current) {
      height.current = document.body.offsetHeight - element.current.offsetTop - 64 - 20;
    }
  }, [uiLeftColumn, dispatch]);

  return (
    <Container className="emptystate" full={full} ref={element} height={height.current}>
      {onMenuClick && <MobileMenuButton onClick={onMenuClick} />}
      <Wrapper>
        <div>
          <Title>{title}</Title>
          <Subtitle>{subtitle}</Subtitle>
          {children && <Content>{children}</Content>}
        </div>
      </Wrapper>
    </Container>
  );
};

EmptyState.propTypes = {
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  subtitle: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  onMenuClick: PropTypes.func,
  children: PropTypes.node,
  uiLeftColumn: PropTypes.bool,
  full: PropTypes.bool,
};

EmptyState.defaultProps = {
  title: '',
  subtitle: '',
  onMenuClick: null,
  children: null,
  uiLeftColumn: false,
  full: false,
};

export default EmptyState;
