import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const MediaItemWrapper = styled.li`
  position: relative;
  cursor: pointer;

  ${props => props.threshold && `
    max-width: 33%;
    padding: 8px;
    box-sizing: border-box;
  `}

  @media(max-width: 767px) {
    width: 100%;
    margin-bottom: 20px;
  }
  &:not(:last-child) {
    @media(min-width: 768px) {
      margin-right: 12px;
    }
  }
  img {
    max-width: 100%;
    height: auto;
    object-fit: contain;
    display: inline-block;
    vertical-align: middle;
  }
`;

const MediaItem = ({ image, onClick, threshold }) => (
  <MediaItemWrapper onClick={onClick} threshold={threshold}>
    <img src={image.filename || image} alt="" />
  </MediaItemWrapper>
);

MediaItem.propTypes = {
  image: PropTypes.oneOfType([
    PropTypes.shape({
      filename: PropTypes.string.isRequired,
    }),
    PropTypes.string,
  ]).isRequired,
  onClick: PropTypes.func,
  threshold: PropTypes.bool,
};

MediaItem.defaultProps = {
  onClick: null,
  threshold: false,
};

export default MediaItem;
