import styled from 'styled-components';

const Title = styled.div`
  text-align: center;
  margin-bottom: 16px;
`;
Title.displayName = 'Title';

export default Title;
