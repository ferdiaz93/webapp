import styled from 'styled-components';

const UserTag = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.9);
  background: rgba(255, 255, 255, 0.2);
  border-radius: 4px;
  padding: 4px 8px;
  margin: 0 8px 10px;
  white-space: nowrap;

  ${props => props.dark && `
    border: 1px solid rgba(0, 0, 0, 0.9);
    background: rgba(0, 0, 0, 0.1);
  `}

  ${props => props.highlighted && `
    border: 2px solid ${props.theme.colors.main};
  `}

  ${props => props.onClick && `
    cursor: pointer;
    transition: all 250ms ease-out;

    ${!props.selected && `
      opacity: .4;
    `}

    &:hover {
      opacity: 1;
    }
  `}
`;

UserTag.displayName = 'UserTag';

export default UserTag;
