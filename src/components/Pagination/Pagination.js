import React, { useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { ChevronLeft, ChevronRight } from 'components/Icons';

const PaginationWrapper = styled.div`
  display: flex;
  margin: 8px auto;
  justify-content: center;
`;
PaginationWrapper.displayName = 'PaginationWrapper';

const Step = styled.div`
  width: 32px;
  height: 32px;
  text-align: center;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  margin: 0 4px;

  ${props => !props.disabled && `
    &:hover, &.active {
      box-shadow: 0 0 5px 0px #0000003b;
    }
  `}

  ${props => props.disabled && `
    color: #AAA;
    cursor: auto;

    svg path {
      fill: #AAA;
    }
  `}

  ${props => props.active && `
    box-shadow: 0 0 5px 0px #0000003b;
    background-color: ${props.theme.colors.main};
    color: white;
    cursor: auto;
  `}

  svg {
    width: 24px;
    height: 24px;
  }
`;
Step.displayName = 'Step';

const Pagination = ({ pages, current, onChange }) => {
  const steps = useMemo(() => {
    const res = [];
    for (let i = current - 3; i <= current + 3; i += 1) {
      if (i > 0 && i <= pages) res.push(i);
    }
    return res;
  }, [pages, current]);

  const onClick = useCallback(page => () => {
    if (page !== current && page > 0 && page <= pages) onChange(page);
  }, [onChange, current, pages]);

  return (
    <PaginationWrapper>
      <Step onClick={onClick(current - 1)} disabled={current === 1}><ChevronLeft /></Step>
      {steps.map(step => <Step key={`pag-step-${step}`} onClick={onClick(step)} active={step === current}>{step}</Step>)}
      <Step onClick={onClick(current + 1)} disabled={current === pages}><ChevronRight /></Step>
    </PaginationWrapper>
  );
};

Pagination.propTypes = {
  pages: PropTypes.number.isRequired,
  current: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

Pagination.defaultProps = {
};

export default Pagination;
