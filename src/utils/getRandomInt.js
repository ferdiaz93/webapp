const getRandomInt = (imin, imax) => {
  const min = Math.ceil(imin);
  const max = Math.floor(imax);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export default getRandomInt;
