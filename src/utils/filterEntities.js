const filterEntities = (entities, filter) => entities
  .filter(({ name }) => {
    if (!name) return false;
    return name.toLowerCase().includes(filter.toLowerCase());
  });

export default filterEntities;
