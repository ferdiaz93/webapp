import React, { useState, useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import Tour from 'reactour';
import { useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import isMobile from 'utils/isMobile';
import { useTranslation } from 'hooks';
import * as appSelectors from 'state/app/selectors';

import locales from '../../i18n';

const SearchTour = ({ showingSearch }) => {
  const { t } = useTranslation(locales);

  const isUiLeftColumnActive = useSelector(appSelectors.selectIsUiLeftColumnActive);

  const [step, setStep] = useState(0);
  const [open, setOpen] = useState(false);
  const close = () => setOpen(false);

  useEffect(() => {
    if (!isMobile || isUiLeftColumnActive) {
      setOpen(true);
    }
  }, [isUiLeftColumnActive]);

  useEffect(() => {
    if (step === 0 && showingSearch) {
      setTimeout(() => {
        setStep(1);
        localStorage.setItem('tour.threadsearch', 'done');
      }, 500);
    }
  }, [showingSearch, step]);

  useEffect(() => {
    const element = document.querySelector('.search-icon');

    const clickListener = () => { setOpen(false); };

    element.addEventListener('click', clickListener);

    return () => {
      element.removeEventListener('click', clickListener);
    };
  }, []);

  const steps = useMemo(() => [
    {
      selector: '.nav-item-search',
      content: (
        <div>
          <h3>{t('Now you can search by community threads!')}</h3>
          <p>{t('Click on the icon to learn how to use it')}</p>
        </div>
      ),
    },
    {
      selector: '.search-icon',
      content: (
        <div>
          <h3>{t('Click here!')}</h3>
          <p>{t('Select the type of search you want to perform')}</p>
        </div>
      ),
    },
  ], [t]);

  return (
    <Tour
      steps={steps}
      isOpen={open}
      onRequestClose={close}
      disableDotsNavigation
      disableKeyboardNavigation
      showButtons={false}
      goToStep={step}
      accentColor={colors.red}
    />
  );
};

SearchTour.propTypes = {
  showingSearch: PropTypes.bool.isRequired,
};

SearchTour.defaultProps = {
};

export default SearchTour;
