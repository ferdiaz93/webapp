import React from 'react';

import { useSelector } from 'react-redux';

import * as bankSelectors from 'state/bank/selectors';

const CouponNotification = () => {
  const hasCoupons = useSelector(bankSelectors.hasCoupons);

  if (!hasCoupons) return null;
  return <div className="notification" />;
};

CouponNotification.propTypes = {
};

CouponNotification.defaultProps = {
};

export default CouponNotification;
