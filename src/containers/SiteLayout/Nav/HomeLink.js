import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import { Home } from 'components/Icons';
import Badge from 'components/Badge';

import NavItem from './Item';
import locales from '../i18n';

const HomeLink = () => {
  const { t } = useTranslation(locales);

  const queueCount = useSelector(feedSelectors.getFeedQueueCount());

  return (
    <NavItem>
      <Link to="/">
        <Home color="white" />
        <span>{t('Home')}</span>
        {queueCount > 0 && <Badge value={queueCount} />}
      </Link>
    </NavItem>
  );
};

HomeLink.propTypes = {
};

export default HomeLink;
