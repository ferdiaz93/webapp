import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Trans } from 'react-i18next';

import { useTranslation } from 'hooks';

import { Tag } from 'components/Icons';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const TagIcon = styled.div.attrs({
  children: <Tag />,
})`
  background-color: ${props => props.theme.colors.main};
  width: 40px;
  height: 40px;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    width: 24px;
    height: 24px;

    path {
      fill: ${props => props.theme.colors.mainLight};
    }
  }
`;
TagIcon.displayName = 'TagIcon';

const TagExpired = ({ read, tagName }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={<TagIcon />} read={read} to="/user/edit/tags">
      <Trans t={t} ns="alerts" i18nKey="tagExpired" values={{ tagname: t(`global:TAG.${tagName}`) }}>
        Your tag
        {' '}
        <strong>{'{{tagname}}'}</strong>
        {' '}
        in your profile has now expired. Purchase it again.
      </Trans>
    </AlertContainer>
  );
};

TagExpired.propTypes = {
  read: PropTypes.bool.isRequired,
  tagName: PropTypes.string.isRequired,
};

export default TagExpired;
