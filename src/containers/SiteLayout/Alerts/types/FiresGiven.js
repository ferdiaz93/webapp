import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import { Fire } from 'components/Icons';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const FireIcon = styled.div.attrs({
  children: <Fire />,
})`
  background-color: ${props => props.theme.colors.main};
  width: 40px;
  height: 40px;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    width: 24px;
    height: 24px;

    path {
      fill: ${props => props.theme.colors.mainLight};
    }
  }
`;
FireIcon.displayName = 'FireIcon';

const FiresGiven = ({ read, given }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={<FireIcon />} read={read}>
      {t('You now have {{given}} more fires available for use', { given, context: given === 1 && 'SINGULAR' })}
    </AlertContainer>
  );
};

FiresGiven.propTypes = {
  read: PropTypes.bool.isRequired,
  given: PropTypes.number.isRequired,
};

export default FiresGiven;
