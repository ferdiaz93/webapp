import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import UserAvatar from 'components/UserAvatar';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const Superfire = ({ fromUserId, read }) => {
  const { t } = useTranslation(locales);

  const displayname = useSelector(state => userSelectors.selectDisplayName(state, fromUserId));
  const username = useSelector(userSelectors.getUsername(fromUserId));

  return (
    <AlertContainer image={<UserAvatar userId={fromUserId} size="40px" showOnline={false} />} to={`/@${username}`} read={read}>
      {t('{{displayname}} sent you a Super Fire', { displayname })}
    </AlertContainer>
  );
};

Superfire.propTypes = {
  fromUserId: PropTypes.number.isRequired,
  read: PropTypes.bool.isRequired,
};

export default Superfire;
