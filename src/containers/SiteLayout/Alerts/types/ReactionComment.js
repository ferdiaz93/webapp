import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import usersToText from 'utils/usersToText';
import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import UserAvatar from 'components/UserAvatar';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const ReactionComment = ({
  alertId,
  comment,
  authors: authorIds,
  read,
}) => {
  const { t } = useTranslation(locales);

  // Remove duplications (this should be a service feature in the future)
  const idsSet = new Set();
  authorIds.forEach(id => idsSet.add(id));
  const authorsBatch = useSelector(userSelectors.getListByIds([...idsSet]), shallowEqual);

  const authors = Object.values(authorsBatch).reverse();
  const authorsText = usersToText(authors, t, alertId);

  return (
    <AlertContainer image={<UserAvatar userId={authors[0].id} size="40px" showOnline={false} />} to={`/publications/${comment.publication.id}`} read={read}>
      {authorsText}
      {' '}
      {t('spanked you a comment', { context: authors.length > 1 && 'PLURAL' })}
      {': '}
      {comment.rawContent}
    </AlertContainer>
  );
};

ReactionComment.propTypes = {
  alertId: PropTypes.string.isRequired,
  comment: PropTypes.shape({
    id: PropTypes.string.isRequired,
    rawContent: PropTypes.string.isRequired,
    publication: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  authors: PropTypes.arrayOf(PropTypes.number).isRequired,
  read: PropTypes.bool.isRequired,
};

export default ReactionComment;
