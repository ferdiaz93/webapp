import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import UserAvatar from 'components/UserAvatar';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const FireMatched = ({ userId, read }) => {
  const { t } = useTranslation(locales);

  const displayname = useSelector(state => userSelectors.selectDisplayName(state, userId));
  const username = useSelector(userSelectors.getUsername(userId));

  return (
    <AlertContainer image={<UserAvatar userId={userId} size="40px" showOnline={false} />} to={`/@${username}`} read={read}>
      {t('Your have a fire match with {{displayname}}!', { displayname })}
    </AlertContainer>
  );
};

FireMatched.propTypes = {
  userId: PropTypes.number.isRequired,
  read: PropTypes.bool.isRequired,
};

export default FireMatched;
