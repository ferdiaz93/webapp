import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const SadesTransaction = ({ read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer to="/bank" read={read}>
      {t('You have received a new SADEs transaction')}
    </AlertContainer>
  );
};

SadesTransaction.propTypes = {
  read: PropTypes.bool.isRequired,
};

export default SadesTransaction;
