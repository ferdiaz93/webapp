import React from 'react';
import PropTypes from 'prop-types';
import { Trans } from 'react-i18next';

import { useTranslation } from 'hooks';

import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const RelationshipAccepted = ({ userId, read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={<UserAvatar userId={userId} size="40px" showOnline={false} />} to="/user/relationships" read={read}>
      <Trans t={t} ns="alerts" i18nKey="relationship.accepted">
        <strong><UserDisplayName userId={userId} /></strong>
        {' '}
        accepted your relationship
      </Trans>
    </AlertContainer>
  );
};

RelationshipAccepted.propTypes = {
  read: PropTypes.bool.isRequired,
  userId: PropTypes.number.isRequired,
};

export default RelationshipAccepted;
