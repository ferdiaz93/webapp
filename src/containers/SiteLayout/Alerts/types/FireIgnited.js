import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import { Fire } from 'components/Icons';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const ChatIcon = styled.div.attrs({
  children: <Fire />,
})`
  background-color: ${props => props.theme.colors.main};
  width: 40px;
  height: 40px;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    width: 24px;
    height: 24px;

    path {
      fill: ${props => props.theme.colors.mainLight};
    }
  }
`;
ChatIcon.displayName = 'ChatIcon';

const FireIgnited = ({ read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer image={<ChatIcon />} read={read}>
      {t('Someone ignited Fire with you')}
    </AlertContainer>
  );
};

FireIgnited.propTypes = {
  read: PropTypes.bool.isRequired,
};

export default FireIgnited;
