import React, { useEffect, useState } from 'react';
import { Trans } from 'react-i18next';
import { useSelector, shallowEqual } from 'react-redux';

import api from 'state/api';
import { useTitle, useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import Title from 'components/Main/Title';
import Main from 'components/Forms/Main';
import Wrapper from 'components/Forms/Wrapper';
import TitleWrapper from 'components/Forms/TitleWrapper';
import { FormWrapper } from 'components/Forms/Wrappers';
import Loading from 'components/Loading';
import Spinner from 'components/Spinner';
import SuccessAnimation from 'components/PurchaseSadesModal/UI/SuccessAnimation';

import locales from './i18n';

const Lovense = () => {
  const { t } = useTranslation(locales);
  useTitle(t('Connect with Lovense'));

  const me = useSelector(authSelectors.selectMe, shallowEqual);

  const [qrSrc, setQrSrc] = useState(null);

  useEffect(() => {
    const load = async () => {
      const { data } = await api.req.get('/users/connections/lovense/qr');
      setQrSrc(data.message);
    };

    load();
  }, []);

  return (
    <Main hasTitle>
      <Wrapper>
        <TitleWrapper>
          <Title>{t('Connect with Lovense')}</Title>
        </TitleWrapper>

        <FormWrapper>
          <Trans t={t} i18nKey="lovense.details" ns="ConnectionsLovense">
            {/* eslint-disable-next-line max-len */}
            <p>
              If you have any
              {' '}
              <a href="https://lovense.com" target="_blank" rel="noreferrer">Lovense</a>
              {' '}
              toys, you can link them with Mazmo,
              {' '}
              to create interactions between your content and publications and your toys.
            </p>
            <p>
              <strong>Scan the QR code below with your Lovense Remote app.</strong>
              {' '}
              {'Under the tab "Long Distance" tap on the (+) plus sign and then select Scan QR.'}
            </p>
          </Trans>

          <br />
          <br />

          {(!me.connections || !me.connections.lovense || !me.connections.lovense.length) ? (
            <>
              {qrSrc ? (
                <div className="text-center">
                  <img src={qrSrc} alt="Lovense QR" />

                  <br />

                  <div className="hstack">
                    <Spinner color="#666" />
                    <span>{t('Waiting for connection...')}</span>
                  </div>
                </div>
              ) : (
                <Loading />
              )}
            </>
          ) : (
            <div className="text-center">
              <SuccessAnimation message={t('Your toy is connected!')} />
            </div>
          )}
        </FormWrapper>
      </Wrapper>
    </Main>
  );
};

export default Lovense;
