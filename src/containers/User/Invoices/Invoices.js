import React from 'react';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useTitle } from 'hooks';
import * as invoiceSelectors from 'state/invoices/selectors';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';

import InvoiceRow from './InvoiceRow';
import locales from './i18n';

const Wrapper = styled.div`
  table {
    width: 100%;
    font-size: 18px;

    .total {
      font-weight: bold;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const Lists = () => {
  const { t } = useTranslation(locales);
  useTitle(t('Invoices'));

  const list = useSelector(invoiceSelectors.selectInvoices, shallowEqual);

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('Invoices')}</PageTitle>
          <FlexContainer framed>
            <Wrapper>
              <table>
                <tbody>
                  {list.map(invoiceId => (
                    <InvoiceRow invoiceId={invoiceId} />
                  ))}
                </tbody>
              </table>
            </Wrapper>
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Lists.propTypes = {
};

Lists.defaultProps = {
};

export default Lists;
