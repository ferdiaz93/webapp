import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { useTranslation, useInputValue } from 'hooks';
import * as invoiceActions from 'state/invoices/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Input from 'components/Forms/Input';
import Button from 'components/Button';

import locales from './i18n';

const Wrapper = styled.div`
  text-align: center;

  .percentage {
    font-weight: 600;
    font-size: 48px;
  }
`;
Wrapper.displayName = 'Wrapper';

const CouponModal = ({ eventId, close }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const couponCode = useInputValue('');
  const [loading, setLoading] = useState(false);
  const [coupon, setCoupon] = useState(null);

  const redeem = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(invoiceActions.redeemCoupon(eventId, coupon.id));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    close();
  }, [dispatch, close, coupon, eventId]);

  const search = useCallback(async () => {
    try {
      setLoading(true);
      const data = await (dispatch(invoiceActions.getCoupon(couponCode.value)));
      setCoupon(data);
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setLoading(false);
  }, [dispatch, couponCode.value]);

  if (!coupon) {
    return (
      <Modal
        title={t('Enter Coupon Code')}
        onCancel={close}
        actions={[
          <Button onClick={search} loading={loading}>{t('global:Search')}</Button>,
        ]}
      >
        <Input placeholder={t('Code')} {...couponCode} />
      </Modal>
    );
  }

  return (
    <Modal
      title={t('Redeem Coupon')}
      onCancel={close}
      actions={[
        <Button onClick={redeem} loading={loading}>{t('Redeem')}</Button>,
      ]}
    >
      <Wrapper>
        <h2>{coupon.code}</h2>
        <div className="percentage">
          {`-${coupon.percentage}%`}
        </div>
      </Wrapper>
    </Modal>
  );
};

CouponModal.propTypes = {
  eventId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

CouponModal.defaultProps = {
};

export default CouponModal;
