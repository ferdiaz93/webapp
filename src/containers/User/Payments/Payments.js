import React, { useEffect, useState } from 'react';
import shortid from 'shortid';
import { useLocation } from 'react-router-dom';
import qs from 'qs';
import { useDispatch, useSelector } from 'react-redux';
import { Trans } from 'react-i18next';

import { useTranslation } from 'hooks';
import * as appActions from 'state/app/actions';
import * as authActions from 'state/auth/actions';
import * as authSelectors from 'state/auth/selectors';

import Title from 'components/Main/Title';
import Main from 'components/Forms/Main';
import Wrapper from 'components/Forms/Wrapper';
import TitleWrapper from 'components/Forms/TitleWrapper';
import { FormWrapper } from 'components/Forms/Wrappers';
import ButtonsContainer from 'components/Forms/ButtonsContainer';
import Button from 'components/Button';
import Loading from 'components/Loading';

import locales from './i18n';

const Payments = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const location = useLocation();

  const mpPublicKey = useSelector(authSelectors.getMercadoPagoPublicKey);

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const load = async () => {
      const queryparams = qs.parse(location.search, { ignoreQueryPrefix: true });

      try {
        if (queryparams.code) {
          await dispatch(authActions.marketplaceMercadoPago({
            code: queryparams.code,
            redirectUri: `https://mazmo.net${location.pathname}`,
          }));
          dispatch(appActions.addToast(t('You can start receiving payments in Mercado Pago')));
        }
      } catch (error) {
        //
      }

      setIsLoading(false);
    };

    load();
  }, [location.search, location.pathname, dispatch, t]);

  if (isLoading) return <Loading />;

  return (
    <Main hasTitle>
      <Wrapper>
        <TitleWrapper>
          <Title>{t('Payments')}</Title>
        </TitleWrapper>

        <FormWrapper>
          <Trans t={t} i18nKey="payments.details" ns="UserPayments">
            {/* eslint-disable-next-line max-len */}
            <p>You can link your profile with Mercado Pago and start receiving payments directly to your account.</p>
          </Trans>

          <br />
          <br />

          {mpPublicKey && (
            <div>
              <Trans t={t} i18nKey="payments.linked" ns="UserPayments">
                {/* eslint-disable-next-line max-len */}
                <p>Your profile is already linked, but you can re link it clicking the button below.</p>
              </Trans>

              <br />
              <br />
            </div>
          )}

          <ButtonsContainer center>
            <Button to={`https://auth.mercadopago.com.ar/authorization?client_id=8019702344894897&response_type=code&platform_id=mp&state=${shortid.generate()}&redirect_uri=https://mazmo.net/user/payments`}>
              Vincular mi cuenta de Mercado Pago
            </Button>
          </ButtonsContainer>
        </FormWrapper>
      </Wrapper>
    </Main>
  );
};

export default Payments;
