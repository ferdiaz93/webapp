import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 32px;

  h3 {
    color: #331A1A;
    font-size: 18px;
    font-weight: 500;
    margin-bottom: 12px;
  }

  textarea {
    font-size: 16px;
    padding: 16px 24px;
    margin: 0 0 10px;
    border: 1px solid #F5F0F0;
    box-shadow: 0px 2px 4px rgba(0,0,0,0.04), 0px 4px 8px rgba(0,0,0,0.08);
    border-radius: 8px;
    transition: all 250ms ease-out;
    min-height: 100px;
    font-family: inherit;
    resize: none;
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;

export const SectionTitle = styled.h2`
  font-size: 20px;
  font-weight: 700;
  margin: 0;
`;

export const SectionWrapper = styled.div`
  background-color: ${props => props.theme.colors.disabledBackground};
  padding: 20px;
  border-radius: 10px;
  margin-bottom: 20px;
`;
