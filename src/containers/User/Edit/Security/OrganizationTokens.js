import React, { useRef, useState, useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useOpenClose, useInputValue } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import Modal from 'components/Modal';
import Input from 'components/Forms/Input';

import Token from './Token';
import locales from '../i18n';

const TokenList = styled.div`
  margin-bottom: 8px;
`;
TokenList.displayName = 'TokenList';

const TokenWrapper = styled.div`
  .token-info, .token-onetime {
    margin-bottom: 16px;
    text-align: center;
  }

  pre {
    border: 1px solid #999;
    padding: 8px;
    border-radius: 4px;
    background-color: #EEE;
    line-break: anywhere;
    white-space: initial;
    margin-top: 40px;
  }
`;
TokenWrapper.displayName = 'TokenWrapper';

const OrganizationTokens = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const nameInput = useRef(null);

  const tokens = useSelector(authSelectors.getOrganizationTokens, shallowEqual);

  const [isAddModalOpen, openAddModal, closeAddModal] = useOpenClose();
  const name = useInputValue('');

  const [encodedToken, setEncodedToken] = useState(null);
  const [saving, setSaving] = useState(false);

  const create = useCallback(async () => {
    try {
      setSaving(true);

      const token = await dispatch(authActions.createOrganizationToken(name.value));
      setEncodedToken(token);
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    name.change('');
    setSaving(false);
    closeAddModal();
  }, [dispatch, closeAddModal, name]);

  return (
    <div>
      <h2>{t('Organization Tokens')}</h2>

      <TokenList>
        {tokens.map(token => (
          <Token key={`org-token-${token.organizationTokenId}`} data={token} />
        ))}
      </TokenList>

      <Button color="white" fontColor="black" onClick={openAddModal}>{t('global:Add')}</Button>

      {/* Modals */}
      {isAddModalOpen && (
        <Modal
          title={t('Add organization token')}
          onCancel={closeAddModal}
          onOpenFocus={nameInput}
          actions={[
            <Button key="add-organization-token-confirm" onClick={create} loading={saving}>{t('global:Confirm')}</Button>,
          ]}
        >
          <Input ref={nameInput} placeholder={t('Token name')} {...name} />
        </Modal>
      )}

      {encodedToken && (
        <Modal
          onClose={() => setEncodedToken(null)}
        >
          <TokenWrapper>
            <div className="token-info">{t('token.info')}</div>
            <div className="token-onetime">{t('token.onetime')}</div>
            <pre>{encodedToken}</pre>
          </TokenWrapper>
        </Modal>
      )}
    </div>
  );
};

OrganizationTokens.propTypes = {
};

OrganizationTokens.defaultProps = {
};

export default OrganizationTokens;
