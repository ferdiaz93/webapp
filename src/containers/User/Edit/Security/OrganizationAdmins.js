import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import SearchUsersModal from 'components/SearchUsersModal';

import locales from '../i18n';
import OrganizationAdmin from './OrganizationAdmin';

const Table = styled.table`
  width: 100%;
  margin-bottom: 16px;
`;
Table.displayName = 'Table';

const OrganizationAdmins = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [isAddModalOpen, openAddModal, closeAddModal] = useOpenClose();
  const admins = useSelector(authSelectors.getOrganizationAdmins, shallowEqual);
  const [addingAdmin, setAddingAdmin] = useState(false);

  const add = useCallback(async (userId) => {
    try {
      setAddingAdmin(true);
      await dispatch(authActions.addOrganizationAdmin(userId));
      dispatch(appActions.addToast(t('Admin added')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setAddingAdmin(false);
    closeAddModal();
  }, [dispatch, t, closeAddModal]);

  return (
    <div>
      <h2>{t('Organization Admins')}</h2>

      <Table>
        <tbody>
          {admins.map(({ userId }) => (
            <OrganizationAdmin key={`orga-admin-${userId}`} userId={userId} />
          ))}
        </tbody>
      </Table>

      <Button color="white" fontColor="black" onClick={openAddModal}>{t('global:Add')}</Button>

      {/* Modals */}
      {isAddModalOpen && (
        <SearchUsersModal
          title={t('Add admin')}
          confirm={add}
          close={closeAddModal}
          confirmLoading={addingAdmin}
        />
      )}
    </div>
  );
};

OrganizationAdmins.propTypes = {
};

OrganizationAdmins.defaultProps = {
};

export default OrganizationAdmins;
