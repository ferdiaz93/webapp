import React, { useEffect, useCallback, useState } from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { useTranslation, useOpenClose, useInputValue } from 'hooks';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Warning from 'components/Warning';
import Input from 'components/Forms/Input';

import locales from '../i18n';

const Wrapper = styled.div`
  margin-top: 80px;
`;

const ModalWrapper = styled.div`
  ul {
    list-style: disc;
    margin: 8px 32px;
  }

  .reactivation {
    font-size: 14px;
  }
`;
Wrapper.displayName = 'Wrapper';

const Suspend = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [isOpenConfirmModal, openConfirmModal, closeConfirmModal] = useOpenClose(false);
  const [isOpenPasswordModal, openPasswordModal, closePasswordModal] = useOpenClose(false);
  const password = useInputValue('');
  const [suspending, setSuspending] = useState(false);

  useEffect(() => {
    if (isOpenPasswordModal) {
      closeConfirmModal();
    }
  }, [isOpenPasswordModal, closeConfirmModal]);

  const suspend = useCallback(async () => {
    try {
      closePasswordModal();
      setSuspending(true);
      await dispatch(authActions.suspendAccount(password.value));
    } catch (error) {
      setSuspending(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, closePasswordModal, password.value]);

  return (
    <>
      <Wrapper>
        <Button
          onClick={openConfirmModal}
          loading={suspending}
          color="white"
          fontColor="black"
        >
          {t('Suspend account')}
        </Button>
      </Wrapper>

      {/* Modals */}
      {isOpenConfirmModal && (
        <Modal
          title={t('Suspend account')}
          onCancel={closeConfirmModal}
          actions={[
            <Button key="suspend-confirm" onClick={openPasswordModal}>{t('global:Confirm')}</Button>,
          ]}
        >
          <ModalWrapper>
            {t('Are you sure you want to suspend your account?')}
            <br />
            <br />

            {t('If you wish to continue, note that')}
            <br />

            <ul>
              <li>{t('Your contact lists will be removed')}</li>
              <li>{t('Your known contacts will be removed')}</li>
              <li>{t('Your private messages will be removed')}</li>
              <li>{t('Your pictures and publications will be removed')}</li>
              <li>{t('Your relationships will be cancelled')}</li>
              <li>{t('You won\'t be able to participate in any community')}</li>
              <li>{t('You won\'t be able to access the chat')}</li>
              <li>{t('You won\'t be able to attend any events')}</li>
              <li>{t('Your profile won\'t be accessible any more')}</li>
              <li>{t('Your will lose all your Sades')}</li>
            </ul>

            <br />
            <br />

            <div className="reactivation">
              <Warning>
                {t('If later on, you want to reactivate your account, you will be able to do that, logging in with you username and password, as usual. Anyhow, the los data won\'t be recoverable.')}
              </Warning>
            </div>
          </ModalWrapper>
        </Modal>
      )}

      {isOpenPasswordModal && (
        <Modal
          title={t('Enter your password to confirm')}
          onCancel={closePasswordModal}
          actions={[
            <Button key="button-suspend" onClick={suspend}>{t('Suspend account')}</Button>,
          ]}
        >
          {t('Please enter your password to confirm suspension')}
          <br />
          <br />
          <Input type="password" {...password} />
        </Modal>
      )}
    </>
  );
};

Suspend.propTypes = {
};

Suspend.defaultProps = {
};

export default Suspend;
