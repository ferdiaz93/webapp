import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import StarMode from './StarMode';
import Password from './Password';
import Email from './Email';
import OrganizationTokens from './OrganizationTokens';
import OrganizationAdmins from './OrganizationAdmins';
import Suspend from './Suspend';
import FollowPublicationOnComment from './FollowPublicationOnComment';

const Wrapper = styled.div`
  margin: 32px;
`;
Wrapper.displayName = 'Wrapper';

const Section = styled.div`
  margin-bottom: 32px;

  h2 {
    font-size: 20px;
  }
`;
Section.displayName = 'Section';

const Privacy = () => {
  const isOrganization = useSelector(authSelectors.isOrganization);

  return (
    <Wrapper>
      <Section>
        <StarMode />
      </Section>

      <Section>
        <FollowPublicationOnComment />
      </Section>

      <Section>
        <Password />
      </Section>

      <Section>
        <Email />
      </Section>

      {isOrganization && (
        <>
          <Section>
            <OrganizationTokens />
          </Section>

          <Section>
            <OrganizationAdmins />
          </Section>
        </>
      )}

      <Section>
        <Suspend />
      </Section>
    </Wrapper>
  );
};

Privacy.propTypes = {
};

Privacy.defaultProps = {
};

export default Privacy;
