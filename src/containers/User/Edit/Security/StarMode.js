import React, { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';

import locales from '../i18n';

const Tip = styled.span`
  font-size: 14px;
  color: #A8A8A8;
`;

const StarMode = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const active = useSelector(authSelectors.selectStarMode);

  const [loading, setLoading] = useState(false);

  const onChange = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(authActions.updateStarMode(!active));
      dispatch(appActions.addToast(t('Star Mode updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
  }, [dispatch, t, active]);

  return (
    <div>
      <h2>{t('Star Mode')}</h2>

      <Toggle active={active} label={t('Active')} onChange={onChange} loading={loading} position="left" />
      <Tip>{t('Only people you follow will be able to follow you')}</Tip>
    </div>
  );
};

StarMode.propTypes = {
};

StarMode.defaultProps = {
};

export default StarMode;
