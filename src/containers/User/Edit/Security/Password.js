import React, { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useInputValue, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Input from 'components/Forms/Input';
import Button from 'components/Button';

import PasswordCheck from './PasswordCheck';
import locales from '../i18n';

const Password = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const username = useSelector(authSelectors.selectUsername);

  const newPassword = useInputValue('');
  const newPasswordRepeat = useInputValue('');

  const [showingSecurityCheck, openSecurityCheck, closeSecurityCheck] = useOpenClose(false);
  const [saving, setSaving] = useState(false);

  const save = useCallback(async (currentPassword) => {
    try {
      setSaving(true);

      if (newPassword.value !== newPasswordRepeat.value) {
        throw new Error(t('Passwords doesn\'t match'));
      }

      await dispatch(authActions.updatePassword(currentPassword, newPassword.value));
      history.replace(`/@${username}`);
      dispatch(appActions.addToast(t('Password updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
      setSaving(false);
    }
  }, [dispatch, history, username, newPassword, newPasswordRepeat, t]);

  const buttonDisabled = !newPassword.value || !newPasswordRepeat.value;

  return (
    <div>
      <h2>{t('Change Password')}</h2>

      <Input placeholder={t('New Password')} {...newPassword} type="password" />
      <Input placeholder={t('Repeat New Password')} {...newPasswordRepeat} type="password" />

      <Button onClick={openSecurityCheck} loading={saving} disabled={buttonDisabled}>{t('global:Save')}</Button>

      {/* Modal */}
      {showingSecurityCheck && (
        <PasswordCheck onCancel={closeSecurityCheck} onConfirm={save} />
      )}
    </div>
  );
};

Password.propTypes = {
};

Password.defaultProps = {
};

export default Password;
