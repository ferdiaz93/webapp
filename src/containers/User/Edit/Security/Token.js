import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as appActions from 'state/app/actions';
import * as authActions from 'state/auth/actions';

import { TrashCan } from 'components/Icons';
import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from '../i18n';

const TokenWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 8px;

  &:nth-child(2n) {
    background-color: ${props => props.theme.colors.mainLight};
  }

  button {
    padding: 0px 12px;

    .icon {
      display: flex;
      position: relative;
      top: auto;
      left: auto;
    }
  }
`;
TokenWrapper.displayName = 'TokenWrapper';

const Token = ({ data }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [removing, setRemoving] = useState(false);
  const [isModalOpen, openModal, closeModal] = useOpenClose();

  const remove = useCallback(async () => {
    try {
      setRemoving(true);
      await dispatch(authActions.removeOrganizationToken(data.organizationTokenId));
      dispatch(appActions.addToast(t('Organization token removed')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
  }, [dispatch, t, data.organizationTokenId]);

  return (
    <>
      <TokenWrapper>
        <div>{data.name}</div>
        <div>
          <Button
            color="white"
            icon={<TrashCan color="#666" outline />}
            onClick={openModal}
          />
        </div>
      </TokenWrapper>

      {/* Modals */}
      {isModalOpen && (
        <Modal
          title={t('Remove organization token')}
          actions={[
            <Button key={`delete-orgtoken-${data.organizationTokenId}`} loading={removing} onClick={remove}>{t('global:Confirm')}</Button>,
          ]}
          onCancel={closeModal}
        >
          {t('Are you sure you want to remove this token?')}
        </Modal>
      )}
    </>
  );
};

Token.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
    organizationTokenId: PropTypes.string.isRequired,
  }).isRequired,
};

Token.defaultProps = {
};

export default Token;
