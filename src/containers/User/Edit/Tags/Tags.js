import React, { useState, useCallback, useMemo } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { DraggableArea } from 'react-draggable-tags';
import { Trans } from 'react-i18next';
import shortid from 'shortid';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import UserTag from 'components/UserTag';
import Button from 'components/Button';
import PurchaseModal from 'components/PurchaseButton/PurchaseModal';

import { USERTAGS } from '../../../../constants';
import locales from '../i18n';
import { SectionTitle, SectionWrapper } from '../Wrapper';

const Explanation = styled.div`
  font-size: 16px;
  margin-bottom: 32px;

  > div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 16px;
  }
`;
Explanation.displayName = 'Explanation';

const AvailableSlotsSection = styled(SectionWrapper)`
  margin-top: 20px;

  > h3 {
    margin-bottom: 10px;
  }
`;

const AvailableSlots = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;

  > span {
    font-weight: 500;
    font-size: 18px;
  }
`;

const Wrapper = styled.div`
  margin: 32px !important;

  .usertags {
    .tag-group {
      margin-bottom: 16px;
      flex-wrap: wrap;
      font-size: 14px;
      display: flex;

      > div {
        margin: 0 4px 4px;
      }
    }
  }
`;
Wrapper.displayName = 'TagsWrapper';

const Counter = styled.div`
  font-size: 14px;
  margin-top: 16px;
  margin-bottom: 16px;
  color: ${props => props.theme.colors.secondary};
  display: block;
`;
Counter.displayName = 'Counter';

const AmountWarning = styled.span`
  color: ${props => props.theme.colors.main};
  font-size: 12px;
  display: block;
`;

const TagsSelector = styled(SectionWrapper)`
  > h2 {
    margin-bottom: 10px;
  }
  .usertags {
    margin-top: 20px;

    > div {
      margin-top: 10px;
    }
  }
`;

const TagsOrder = styled(SectionWrapper)`
  .usertags {
    margin: 10px 0;

    .usertag {
      color: black;
      margin: 0 4px 4px;
    }
  }
`;

const Tags = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const userTags = useSelector(authSelectors.selectTags, shallowEqual);
  const available = useSelector(authSelectors.tagsAvailable);
  const purchased = useSelector(authSelectors.tagsPurchased);

  const [tags, setTags] = useState(userTags);
  const [saving, setSaving] = useState(false);
  const [tagPurchase, setTagPurchase] = useState(null);
  const [slotPurchase, setSlotPurchase] = useState(null);
  const [saveEnabled, setSaveEnabled] = useState(false);
  const [extraSlots, setExtraSlots] = useState(null);

  const closeTagPurchase = useCallback(() => { setTagPurchase(null); setExtraSlots(null); }, []);
  const closeSlotPurchase = useCallback(() => { setSlotPurchase(null); setExtraSlots(null); }, []);

  const toggleSelect = useCallback(tagkey => () => {
    const tag = USERTAGS[tagkey].label;
    setTags((currentTags) => {
      if (currentTags.includes(tag)) {
        // removes an existing tag, must enable save button as well
        setSaveEnabled(true);
        return currentTags.filter(tt => tt !== tag);
      }

      if (USERTAGS[tagkey].price > 0 && !purchased.includes(tag)) {
        setTagPurchase(tag);
        if (currentTags.length >= available) {
          setExtraSlots(Array(currentTags.length - available + 1).fill('TAG_SLOT'));
        }
        return currentTags;
      }

      if (currentTags.length >= available) {
        setSlotPurchase(true);
        setExtraSlots(Array(currentTags.length - available).fill('TAG_SLOT'));
        return currentTags;
      }

      setSaveEnabled(true);

      return [
        ...currentTags,
        tag,
      ];
    });
  }, [available, purchased]);

  const reorderTags = useCallback((newTags) => {
    setTags(newTags.map(tag => tag.content));
    setSaveEnabled(true);
  }, []);

  const addTagAfterPurchase = useCallback(() => {
    setTags((currentTags) => {
      const nt = [...currentTags];
      if (tagPurchase) nt.push(tagPurchase);
      if (slotPurchase) nt.push(slotPurchase);
      return nt;
    });
    setSaveEnabled(true);

    setTagPurchase(null);
    setSlotPurchase(null);
  }, [tagPurchase, slotPurchase]);

  const purchaseSlots = useCallback(amount => () => {
    if (amount > 1) {
      setExtraSlots(Array(amount - 1).fill('TAG_SLOT'));
    }
    setSlotPurchase(true);
  }, []);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.updateTags(tags));
      setSaveEnabled(false);
      setSaving(false);
      dispatch(appActions.addToast(t('Tags updated')));
    } catch (e) {
      setSaving(false);
      dispatch(appActions.addError(e));
    }
  }, [dispatch, t, tags]);

  const groups = useMemo(() => {
    const res = {};

    Object.keys(USERTAGS).forEach((tagkey) => {
      const tag = USERTAGS[tagkey];
      if (!res[tag.group]) res[tag.group] = [];
      res[tag.group].push({ ...tag, key: tagkey });
    });

    return res;
  }, []);

  return (
    <>
      <Wrapper>
        <Explanation>
          <Trans t={t} i18nKey="tags.explanation" ns="userEdit">
            <span>
              Choose the tags that you feel define your sexuality.
              You can change them as many times as you want.
            </span>
          </Trans>
        </Explanation>
        <AvailableSlotsSection>
          <SectionTitle>{t('Available slots')}</SectionTitle>
          <AvailableSlots>
            <span>{t('Available {{available}}', { available })}</span>
            <Button onClick={purchaseSlots(tags.length > available ? tags.length - available : 1)}>
              {t('Add More', { amount: tags.length > available ? tags.length - available : 1 })}
            </Button>
          </AvailableSlots>
          <Counter>
            {t('Selected tags {{selected}}', { selected: tags.length })}
            {tags.length > available && (
              <AmountWarning>
                {t('You need to purchase spaces to save your tags', { amount: tags.length - available })}
              </AmountWarning>
            )}
          </Counter>
        </AvailableSlotsSection>
        {tags.length > 1 && (
          <>
            <TagsOrder>
              <SectionTitle>{t('Sort your tags')}</SectionTitle>
              <DraggableArea
                className="usertags"
                tags={tags.map(tag => ({ id: tag, content: tag }))}
                render={({ tag }) => (
                  <UserTag
                    dark
                    selected
                    role="button"
                    key={`usertag-general-${tag.content}`}
                    className="usertag"
                  >
                    {t(`global:TAG.${tag.content}`)}
                  </UserTag>
                )}
                onChange={reorderTags}
              />
              <Button onClick={save} disabled={!saveEnabled} loading={saving}>{t('global:Save')}</Button>
            </TagsOrder>
          </>
        )}
        <TagsSelector>
          <SectionTitle>{t('Select your tags')}</SectionTitle>
          <Button onClick={save} disabled={!saveEnabled} loading={saving}>{t('global:Save')}</Button>
          <Counter>
            {t('Selected tags {{selected}}', { selected: tags.length })}
            {tags.length > available && (
              <AmountWarning>
                {t('You need to purchase spaces to save your tags', { amount: tags.length - available })}
              </AmountWarning>
            )}
          </Counter>
          <div className="usertags">
            <div>
              {Object.values(groups).map(group => (
                <div className="tag-group" key={`taggroup-${shortid.generate()}`}>
                  {Object.values(group).map(tag => (
                    <UserTag
                      key={`usertag-general-${tag.label}`}
                      dark
                      selected={tags.includes(tag.label)}
                      onClick={toggleSelect(tag.key)}
                      role="button"
                      highlighted={tag.price > 0 && !purchased.includes(tag.label)}
                    >
                      {t(`global:TAG.${tag.label}`)}
                    </UserTag>
                  ))}
                </div>
              ))}
            </div>
          </div>

          <Counter>
            {tags.length > available && (
              <AmountWarning>
                {t('You need to purchase spaces to save your tags', { amount: tags.length - available })}
              </AmountWarning>
            )}
            {t('Selected tags {{selected}}', { selected: tags.length })}
          </Counter>

          <Button onClick={save} disabled={!saveEnabled} loading={saving}>{t('global:Save')}</Button>
        </TagsSelector>
      </Wrapper>

      {/* Modals */}
      {tagPurchase && (
        <PurchaseModal
          itemName={`USERTAG.${tagPurchase}`}
          otherItems={extraSlots || []}
          close={closeTagPurchase}
          afterPurchase={addTagAfterPurchase}
        />
      )}

      {slotPurchase && (
        <PurchaseModal itemName="TAG_SLOT" close={closeSlotPurchase} afterPurchase={addTagAfterPurchase} otherItems={extraSlots || []} />
      )}
    </>
  );
};

Tags.propTypes = {
};

Tags.defaultProps = {
};

export default Tags;
