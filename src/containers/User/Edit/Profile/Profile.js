import React, { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import moment from 'moment';

import { useTranslation, useInputValue } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Birthdate from 'components/ProfileForm/Birthdate';
import Countries from 'components/ProfileForm/Countries';
import Regions from 'components/ProfileForm/Regions';
import Cities from 'components/ProfileForm/Cities';
import Genders from 'components/ProfileForm/Genders';
import Pronouns from 'components/ProfileForm/Pronouns';
import InputWrapper from 'components/Forms/InputWrapper';
import Input from 'components/Forms/Input';
import Button from 'components/Button';

import OrganizationSwitch from './OrganizationSwitch';
import Wrapper from '../Wrapper';
import locales from '../i18n';

const Profile = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const userDisplayname = useSelector(authSelectors.selectDisplayname);
  const isOrganization = useSelector(authSelectors.isOrganization);

  const userCountry = useSelector(authSelectors.selectCountry, shallowEqual);
  const userRegion = useSelector(authSelectors.selectRegion, shallowEqual);
  const userCity = useSelector(authSelectors.selectCity, shallowEqual);

  const username = useSelector(authSelectors.selectUsername);
  const userGender = useSelector(authSelectors.selectGender);
  const userPronoun = useSelector(authSelectors.selectPronoun);
  const userBirthdate = useSelector(authSelectors.selectBirthdate);
  const userJob = useSelector(authSelectors.selectJob);
  const userAboutMe = useSelector(authSelectors.selectAboutMe);

  const displayname = useInputValue(userDisplayname);

  const [country, setCountry] = useState(userCountry ? userCountry.id : null);
  const [region, setRegion] = useState(userRegion ? userRegion.id : null);
  const [city, setCity] = useState(userCity ? userCity.id : null);

  const [gender, setGender] = useState({ label: t(`global:GENDER.${userGender}`), value: userGender });
  const [pronoun, setPronoun] = useState({ label: t(`global:PRONOUN.${userPronoun}`), value: userPronoun });

  const [birthdate, setBirthdate] = useState(moment(userBirthdate));
  const job = useInputValue(userJob);
  const aboutMe = useInputValue(userAboutMe);

  const [saving, setSaving] = useState(false);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.updateProfile({
        displayname: displayname.value,
        country,
        region,
        city,
        gender: gender.value,
        pronoun: pronoun.value,
        birthdate,
        job: job.value,
        aboutMe: aboutMe.value,
      }));
      dispatch(appActions.addToast(t('Profile updated')));
      history.push(`/@${username}/info`);
    } catch (err) {
      dispatch(appActions.addError(err));
      setSaving(false);
    }
  }, [
    dispatch, t, history, username, displayname, country, region,
    city, gender, pronoun, birthdate, job, aboutMe,
  ]);

  return (
    <Wrapper>
      <InputWrapper>
        <Input label={t('Display name')} maxChars={25} {...displayname} />
      </InputWrapper>

      {!isOrganization && (
        <>
          <InputWrapper>
            <h3>{t('Country')}</h3>
            <Countries value={country} onChange={setCountry} />
          </InputWrapper>

          <InputWrapper>
            <h3>{t('Region')}</h3>
            <Regions value={region} onChange={setRegion} countryId={country} />
          </InputWrapper>

          <InputWrapper>
            <Cities value={city} onChange={setCity} countryId={country} regionId={region} label />
          </InputWrapper>

          <InputWrapper>
            <h3>{t('global:Gender')}</h3>
            <Genders value={gender} onChange={setGender} simple={false} />
          </InputWrapper>

          <InputWrapper>
            <h3>{t('global:Pronoun')}</h3>
            <Pronouns value={pronoun} onChange={setPronoun} simple={false} />
          </InputWrapper>

          <InputWrapper>
            <h3>{t('Birthdate')}</h3>
            <Birthdate birthdate={birthdate} setBirthdate={setBirthdate} />
          </InputWrapper>

          <InputWrapper>
            <Input label={t('Job')} maxChars={25} {...job} />
          </InputWrapper>
        </>
      )}

      <InputWrapper>
        <h3>{t('About Me')}</h3>
        <textarea placehohlder={t('About Me')} {...aboutMe} />
      </InputWrapper>

      <Button onClick={save} loading={saving}>{t('global:Save')}</Button>

      <OrganizationSwitch />
    </Wrapper>
  );
};

Profile.propTypes = {
};

Profile.defaultProps = {
};

export default Profile;
