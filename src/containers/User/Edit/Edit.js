import React, { useMemo, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Route, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { useTranslation } from 'hooks';
import * as appSelectors from 'state/app/selectors';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import { TabsList, TabsWrapper } from 'components/Tabs';
import Layout from 'components/Layout';
import Sidebar from 'containers/Sidebar';
import PageTitle from 'components/PageTitle';
import {
  Account, TagMultiple, Cog,
  AccountCircle, ImageArea, ArrowBack,
} from 'components/Icons';

import Profile from './Profile';
import Avatar from './Avatar';
import Cover from './Cover';
import Tags from './Tags';
import Security from './Security';

import locales from './i18n';

const BackButton = styled.div`
  width: 36px;
  position: absolute;
  cursor: pointer;
  top: 32px;
  left: 32px;
`;

const ICON_COLOR = '#666';

const Edit = ({ match }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const uiLeftColumn = useSelector(appSelectors.selectIsUiLeftColumnActive);
  const historyCanGoBack = useSelector(appSelectors.selectHistoryCanGoBack);
  const isOrganization = useSelector(authSelectors.isOrganization);

  const tabs = useMemo(() => {
    const tags = isOrganization ? [] : [{ key: 'tags', label: t('Tags'), icon: <TagMultiple color={ICON_COLOR} /> }];
    return [
      { key: 'profile', label: t('Profile'), icon: <Account color={ICON_COLOR} /> },
      { key: 'avatar', label: t('Avatar'), icon: <AccountCircle color={ICON_COLOR} /> },
      { key: 'cover', label: t('Cover'), icon: <ImageArea color={ICON_COLOR} /> },
      ...tags,
      { key: 'security', label: t('Configuration'), icon: <Cog color={ICON_COLOR} /> },
    ];
  }, [t, isOrganization]);

  const onTabChange = useCallback((index) => {
    history.replace(`/user/edit/${tabs[index].key}`);
  }, [history, tabs]);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [uiLeftColumn, dispatch]);

  const onBackClick = useCallback(() => {
    if (historyCanGoBack) {
      history.goBack();
    } else {
      history.push('/');
    }
  }, [history, historyCanGoBack]);

  const tabIndex = tabs.findIndex(tab => document.location.pathname === `${match.path}/${tab.key}`);

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <BackButton onClick={onBackClick}>
            <ArrowBack color="black" />
          </BackButton>
          <PageTitle>{t('Edit your Profile')}</PageTitle>

          <TabsWrapper sticky>
            <TabsList data={tabs} selected={tabIndex} onSelect={onTabChange} />
          </TabsWrapper>

          <FlexContainer framed>
            <Route path={`${match.path}/profile`} component={Profile} />
            <Route path={`${match.path}/avatar`} component={Avatar} />
            <Route path={`${match.path}/cover`} component={Cover} />
            <Route path={`${match.path}/tags`} component={Tags} />
            <Route path={`${match.path}/security`} component={Security} />
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Edit.propTypes = {
  match: PropTypes.shape({
    path: PropTypes.string.isRequired,
  }).isRequired,
};

Edit.defaultProps = {
};

export default Edit;
