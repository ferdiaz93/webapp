import React, { useState, useCallback, useRef } from 'react';
import styled from 'styled-components';
import ReactCrop from 'react-image-crop';
import loadImage from 'blueimp-load-image';
import { useDispatch, useSelector } from 'react-redux';
import 'react-image-crop/dist/ReactCrop.css';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as userSelectors from 'state/users/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import FileSelector from 'components/FileSelector';

import { SectionTitle, SectionWrapper } from '../Wrapper';
import locales from '../i18n';

const FileSelectorWrapper = styled.div`
  text-align: center;
  margin: 32px;
`;
FileSelectorWrapper.displayName = 'FileSelectorWrapper';

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100vh - 48px);
  overflow: hidden;
`;
ContentWrapper.displayName = 'ContentWrapper';

const ContainerWrapper = styled.div`
  flex: 1;
  height: 100%;
  overflow: scroll;
  display: flex;
  flex-direction: column;
`;
ContainerWrapper.displayName = 'ContainerWrapper';

const Container = styled.div`
  margin: 24px;
  flex: 1;
  display: flex;
  align-items: center;
  text-align: center;
  align-self: center;
`;
Container.displayName = 'Container';

const ButtonWrapper = styled.div`
  flex-shrink: 0;
`;
ButtonWrapper.displayName = 'ButtonWrapper';

const CropWrapper = styled.div`
  width: 90%;
  max-width: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: center;

  .ReactCrop__image {
    max-height: inherit;
  }
`;

const CoverPreview = styled.img`
  margin-top: 10px;
  width: 100%;
`;

const Cover = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const userId = useSelector(authSelectors.selectId);
  const cover = useSelector(userSelectors.getCover(userId));

  const imageDOM = useRef(null);
  const [image, setImage] = useState(null);

  const [saving, setSaving] = useState(false);

  const onChange = useCallback((e) => {
    loadImage(
      e.target.files[0],
      (img) => {
        const base64data = img.toDataURL('image/jpeg');
        setImage({
          src: base64data,
          img,
        });
      },
      {
        maxWidth: 600, maxHeight: 700, orientation: true, canvas: true,
      },
    );
  }, []);

  const [crop, setCrop] = useState({
    x: 0,
    y: 0,
    width: 300,
    height: 150,
  });

  const onSubmitClick = useCallback(async () => {
    const getCroppedImg = (img, c, fileName) => {
      const canvas = document.createElement('canvas');
      const scaleX = img.naturalWidth / img.width;
      const scaleY = img.naturalHeight / img.height;
      canvas.width = c.width;
      canvas.height = c.height;
      const ctx = canvas.getContext('2d');

      ctx.drawImage(
        img,
        c.x * scaleX,
        c.y * scaleY,
        c.width * scaleX,
        c.height * scaleY,
        0,
        0,
        c.width,
        c.height,
      );

      return new Promise((resolve, reject) => {
        canvas.toBlob((blob) => {
          if (!blob) {
            reject(t('global:Something went wrong'));
          } else {
            // eslint-disable-next-line no-param-reassign
            blob.name = fileName;
            resolve(blob);
          }
        }, 'image/jpeg');
      });
    };

    try {
      setSaving(true);
      const croppedImg = await getCroppedImg(imageDOM.current, crop, 'temp.jpg');
      await dispatch(authActions.updateCover(croppedImg));
      window.location.reload();
    } catch (e) {
      setSaving(false);
      dispatch(appActions.addError(e));
    }
  }, [dispatch, crop, t]);

  const imageLoaded = useCallback((img) => {
    imageDOM.current = img;
  }, []);
  const onCropChange = useCallback((data) => {
    setCrop(data);
  }, []);

  return (
    <div>
      {cover && (
        <SectionWrapper>
          <SectionTitle>{t('Current Cover')}</SectionTitle>
          <CoverPreview src={cover} />
        </SectionWrapper>
      )}
      <SectionWrapper>
        <SectionTitle>{t('New Cover')}</SectionTitle>
        <div>
          {!image
            ? (
              <FileSelectorWrapper>
                <FileSelector onChange={onChange}>
                  <Button>{t('Upload image')}</Button>
                </FileSelector>
              </FileSelectorWrapper>
            ) : (
              <CropWrapper>
                <ReactCrop
                  src={image.src}
                  onChange={onCropChange}
                  onImageLoaded={imageLoaded}
                  crop={crop}
                  minWidth={300}
                  minHeight={150}
                  keepSelection
                />
              </CropWrapper>
            )
          }

          {image && (
            <Button onClick={onSubmitClick} loading={saving} disabled={!image}>{t('global:Confirm')}</Button>
          )}
        </div>
      </SectionWrapper>
    </div>
  );
};

Cover.propTypes = {
};

Cover.defaultProps = {
};

export default Cover;
