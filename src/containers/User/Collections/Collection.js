import React, { useContext, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import { useDispatch } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as appActions from 'state/app/actions';
import * as feedActions from 'state/feed/actions';

import { TrashCan, CursorMove } from 'components/Icons';
import Button from 'components/Button';
import { SelectableListItem } from 'components/SelectableList';
import Modal from 'components/Modal';
import Warning from 'components/Warning';

import locales from './i18n';

const Collection = ({ id, hashtag, setCollections }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);

  const [isRemoveModalOpen, openRemoveModal, closeRemoveModal] = useOpenClose();
  const [removing, setRemoving] = useState(false);

  const remove = useCallback(async () => {
    try {
      setRemoving(true);
      await dispatch(feedActions.removeCollection(id));
      dispatch(appActions.addToast(t('Collection removed')));

      setCollections(currentValue => currentValue.filter(c => c.id !== id));
    } catch (error) {
      dispatch(appActions.addError(error));
      setRemoving(false);
      closeRemoveModal();
    }
  }, [dispatch, t, closeRemoveModal, id, setCollections]);

  return (
    <>
      <SelectableListItem
        title={`#${hashtag}`}
        actions={[
          <CursorMove className="handle" color={theme.colors.secondary} key={`collection-move-${hashtag}`} />,
          <Button className="empty" onClick={openRemoveModal}><TrashCan color={theme.colors.main} key={`collection-delete-${hashtag}`} /></Button>,
        ]}
      />

      {isRemoveModalOpen && (
        <Modal
          onCancel={closeRemoveModal}
          title={t('Delete Collection')}
          actions={[
            <Button key={`collection-remove-${id}`} onClick={remove} loading={removing}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Are you sure you want to remove this Collection?')}

          <Warning>{t('Removing a Collections doesn\'t remove the publications in it')}</Warning>
        </Modal>
      )}
    </>
  );
};

Collection.propTypes = {
  id: PropTypes.string.isRequired,
  hashtag: PropTypes.string.isRequired,
  setCollections: PropTypes.func.isRequired,
};

Collection.defaultProps = {
};

export default Collection;
