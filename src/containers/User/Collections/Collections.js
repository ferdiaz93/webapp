import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { ReactSortable } from 'react-sortablejs';

import { useTranslation, useTitle } from 'hooks';
import * as feedActions from 'state/feed/actions';
import * as authSelectors from 'state/auth/selectors';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';
import { SelectableList } from 'components/SelectableList';
import Loading from 'components/Loading';
import EmptyState from 'components/EmptyState';

import CreateButton from './CreateButton';
import Collection from './Collection';
import locales from './i18n';

const ListWrapper = styled.div`
  .actions-container {
    > div {
      display: flex;
      align-items: center;
    }

    .handle {
      width: 24px;
      height: 24px;
      cursor: grab;
    }
  }
`;
ListWrapper.displayName = 'ListWrapper';

const Lists = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  useTitle(t('Collections'));

  const userId = useSelector(authSelectors.selectUserId);
  const [collections, setCollections] = useState(null);

  useEffect(() => {
    const load = async () => {
      const { data } = await dispatch(feedActions.loadCollections(userId));
      setCollections(data);
    };

    load();
  }, [dispatch, userId]);

  const sortItems = useCallback(async (sortedItems) => {
    if (sortedItems && collections) {
      const currentIds = collections.map(collection => collection.id);
      const ids = sortedItems.map(collection => collection.id);

      if (ids.some((id, index) => currentIds[index] !== id)) {
        // Optimistic
        const newValue = [];
        ids.forEach((id) => {
          const collection = collections.find(c => c.id === id);
          newValue.push(collection);
        });
        setCollections(newValue);

        await dispatch(feedActions.sortCollections(ids));
      }
    }
  }, [dispatch, collections]);

  if (collections === null) return <Loading />;

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('Collections')}</PageTitle>
          <CreateButton setCollections={setCollections} />

          <FlexContainer framed>
            {collections.length > 0 ? (
              <ListWrapper>
                <SelectableList>
                  <ReactSortable tag="div" list={collections} setList={sortItems} handle=".handle">
                    {collections.map(collection => (
                      <Collection
                        key={`collection-${collection.id}`}
                        id={collection.id}
                        hashtag={collection.hashtag}
                        setCollections={setCollections}
                      />
                    ))}
                  </ReactSortable>
                </SelectableList>
              </ListWrapper>
            ) : (
              <EmptyState title={t('You have no collections yet')} />
            )}
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Lists.propTypes = {
};

Lists.defaultProps = {
};

export default Lists;
