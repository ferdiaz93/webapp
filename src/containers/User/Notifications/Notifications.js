import React, { useEffect, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useTitle, useTranslation } from 'hooks';
import * as appSelectors from 'state/app/selectors';
import * as appActions from 'state/app/actions';
import * as userActions from 'state/users/actions';

import Sidebar from 'containers/Sidebar';
import LoadingPage from 'containers/App/LoadingPage';
import Layout from 'components/Layout';
import { TabsList, TabsWrapper } from 'components/Tabs';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';

import Email from './Email';
import Push from './Push';
import locales from './i18n';

const Home = () => {
  const { t } = useTranslation(locales);

  const uiLeftColumn = useSelector(appSelectors.selectIsUiLeftColumnActive);
  const dispatch = useDispatch();

  const [tabIndex, setTabIndex] = useState(0);
  const [data, setData] = useState(null);

  useTitle(t('Notifications'));

  useEffect(() => {
    const load = async () => {
      const config = await dispatch(userActions.loadNotificationsConfig());
      setData(config);
    };

    load();
  }, [dispatch]);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [uiLeftColumn, dispatch]);

  const onTabChange = useCallback((tab) => {
    setTabIndex(tab);
  }, []);

  const tabs = [
    { key: 'email', label: t('E-mail') },
    { key: 'push', label: t('Push') },
  ];

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper>
        <FlexInnerWrapper>
          <PageTitle>{t('Manage notifications')}</PageTitle>

          {data === null ? (
            <LoadingPage />
          ) : (
            <>
              <TabsWrapper sticky>
                <TabsList data={tabs} selected={tabIndex} onSelect={onTabChange} />
              </TabsWrapper>
              <FlexContainer framed>
                {tabIndex === 0 && (
                  <>
                    <p>{t('Choose what kind of e-mails do you want to receive in your inbox for notifications and updates.')}</p>
                    <Email data={data.email} />
                  </>
                )}

                {tabIndex === 1 && (
                  <>
                    <p>{t('Choose what kind of push notifications do you want to receive in your mobile device. You need to activate this option in the alerts dropwdown in order to work.')}</p>
                    <Push data={data.push} />
                  </>
                )}
              </FlexContainer>
            </>
          )}
        </FlexInnerWrapper>
      </FlexWrapper>
      <Sidebar />
    </Layout>
  );
};

export default Home;
