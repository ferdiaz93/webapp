import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import locales from 'containers/Profiles/Checklist/i18n';
import Select from 'components/Select';

const ExperienceSelect = ({ onChange, selection }) => {
  const { t } = useTranslation(locales);

  const options = useMemo(() => ([
    { value: null, label: '' },
    { value: 'NONE', label: t('EXPERIENCE.NONE') },
    { value: 'LITTLE', label: t('EXPERIENCE.LITTLE') },
    { value: 'SOME', label: t('EXPERIENCE.SOME') },
    { value: 'LOT', label: t('EXPERIENCE.LOT') },
  ]), [t]);

  const value = selection
    ? options.find(o => o.value === selection.experience) : null;

  return (
    <Select options={options} value={value} placeholder="" small onChange={onChange} />
  );
};

ExperienceSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  selection: PropTypes.shape({
    experience: PropTypes.string.isRequired,
  }),
};

ExperienceSelect.defaultProps = {
  selection: null,
};

export default ExperienceSelect;
