import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import Api from 'state/api';
import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import locales from 'containers/Profiles/Checklist/i18n';
import Spinner from 'components/Spinner';
import Button from 'components/Button';

import Wrapper from './Wrapper';
import Privacy from './Privacy';
import ExperienceSelect from './ExperienceSelect';
import InterestSelect from './InterestSelect';

const Content = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const username = useSelector(authSelectors.selectUsername);
  const checklistPrivacy = useSelector(authSelectors.selectChecklistPrivacy);
  const checklistLists = useSelector(authSelectors.selectChecklistListIds, shallowEqual);

  const [all, setAll] = useState(null);
  const [mine, setMine] = useState(null);
  const [loading, setLoading] = useState(false);
  const [privacy, setPrivacy] = useState(checklistPrivacy);
  const [lists, setLists] = useState(checklistLists || []);

  useEffect(() => {
    const loadAll = async () => {
      const { data } = await Api.req.get('/users/checklists');

      const sorted = data.sort((a, b) => a.category.order - b.category.order);
      setAll(sorted);
    };

    const loadMine = async () => {
      const { data } = await Api.req.get(`/users/${username}/checklist`);

      let result = [];
      data.forEach((s) => {
        result = result.concat(s.selections);
      });

      setMine(result);
    };

    loadAll();
    loadMine();
  }, [username]);

  const changeExperience = useCallback(item => (experience) => {
    setMine((currentValue) => {
      if (!currentValue.some(s => s.item.id === item.id)) {
        return [...currentValue, { item, experience: experience.value }];
      }

      return currentValue.map((s) => {
        if (s.item.id !== item.id) return s;
        return {
          ...s,
          experience: experience.value,
        };
      });
    });
  }, []);

  const changeInterest = useCallback(item => (interest) => {
    setMine((currentValue) => {
      if (!currentValue.some(s => s.item.id === item.id)) {
        return [...currentValue, { item, interest: interest.value }];
      }

      return currentValue.map((s) => {
        if (s.item.id !== item.id) return s;
        return {
          ...s,
          interest: interest.value,
        };
      });
    });
  }, []);

  const save = useCallback(async () => {
    try {
      setLoading(true);

      const selections = mine
        .filter(s => s.experience && s.interest)
        .map(s => ({ ...s, item: s.item.id }));
      await dispatch(authActions.updateChecklist(privacy, lists, selections));

      dispatch(appActions.addToast(t('Checklist updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
  }, [dispatch, t, mine, privacy, lists]);

  if (all === null || mine == null) return <Spinner color="#A8A8A8" />;

  return (
    <Wrapper>
      <div>
        <Privacy privacy={privacy} onPrivacyChange={setPrivacy} lists={lists} setLists={setLists} />
      </div>

      {all.map(section => (
        <div className="section" key={`checklist-section-${section.category.id}`}>
          <h3>{t(section.category.name)}</h3>

          <table>
            <thead>
              <tr>
                <td />
                <td>{t('Experience')}</td>
                <td>{t('Interest')}</td>
              </tr>
            </thead>
            <tbody>
              {section.items.map((item) => {
                const selection = mine.find(s => s.item.id === item.id);

                return (
                  <tr key={`checklist-item-${item.id}`}>
                    <td>{t(item.name, { ns: 'Checklist' })}</td>
                    <td>
                      <ExperienceSelect selection={selection} onChange={changeExperience(item)} />
                    </td>
                    <td>
                      <InterestSelect selection={selection} onChange={changeInterest(item)} />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      ))}

      <div className="actions">
        <Button onClick={save} loading={loading}>{t('global:Save')}</Button>
      </div>
    </Wrapper>
  );
};

Content.propTypes = {
};

Content.defaultProps = {
};

export default Content;
