import React, { useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import locales from 'containers/Profiles/Checklist/i18n';
import Select from 'components/Select';
import Toggle from 'components/Toggle';

const Privacy = ({
  privacy, lists, onPrivacyChange, setLists,
}) => {
  const { t } = useTranslation(locales);

  const allLists = useSelector(authSelectors.selectUserLists, shallowEqual);

  const options = useMemo(() => ([
    { value: 'PUBLIC', label: t('PRIVACY.PUBLIC') },
    { value: 'CONTACTS', label: t('PRIVACY.CONTACTS') },
    { value: 'LISTS', label: t('PRIVACY.LISTS') },
  ]), [t]);

  const setPrivacy = useCallback((opt) => {
    onPrivacyChange(opt.value);
  }, [onPrivacyChange]);

  const listToggled = useCallback(id => (evt) => {
    const { checked } = evt.target;

    setLists((currentValue = []) => {
      if (!checked) return currentValue.filter(cId => cId !== id);
      return [
        ...currentValue,
        id,
      ];
    });
  }, [setLists]);

  return (
    <div>
      <Select
        label={t('Privacy')}
        options={options}
        value={options.find(o => o.value === privacy)}
        onChange={setPrivacy}
      />

      {privacy === 'LISTS' && (
        <div>
          {allLists.map(list => (
            <div key={`privacy-list-${list.id}`}>
              <Toggle
                active={lists.includes(list.id)}
                label={list.name}
                onChange={listToggled(list.id)}
                position="left"
              />
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

Privacy.propTypes = {
  privacy: PropTypes.string.isRequired,
  lists: PropTypes.arrayOf(PropTypes.string),
  onPrivacyChange: PropTypes.func.isRequired,
  setLists: PropTypes.func.isRequired,
};

Privacy.defaultProps = {
  lists: [],
};

export default Privacy;
