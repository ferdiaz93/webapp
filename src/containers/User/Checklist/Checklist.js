import React from 'react';

import { useTranslation, useTitle } from 'hooks';

import locales from 'containers/Profiles/Checklist/i18n';
import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';

import Content from './Content';

const Checklist = () => {
  const { t } = useTranslation(locales);
  useTitle(t('Checklist'));

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('Checklist')}</PageTitle>

          <FlexContainer framed>
            <Content />
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Checklist.propTypes = {
};

Checklist.defaultProps = {
};

export default Checklist;
