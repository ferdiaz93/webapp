import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import loadImage from 'blueimp-load-image';
import { useDispatch } from 'react-redux';

import Api from 'state/api';
import { useTranslation } from 'hooks';
import * as appActions from 'state/app/actions';

import ContextMenu from 'components/ContextMenu';
import FileSelector from 'components/FileSelector';
import AvatarModal from 'components/AvatarModal';

import EditModal from './Modals/EditModal';
import HooksModal from './Modals/HooksModal';
import SecretModal from './Modals/SecretModal';
import RemoveModal from './Modals/RemoveModal';
import locales from './i18n';

const BotContainer = styled.div`
  display: flex;
  position: relative;

  img {
    width: 50px;
    height: 50px;
    margin-right: 16px;
  }

  .name {
    font-weight: 500;
    font-size: 20px;
  }

  .description {
    color: ${props => props.theme.colors.secondary};
  }
`;
BotContainer.displayName = 'BotContainer';

const Bot = ({
  id, name, description, avatar, hooks, reload,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [localAvatar, setLocalAvatar] = useState(null);
  const [modal, setModal] = useState(null);
  const openModal = key => () => setModal(key);
  const closeModal = () => {
    setModal(null);
    setLocalAvatar(null);
  };


  const onAvatarFileChange = async (e) => {
    loadImage(
      e.target.files[0],
      (img) => {
        const base64data = img.toDataURL('image/jpeg');
        setLocalAvatar({
          src: base64data,
          img,
        });
      },
      {
        maxWidth: 600,
        maxHeight: 700,
        orientation: true,
        canvas: true,
      },
    );
  };

  const onAvatarSubmit = async (croppedImg) => {
    try {
      const formData = new FormData();
      formData.append('avatar', croppedImg, croppedImg.name);

      await Api.req.put(`/chat/bots/${id}`, formData, {
        headers: {
          'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
        },
        timeout: 30000,
      });

      dispatch(appActions.addToast(t('Avatar changed')));
      setTimeout(reload, 10);
    } catch (error) {
      dispatch(appActions.addError(error));
    }
  };

  const items = [
    {
      key: 'edit',
      onClick: openModal('edit'),
      component: t('Edit information'),
    },
    {
      key: 'hooks',
      onClick: openModal('hooks'),
      component: t('Edit hooks'),
    },
    {
      key: 'secret',
      onClick: openModal('secret'),
      component: t('Renew secret'),
    },
    {
      key: 'avatar',
      component: (
        <FileSelector onChange={onAvatarFileChange}>
          <span>{t('Change avatar')}</span>
        </FileSelector>
      ),
    },
    {
      key: 'remove',
      onClick: openModal('remove'),
      component: t('Remove'),
      danger: true,
    },
  ];

  return (
    <>
      <BotContainer>
        <img src={avatar} alt={name} />
        <div>
          <div className="name">{name}</div>
          <div className="description">{description}</div>
        </div>
        <ContextMenu
          items={items}
          size={5}
        />
      </BotContainer>

      {/* Modals */}
      {modal === 'edit' && (
        <EditModal
          id={id}
          name={name}
          description={description}
          onCancel={closeModal}
          reload={reload}
        />
      )}
      {modal === 'hooks' && <HooksModal id={id} hooks={hooks || {}} onCancel={closeModal} reload={reload} />}
      {modal === 'secret' && <SecretModal id={id} onCancel={closeModal} />}
      {localAvatar && (
        <AvatarModal close={closeModal} image={localAvatar} onSubmit={onAvatarSubmit} />
      )}
      {modal === 'remove' && <RemoveModal id={id} onCancel={closeModal} reload={reload} />}
    </>
  );
};

Bot.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  hooks: PropTypes.shape({
    message: PropTypes.string,
    join: PropTypes.string,
    part: PropTypes.string,
    ban: PropTypes.string,
    update: PropTypes.string,
    messageUpdate: PropTypes.string,
    reactionAdded: PropTypes.string,
    reactionRemoved: PropTypes.string,
  }).isRequired,
  reload: PropTypes.func.isRequired,
};

Bot.defaultProps = {
};

export default Bot;
