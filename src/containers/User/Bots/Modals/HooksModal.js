import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import Api from 'state/api';
import { useTranslation, useInputValue } from 'hooks';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Input from 'components/Forms/Input';
import InputWrapper from 'components/Forms/InputWrapper';
import Button from 'components/Button';

import locales from '../i18n';

const EditModal = ({
  id, onCancel, hooks, reload,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [saving, setSaving] = useState(false);
  const newMessage = useInputValue(hooks.message);
  const newJoin = useInputValue(hooks.join);
  const newPart = useInputValue(hooks.part);
  const newBan = useInputValue(hooks.ban);
  const newUpdate = useInputValue(hooks.update);
  const newMessageUpdate = useInputValue(hooks.messageUpdate);
  const newReactionAdded = useInputValue(hooks.reactionAdded);
  const newReactionRemoved = useInputValue(hooks.reactionRemoved);
  const newSadesTransfer = useInputValue(hooks.sadesTransfer);

  const submit = async () => {
    try {
      setSaving(true);
      await Api.req.put(`/chat/bots/${id}/hooks`, {
        message: newMessage.value,
        join: newJoin.value,
        part: newPart.value,
        ban: newBan.value,
        update: newUpdate.value,
        messageUpdate: newMessageUpdate.value,
        reactionAdded: newReactionAdded.value,
        reactionRemoved: newReactionRemoved.value,
        sadesTransfer: newSadesTransfer.value,
      });
      dispatch(appActions.addToast(t('Bot hooks updated')));
      setTimeout(reload, 10);
    } catch (error) {
      dispatch(appActions.addError(error));
      onCancel();
    }
  };

  return (
    <Modal
      onCancel={onCancel}
      actions={[
        <Button key="bot-info-edit" onClick={submit} loading={saving}>{t('global:Confirm')}</Button>,
      ]}
    >
      <InputWrapper>
        <Input
          label={t('On Message Create')}
          {...newMessage}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Join')}
          {...newJoin}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Part')}
          {...newPart}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Ban')}
          {...newBan}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Channel Update')}
          {...newUpdate}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Message Update')}
          {...newMessageUpdate}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Reaction Added')}
          {...newReactionAdded}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Reaction Removed')}
          {...newReactionRemoved}
        />
      </InputWrapper>
      <InputWrapper>
        <Input
          label={t('On Sades Received')}
          {...newSadesTransfer}
        />
      </InputWrapper>
    </Modal>
  );
};

EditModal.propTypes = {
  id: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  hooks: PropTypes.shape({
    message: PropTypes.string,
    join: PropTypes.string,
    part: PropTypes.string,
    ban: PropTypes.string,
    update: PropTypes.string,
    messageUpdate: PropTypes.string,
    reactionAdded: PropTypes.string,
    reactionRemoved: PropTypes.string,
    sadesTransfer: PropTypes.string,
  }).isRequired,
  reload: PropTypes.func.isRequired,
};

EditModal.defaultProps = {
};

export default EditModal;
