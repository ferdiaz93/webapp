import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import Api from 'state/api';
import { useTranslation } from 'hooks';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from '../i18n';

const RemoveModal = ({ id, onCancel, reload }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  const submit = async () => {
    try {
      setLoading(true);
      await Api.req.delete(`/chat/bots/${id}`);
      dispatch(appActions.addToast(t('Bot removed')));
      setTimeout(reload, 10);
    } catch (error) {
      dispatch(appActions.addError(error));
      onCancel();
    }
  };

  return (
    <Modal
      onCancel={onCancel}
      actions={[
        <Button key="remove-bot-confirm" onClick={submit} loading={loading}>{t('global:Remove')}</Button>,
      ]}
    >
      {t('Are you sure you want to delete this bot?')}
    </Modal>
  );
};

RemoveModal.propTypes = {
  id: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  reload: PropTypes.func.isRequired,
};

RemoveModal.defaultProps = {
};

export default RemoveModal;
