import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import Api from 'state/api';
import { useTranslation, useInputValue } from 'hooks';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Input from 'components/Forms/Input';
import InputWrapper from 'components/Forms/InputWrapper';
import Button from 'components/Button';

import locales from '../i18n';

const EditModal = ({
  id, onCancel, name, description, reload,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [saving, setSaving] = useState(false);
  const newName = useInputValue(name);
  const newDescription = useInputValue(description);

  const submit = async () => {
    try {
      setSaving(true);
      await Api.req.put(`/chat/bots/${id}`, {
        name: newName.value,
        description: newDescription.value,
      });
      dispatch(appActions.addToast(t('Bot updated')));
      setTimeout(reload, 10);
    } catch (error) {
      dispatch(appActions.addError(error));
      onCancel();
    }
  };

  return (
    <Modal
      onCancel={onCancel}
      actions={[
        <Button key="bot-info-edit" onClick={submit} loading={saving}>{t('global:Confirm')}</Button>,
      ]}
    >
      <InputWrapper>
        <Input
          label={t('Name')}
          maxChars={40}
          {...newName}
        />
      </InputWrapper>

      <InputWrapper>
        <Input
          label={t('Description')}
          maxChars={140}
          {...newDescription}
        />
      </InputWrapper>
    </Modal>
  );
};

EditModal.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  reload: PropTypes.func.isRequired,
};

EditModal.defaultProps = {
};

export default EditModal;
