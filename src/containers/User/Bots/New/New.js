import React, { useState, useCallback, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { useTranslation, useInputValue, useTitle } from 'hooks';
import Api from 'state/api';
import * as appActions from 'state/app/actions';

import {
  FlexWrapper,
  FlexInnerWrapper,
  FlexContainer,
  ActionsFooter,
} from 'components/FlexWrapper';
import Header from 'components/Header';
import Overflow from 'components/Layout/Overflow';
import PageTitle from 'components/PageTitle';
import Button from 'components/Button';
import Input from 'components/Forms/Input';
import InputWrapper from 'components/Forms/InputWrapper';
import Toggle from 'components/Toggle';
import Box from 'components/Forms/Box';

import Secret from '../Secret';
import locales from '../i18n';

const NewBot = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  useTitle(t('New Bot'));

  const [step, setStep] = useState(1);
  const [creating, setCreating] = useState(false);
  const secret = useRef(null);

  const name = useInputValue('');
  const description = useInputValue('');
  const [noHooks, setNoHooks] = useState(true);
  const toggleHooks = () => { setNoHooks(value => !value); };
  const message = useInputValue('');
  const join = useInputValue('');
  const part = useInputValue('');
  const ban = useInputValue('');
  const update = useInputValue('');
  const messageUpdate = useInputValue('');
  const reactionAdded = useInputValue('');
  const reactionRemoved = useInputValue('');

  const nextStep = useCallback(() => setStep(currentStep => currentStep + 1), []);

  const create = useCallback(async () => {
    try {
      setCreating(true);
      const { data } = await Api.req.post('/chat/bots', { name: name.value, description: description.value });
      secret.current = data.secret;
      dispatch(appActions.addToast(t('Bot created')));
      nextStep();
    } catch (error) {
      setCreating(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, t, name.value, description.value, nextStep]);

  const finish = useCallback(() => {
    history.push('/user/bots');
  }, [history]);

  return (
    <FlexWrapper>
      <FlexInnerWrapper>
        <Header title={t('New Bot')} mobileOnly />

        <Overflow>
          <PageTitle>{t('New bot')}</PageTitle>

          <FlexContainer>
            {step === 1 && (
              <div>
                <h2>{t('What is a chat bot?')}</h2>
                <div>{t('what.is.a.bot')}</div>

                <h2>{t('What do I need to create one?')}</h2>
                <div>{t('what.do.i.need')}</div>

                <h2>{t('How does it works?')}</h2>
                <div>{t('how.does.it.works')}</div>

                <h2>{t('Things to consider')}</h2>
                <ul>
                  <li>{t('Multiple misses to bot\'s hooks will result in termination')}</li>
                  <li>{t('Bots that are offensive or violates any rules in the site will be banned along with it\'s creator')}</li>
                  <li>{t('Idle bots will be terminated')}</li>
                </ul>
              </div>
            )}

            {step === 2 && (
              <Box style={{ maxWidth: '500px', margin: '0 auto' }}>
                <InputWrapper>
                  <Input
                    label={t('Name')}
                    maxChars={40}
                    {...name}
                  />
                </InputWrapper>

                <InputWrapper>
                  <Input
                    label={t('Description')}
                    maxChars={140}
                    {...description}
                  />
                </InputWrapper>

                <h2>Hooks</h2>
                <Toggle label={t('I will add them later')} active={noHooks} onChange={toggleHooks} />

                <br />
                <br />

                <InputWrapper>
                  <Input
                    label={t('On Message Create')}
                    disabled={noHooks}
                    {...message}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Join')}
                    disabled={noHooks}
                    {...join}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Part')}
                    disabled={noHooks}
                    {...part}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Ban')}
                    disabled={noHooks}
                    {...ban}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Channel Update')}
                    disabled={noHooks}
                    {...update}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Message Update')}
                    disabled={noHooks}
                    {...messageUpdate}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Reaction Added')}
                    disabled={noHooks}
                    {...reactionAdded}
                  />
                </InputWrapper>
                <InputWrapper>
                  <Input
                    label={t('On Reaction Removed')}
                    disabled={noHooks}
                    {...reactionRemoved}
                  />
                </InputWrapper>
              </Box>
            )}

            {step === 3 && (
              <Box style={{ maxWidth: '760px', margin: '0 auto', textAlign: 'center' }}>
                <h2>{t('This is the bot\'s secret key')}</h2>
                <Secret>{secret.current}</Secret>

                <p>{t('secret.explain')}</p>
              </Box>
            )}
          </FlexContainer>
        </Overflow>
      </FlexInnerWrapper>

      <ActionsFooter>
        <div />
        <div>
          {step === 1 && <Button color="white" fontColor="black" onClick={nextStep}>{t('I agree')}</Button>}
          {step === 2 && <Button empty onClick={create} loading={creating}>{t('Create')}</Button>}
          {step === 3 && <Button empty onClick={finish}>{t('Finish')}</Button>}
        </div>
      </ActionsFooter>
    </FlexWrapper>
  );
};

NewBot.propTypes = {
};

NewBot.defaultProps = {
};

export default NewBot;
