import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import Api from 'state/api';
import * as authSelectors from 'state/auth/selectors';

import Header from 'components/Header';
import PageTitle from 'components/PageTitle';
import Button from 'components/Button';
import Loading from 'components/Loading';
import EmptyState from 'components/EmptyState';
import Overflow from 'components/Layout/Overflow';
import {
  FlexWrapper,
  FlexInnerWrapper,
  FlexContainer,
  ActionsFooter,
} from 'components/FlexWrapper';

import Bot from './Bot';
import locales from './i18n';

const Bots = () => {
  const { t } = useTranslation(locales);

  const userId = useSelector(authSelectors.selectUserId);
  const [bots, setBots] = useState(null);

  const load = useCallback(async () => {
    setBots(null);
    const { data } = await Api.req.get('/chat/bots');
    setBots(data.filter(bot => bot.ownerId === userId));
  }, [userId]);

  useEffect(() => {
    load();
  }, [load]);

  if (bots === null) return <Loading />;

  return (
    <FlexWrapper>
      <FlexInnerWrapper>
        <Header title={t('My chat bots')} mobileOnly />

        {!bots.length
          ? <EmptyState title={t('You don\'t have any bots yet')} />
          : (
            <Overflow>
              <PageTitle>{t('My chat bots')}</PageTitle>

              <FlexContainer style={{ maxWidth: '800px', margin: '0 auto' }}>
                {bots.map(bot => (
                  <Bot key={bot.id} {...bot} reload={load} />
                ))}
              </FlexContainer>
            </Overflow>
          )
        }
      </FlexInnerWrapper>

      <ActionsFooter>
        <div />
        <div>
          <Button to="/user/bots/new">{t('Create a bot')}</Button>
        </div>
      </ActionsFooter>
    </FlexWrapper>
  );
};

Bots.propTypes = {
};

Bots.defaultProps = {
};

export default Bots;
