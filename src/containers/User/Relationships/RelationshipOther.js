import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';
import UserLink from 'components/UserLink';
import { Check, TrashCan, Close } from 'components/Icons';
import Button from 'components/Button';
import Modal from 'components/Modal';

import { RELATIONSHIP_TYPES } from '../../../constants';
import locales from './i18n';

const RelationshipOther = ({
  userId, type, relationshipId, approved,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const pronoun = useSelector(userSelectors.getPronoun(userId));
  const displayname = useSelector(userSelectors.getDisplayName(userId));

  const [showingRemoveModal, openRemoveModal, closeRemoveModal] = useOpenClose(false);
  const [showingApproveModal, openApproveModal, closeApproveModal] = useOpenClose(false);
  const [showingRejectModal, openRejectModal, closeRejectModal] = useOpenClose(false);

  const [saving, setSaving] = useState(false);

  const remove = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.removeRelationship(userId, relationshipId));
      dispatch(appActions.addToast(t('Relationship removed')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setSaving(false);
    closeRemoveModal();
  }, [dispatch, t, closeRemoveModal, userId, relationshipId]);

  const reject = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.removeRelationship(userId, relationshipId));
      dispatch(appActions.addToast(t('Relationship rejected')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setSaving(false);
    closeRejectModal();
  }, [dispatch, t, closeRejectModal, userId, relationshipId]);

  const approve = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.approveRelationship(userId, relationshipId));
      dispatch(appActions.addToast(t('Relationship approved')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setSaving(false);
    closeApproveModal();
  }, [dispatch, t, closeApproveModal, userId, relationshipId]);

  return (
    <tr>
      <td className="description">
        <UserLink userId={userId}>
          <UserAvatar userId={userId} size="16px" showOnline={false} />
          <UserDisplayName userId={userId} />
        </UserLink>
        {' '}
        {t('is your')}
        {' '}
        {t(`global:RELATION.${type}`, { context: pronoun })}
        {!approved && (
          <div className="pending">{t('Pending')}</div>
        )}
      </td>
      <td className="actions">
        {approved
          ? (
            <div>
              <Button className="empty" onClick={openRemoveModal}><TrashCan color="#A8A8A8" /></Button>
            </div>
          ) : (
            <div>
              <Button className="empty" onClick={openApproveModal}><Check className="approve" color="#A8A8A8" /></Button>
              <Button className="empty" onClick={openRejectModal}><Close color="#A8A8A8" /></Button>
            </div>
          )
        }
      </td>

      {/* Modals */}
      {showingRemoveModal && (
        <Modal
          title={t('Remove Relationship')}
          onCancel={closeRemoveModal}
          actions={[
            <Button key="relationsip-other-remove" onClick={remove} loading={saving}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Are you sure you want to remove your relationship with {{name}}?', { name: displayname })}
        </Modal>
      )}

      {showingRejectModal && (
        <Modal
          title={t('Reject Relationship')}
          onCancel={closeRejectModal}
          actions={[
            <Button key="relationsip-other-reject" onClick={reject} loading={saving}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Are you sure you want to reject the relationship requested by {{name}}?', { name: displayname })}
        </Modal>
      )}

      {showingApproveModal && (
        <Modal
          title={t('Approve Relationship')}
          onCancel={closeApproveModal}
          actions={[
            <Button key="relationsip-other-approve" onClick={approve} loading={saving}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Are you sure you want to approve the relationship requested by {{name}}?', { name: displayname })}
        </Modal>
      )}
    </tr>
  );
};

RelationshipOther.propTypes = {
  userId: PropTypes.number.isRequired,
  relationshipId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(Object.values(RELATIONSHIP_TYPES)).isRequired,
  approved: PropTypes.bool.isRequired,
};

RelationshipOther.defaultProps = {
};

export default RelationshipOther;
