import React, { useMemo, useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import SearchUsersModal from 'components/SearchUsersModal';
import Select from 'components/Select';

import { RELATIONSHIP_TYPES } from '../../../constants';
import locales from './i18n';

const AddRelationship = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [showingSearchModal, openSearchModal, closeSearchModal] = useOpenClose(false);

  const [saving, setSaving] = useState(false);
  const [type, setType] = useState(null);
  const types = useMemo(() => Object.values(RELATIONSHIP_TYPES).map(tt => ({ label: t(`global:RELATION.${tt}`), value: tt })), [t]);

  const save = useCallback(async (relatesTo) => {
    try {
      setSaving(true);
      if (!type) throw new Error(t('You need to select a relationship type'));

      await dispatch(authActions.addRelationship(relatesTo, type.value));
      dispatch(appActions.addToast(t('Relationship Request sent')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setSaving(false);
    closeSearchModal();
  }, [dispatch, t, closeSearchModal, type]);

  return (
    <div>
      <Button color="white" fontColor="black" onClick={openSearchModal}>{t('Create new one')}</Button>

      {/* Modals */}
      {showingSearchModal && (
        <SearchUsersModal
          title={t('Create Relationship')}
          close={closeSearchModal}
          confirm={save}
          confirmLoading={saving}
        >
          <div>
            <Select
              value={types.find(tt => tt === type)}
              onChange={setType}
              placeholder={t('Type')}
              options={types}
            />
          </div>
        </SearchUsersModal>
      )}
    </div>
  );
};

AddRelationship.propTypes = {
};

AddRelationship.defaultProps = {
};

export default AddRelationship;
