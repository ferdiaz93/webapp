import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';

import Content from './Content';
import locales from './i18n';

const Relationships = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const relationships = useSelector(authSelectors.selectRelationships, shallowEqual);

  const [error, setError] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        await dispatch(authActions.fetchRelationships());
      } catch (err) {
        setError(err);
      }
    };

    if (typeof relationships === 'undefined') {
      fetch();
    }
  }, [relationships, dispatch]);

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper>
        <FlexInnerWrapper>
          <PageTitle>{t('Relationships')}</PageTitle>

          <FlexContainer framed>
            <Content hasError={!!error} />
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Relationships.propTypes = {
};

Relationships.defaultProps = {
};

export default Relationships;
