import styled from 'styled-components';

const Wrapper = styled.div`
  min-height: 460px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  .spinnerwrapper {
    display: flex;
    flex: 1;
    align-items: center;
  }

  table {
    width: 100%;
    font-size: 14px;

    tbody {
      tr:nth-child(2n - 1) {
        background-color: ${props => props.theme.colors.mainLight};
      }

      td {
        padding: 4px;

        @media(max-width: 767px) {
          &:nth-child(2) {
            display: none;
          }
        }

        &.entity {
          color: ${props => props.theme.colors.secondary};
          width: auto;
        }

        &.concept {
          a {
            color: ${props => props.theme.colors.main};
          }
        }

        &.date {
          color: ${props => props.theme.colors.secondary};
          font-size: 12px;
          text-align: right;
        }

        &.amount {
          strong {
            font-weight: 500;
          }

          span {
            margin-left: 8px;
            font-size: 12px;
            color: ${props => props.theme.colors.secondary};
          }
        }
      }
    }
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
