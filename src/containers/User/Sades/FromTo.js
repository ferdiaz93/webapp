import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import { ArrowRightBoldBox, ArrowLeftBoldBox } from 'components/Icons';
import UserLink from 'components/UserLink';
import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';
import BotAvatar from 'components/BotAvatar';
import BotDisplayName from 'components/BotDisplayName';

import { BANK_BOX_TYPES } from '../../../constants';

const Wrapper = styled.div`
  display: flex;
  align-items: center;

  .bot, .userlink, .mazmo {
    display: flex;
    align-items: center;

    .avatar {
      width: 24px;
      height: 24px;
      margin-right: 8px;
    }
  }

  .mazmo img {
    width: 24px;
    height: 24px;
    margin-right: 8px;
    border-radius: 100%;
  }

  svg {
    width: 24px;
    height: 24px;
  }
`;
Wrapper.displayName = 'Wrapper';

const FromTo = ({ transaction }) => {
  const myId = useSelector(authSelectors.selectId);

  const { from: { owner: from }, to: { owner: to } } = transaction;
  const outgoing = from.type === 'USER' && from.id === myId;

  if (outgoing && to.type === 'USER') {
    return (
      <Wrapper>
        <ArrowRightBoldBox color="red" />
        <UserLink userId={to.id}>
          <UserAvatar userId={to.id} size="24px" showOnline={false} />
          <UserDisplayName userId={to.id} />
        </UserLink>
      </Wrapper>
    );
  }
  if (outgoing && to.type === 'BOT') {
    return (
      <Wrapper>
        <ArrowRightBoldBox color="red" />
        <div className="bot">
          <BotAvatar botId={to.id} size="24px" />
          <BotDisplayName botId={to.id} />
        </div>
      </Wrapper>
    );
  }
  if (outgoing && to.type === 'CENTRAL_BANK') {
    return (
      <Wrapper>
        <ArrowRightBoldBox color="red" />
        <div className="mazmo">
          <img src="/images/isologo.png" alt="Mazmo" />
          <span>Mazmo</span>
        </div>
      </Wrapper>
    );
  }

  if (from.type === 'USER') {
    return (
      <Wrapper>
        <ArrowLeftBoldBox color="green" />
        <UserLink userId={from.id}>
          <UserAvatar userId={from.id} size="24px" showOnline={false} />
          <UserDisplayName userId={from.id} />
        </UserLink>
      </Wrapper>
    );
  }
  if (from.type === 'BOT') {
    return (
      <Wrapper>
        <ArrowLeftBoldBox color="green" />
        <div className="bot">
          <BotAvatar botId={from.id} size="24px" />
          <BotDisplayName botId={from.id} />
        </div>
      </Wrapper>
    );
  }
  if (from.type === 'CENTRAL_BANK') {
    return (
      <Wrapper>
        <ArrowLeftBoldBox color="green" />
        <div className="mazmo">
          <img src="/images/isologo.png" alt="Mazmo" />
          <span>Mazmo</span>
        </div>
      </Wrapper>
    );
  }

  return null;
};

FromTo.propTypes = {
  transaction: PropTypes.shape({
    from: PropTypes.shape({
      owner: PropTypes.shape({
        type: PropTypes.oneOf(Object.values(BANK_BOX_TYPES)),
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    }),
    to: PropTypes.shape({
      owner: PropTypes.shape({
        type: PropTypes.oneOf(Object.values(BANK_BOX_TYPES)),
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    }),
  }).isRequired,
};

FromTo.defaultProps = {
};

export default FromTo;
