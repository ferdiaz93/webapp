import React, { useEffect, useState, useCallback } from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';

import Api from 'state/api';
import { useTranslation, useTitle } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';
import EmptyState from 'components/EmptyState';
import Spinner from 'components/Spinner';
import Pagination from 'components/Pagination';

import NewTransfer from './NewTransfer';
import BuyMore from './BuyMore';
import FromTo from './FromTo';
import Concept from './Concept';
import Wrapper from './UI/Wrapper';
import HeaderContainer from './UI/HeaderContainer';
import locales from './i18n';

const Lists = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  useTitle(t('global:Sades'));

  const myId = useSelector(authSelectors.selectId);
  const balance = useSelector(authSelectors.getSades);

  const [data, setData] = useState({});

  const loadPage = useCallback(async (page) => {
    setData(({ documents, ...p }) => p);
    const { data: result } = await Api.req.get('/bank/transactions', { params: { page, limit: 15 } });
    setData(result);
  }, []);

  useEffect(() => {
    loadPage(1);
    dispatch(appActions.uiLeftColumn(true));
  }, [loadPage, dispatch]);

  const { documents, ...pagination } = data;

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('global:Sades')}</PageTitle>

          <HeaderContainer>
            <div className="balance">{t('Balance §{{balance}}', { balance: balance.toFixed(2) })}</div>
            <div className="new-transfer">
              <NewTransfer />
              <BuyMore />
            </div>
          </HeaderContainer>

          <FlexContainer framed>
            <Wrapper>
              {documents ? (
                <>
                  {documents.length > 0 ? (
                    <table>
                      <tbody>
                        {documents.map((transaction) => {
                          const outgoing = transaction.from.owner.type === 'USER' && transaction.from.owner.id === myId;

                          return (
                            <tr key={`transaction-${transaction.id}`}>
                              <td className="entity"><FromTo transaction={transaction} /></td>
                              <td className="concept"><Concept transaction={transaction} /></td>
                              <td className="amount">
                                <strong>{`§${transaction.amount.toFixed(2)}`}</strong>
                                {outgoing && transaction.tax > 0 && <span>{`(+§${transaction.tax})`}</span>}
                              </td>
                              <td className="date">{moment(transaction.createdAt).format('D/M/YY HH:mm')}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  ) : (
                    <EmptyState title={t('No transactions yet')} />
                  )}
                </>
              ) : (
                <div className="spinnerwrapper"><Spinner color="#999" /></div>
              )}

              {typeof pagination.current !== 'undefined' && pagination.pages > 0 && (
                <Pagination {...pagination} onChange={loadPage} disabled={!!documents} />
              )}
            </Wrapper>
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

Lists.propTypes = {
};

Lists.defaultProps = {
};

export default Lists;
