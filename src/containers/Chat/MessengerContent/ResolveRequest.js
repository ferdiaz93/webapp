import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';
import * as messengerActions from 'state/messengers/actions';

import { useTranslation } from 'hooks';
import UserType from 'state/users/type';

import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import Bubble from '../Content/Bubble';
import locales from '../i18n';

const Request = styled.div`
  border-top: 1px solid #ccc;
  margin-top: 30px;
  padding: 20px;
  color: black;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;

  > div > div {
    width: 100%;
    text-align: left;
    padding: 8px 12px 16px;
    box-sizing: border-box;
  }

  .avatar > div {
    width: 6px;
    height: 6px;
  }

  button {
    margin: 40px 5px;

    &.empty {
      text-transform: initial;
      font-weight: normal;
      color: ${props => props.theme.colors.main};
      display: inline-box;
      margin: 32px auto;
    }
  }
`;

const Rejected = styled.div`
  color: #333;
  margin-top: 5px;
  font-size: 15px;
`;

const ResolveRequest = ({
  id,
  message,
  requester,
  rejected,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const [accepting, setAccepting] = useState(false);
  const [rejecting, setRejecting] = useState(false);

  const accept = async () => {
    try {
      setAccepting(true);
      await dispatch(messengerActions.approve(id));
    } catch (error) {
      dispatch(appActions.addError(error));
      setAccepting(false);
    }
  };

  const reject = async () => {
    try {
      setRejecting(true);
      await dispatch(messengerActions.reject(id));
      setRejecting(false);
    } catch (error) {
      dispatch(appActions.addError(error));
      setRejecting(false);
    }
  };

  return (
    <EmptyState
      title={t('Do you want to chat with {{username}}?', { username: requester.displayname })}
      subtitle={(
        <>
          {t('If you choose to accept, the user will be notified and you can start chatting stright away. If not, they will never be notified and won\'t be able to contact you again.', { context: requester.pronoun })}
          <Request>
            <Bubble author={requester}>
              {message}
            </Bubble>

            {rejected ? (
              <>
                <Rejected>{t('You have rejected this request')}</Rejected>
                <Button onClick={accept} loading={accepting} className="empty">{t('I\'ve made a mistake. I want to accept this request.')}</Button>
              </>
            ) : (
              <div>
                <Button onClick={accept} loading={accepting} disabled={rejecting}>{t('global:Accept')}</Button>
                {!rejected && <Button onClick={reject} loading={rejecting} disabled={accepting} color="white" fontColor="#666">{t('global:Reject')}</Button>}
              </div>
            )}
          </Request>
        </>
      )}
    />
  );
};

ResolveRequest.propTypes = {
  id: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  requester: UserType.isRequired,
  rejected: PropTypes.bool.isRequired,
};

export default ResolveRequest;
