import React, { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import Api from 'state/api';
import * as channelActions from 'state/channels/actions';
import * as appActions from 'state/app/actions';

import { useTranslation } from 'hooks';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Loading from 'components/Loading';
import EmptyState from 'components/EmptyState';
import { SelectableList } from 'components/SelectableList';

import UserItem from '../UserItem';
import locales from '../../i18n';

const Invites = ({ channelId, close, openAddInvite }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const [invites, setInvites] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      const { data } = await Api.req.get(`/chat/channels/${channelId}/invitations`);
      setInvites(data);
    };

    fetch();
  }, [channelId]);

  const onDelete = async (userId) => {
    const invitation = invites.find(i => i.inviteeId === userId);
    await dispatch(channelActions.removeInvitation(channelId, invitation.id));

    setInvites(prevInvites => prevInvites.filter(i => i.inviteeId !== userId));
    dispatch(appActions.addToast(t('Invitation removed')));
  };

  const addInvite = useCallback(() => {
    openAddInvite();
    close();
  }, [openAddInvite, close]);

  return (
    <Modal
      title={t('Invites')}
      onClose={close}
      actions={[
        <Button key="invite-add" onClick={addInvite}>{t('Add Invite')}</Button>,
      ]}
    >
      {invites === null
        ? <Loading />
        : (
          <SelectableList>
            {!invites.length && <EmptyState title={t('There are no invitations yet')} />}
            {invites.map(i => (
              <UserItem
                key={`invite-user-${i.inviteeId}`}
                userId={i.inviteeId}
                onDelete={onDelete}
              />
            ))}
          </SelectableList>
        )
      }
    </Modal>
  );
};

Invites.propTypes = {
  channelId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  openAddInvite: PropTypes.func.isRequired,
};

export default Invites;
