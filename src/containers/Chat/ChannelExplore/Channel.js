import React from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';
import * as userSelectors from 'state/users/selectors';
import * as userActions from 'state/users/actions';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';
import ChannelType from 'state/channels/type';

import Card from 'components/Card';
import Info from 'components/Card/Info';
import InfoItem from 'components/Card/InfoItem';
import { Account, AccessPoint, ShieldStar } from 'components/Icons';
import MembershipRequestButton from 'components/ChannelMembershipRequestButton';

import locales from '../i18n';
import { CHANNEL_ROLES } from '../../../constants';

const Channel = ({ channel }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const userIsLoggedIn = useSelector(authSelectors.loggedIn);
  const participants = (channel.participants || []);
  const online = useSelector(userSelectors.onlineCount(participants.map(p => p.userId)));
  const ownerId = (participants.find(p => p.role === CHANNEL_ROLES.OWNER) || {}).userId;
  const owner = useSelector(userSelectors.getById(ownerId), shallowEqual);
  if (owner && owner.loading) dispatch(userActions.fetchData(ownerId));

  return (
    <Card
      avatar={{
        src: channel.avatar,
        alt: channel.name,
        type: 'rounded',
      }}
      title={channel.name}
      link={`/chat/channels/${channel.id}`}
      description={(
        <>
          <Info>
            <InfoItem title={t('Members')}>
              <Account color={colors.grey} />
              {channel.participantCount}
            </InfoItem>
            {userIsLoggedIn && (
              <InfoItem title={t('Online')}>
                <AccessPoint color={colors.grey} />
                {online}
              </InfoItem>
            )}
            {owner && (
              <InfoItem title={t('Owner', { context: owner.pronoun })}>
                <ShieldStar color={colors.grey} outline />
                {owner.displayname}
              </InfoItem>
            )}
          </Info>
          <span>{channel.description}</span>
        </>
      )}
      action={(
        <MembershipRequestButton channel={channel} />
      )}
    />
  );
};

Channel.propTypes = {
  channel: ChannelType.isRequired,
};

export default Channel;
