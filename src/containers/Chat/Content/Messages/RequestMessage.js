import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import * as messengerSelectors from 'state/messengers/selectors';
import * as authSelectors from 'state/auth/selectors';

import Bubble from '../Bubble';

const RequestMessage = ({ messengerId }) => {
  const message = useSelector(state => messengerSelectors.getRequestMessage(state, messengerId));
  const requester = useSelector(
    state => messengerSelectors.getRequester(state, messengerId),
    shallowEqual,
  );
  const meId = useSelector(authSelectors.selectUserId);

  if (!requester) return null;

  const outgoing = requester.id === meId;

  return <Bubble bordered author={requester} outgoing={outgoing}>{message}</Bubble>;
};

RequestMessage.propTypes = {
  messengerId: PropTypes.string.isRequired,
};

RequestMessage.defaultProps = {
};

export default RequestMessage;
