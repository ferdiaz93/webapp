import styled from 'styled-components';

import colors from 'utils/css/colors';

const Author = styled.div`
  font-weight: bold;
  font-size: 12px;
  color: ${colors.red};
  cursor: pointer;
`;
Author.displayName = 'Author';

export default Author;
