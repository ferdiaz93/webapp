import { useEffect } from 'react';
import ReactGA from 'react-ga';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

const Analytics = () => {
  const location = useLocation();
  const userIsLoggedIn = useSelector(authSelectors.loggedIn);

  useEffect(() => {
    if (userIsLoggedIn) {
      ReactGA.set({ dimension1: 'yes' });
    } else {
      ReactGA.set({ dimension1: 'no' });
    }
  }, [userIsLoggedIn]);

  useEffect(() => {
    ReactGA.pageview(`${location.pathname}${location.search}`);
  }, [location]);

  return null;
};

Analytics.propTypes = {
};

Analytics.defaultProps = {
};

export default Analytics;
