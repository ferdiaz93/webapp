/* eslint-disable react-hooks/exhaustive-deps */
import React, { Suspense, lazy, useState } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import smoothscroll from 'smoothscroll-polyfill';
import ReactGA from 'react-ga';
import { useDispatch, useSelector } from 'react-redux';
import * as Sentry from '@sentry/react';

import useOnMount from 'hooks/useOnMount';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';

import Loading from 'components/Loading';

import LoadingPage from './LoadingPage';
import Analytics from './Analytics';
import Boundary from './Boundary';
import { GA_TRACKING_ID } from '../../constants';
import './App.css';

smoothscroll.polyfill();

const Sockets = lazy(() => import('./Sockets' /* webpackChunkName: "sockets" */));
const Routines = lazy(() => import('./Routines' /* webpackChunkName: "routines" */));
const AuthedRoute = lazy(() => import('./AuthedRoute' /* webpackChunkName: "authedroute" */));
const UnauthedRoute = lazy(() => import('./UnauthedRoute' /* webpackChunkName: "unauthedroute" */));
const GlobalInformation = lazy(() => import('./GlobalInformation' /* webpackChunkName: "globalinformation" */));
const BouncedAlert = lazy(() => import('./BouncedAlert' /* webpackChunkName: "bouncedalert" */));
const Coupons = lazy(() => import('./Coupons' /* webpackChunkName: "coupons" */));
const CommunitiesSwitch = lazy(() => import('./CommunitiesSwitch' /* webpackChunkName: "communitiesswitch" */));
const UserSwitch = lazy(() => import('./UsersSwitch' /* webpackChunkName: "userswitch" */));
const Profile = lazy(() => import('containers/Profiles' /* webpackChunkName: "profile" */));
const SiteLayout = lazy(() => import('containers/SiteLayout' /* webpackChunkName: "sitelayout" */));
const Landing = lazy(() => import('containers/Landing' /* webpackChunkName: "landing" */));
const Login = lazy(() => import('containers/Login' /* webpackChunkName: "login" */));
const Signup = lazy(() => import('containers/Signup' /* webpackChunkName: "signup" */));
const ForgotPassword = lazy(() => import('containers/ForgotPassword' /* webpackChunkName: "forgotpassword" */));
const ResetPassword = lazy(() => import('containers/ForgotPassword/Reset' /* webpackChunkName: "resetpassword" */));
const Logout = lazy(() => import('containers/Logout' /* webpackChunkName: "logout" */));
const Home = lazy(() => import('containers/Home' /* webpackChunkName: "home" */));
const Publication = lazy(() => import('containers/Home/Publication' /* webpackChunkName: "publication" */));
const NewPublication = lazy(() => import('containers/Home/New' /* webpackChunkName: "newpublication" */));
const InfoAbout = lazy(() => import('containers/Info/About' /* webpackChunkName: "infoabout" */));
const InfoTOS = lazy(() => import('containers/Info/TOS' /* webpackChunkName: "infotos" */));
const ChatDashboard = lazy(() => import('containers/Chat' /* webpackChunkName: "chatdashboard" */));
const Unsubscribe = lazy(() => import('containers/User/Unsubscribe' /* webpackChunkName: "userunsubscribe" */));
const Hashtags = lazy(() => import('containers/Home/Hashtags' /* webpackChunkName: "hashtags" */));
const EventAssistance = lazy(() => import('containers/Events/Assistance' /* webpackChunkName: "eventassist" */));
const EventsSwitch = lazy(() => import('./EventsSwitch' /* webpackChunkName: "eventsswitch" */));
const ConnectionsSwitch = lazy(() => import('./ConnectionsSwitch' /* webpackChunkName: "connectionsswitch" */));

const App = () => {
  const dispatch = useDispatch();

  const userIsLoggedIn = useSelector(authSelectors.loggedIn);
  const isAutoLoggingIn = useSelector(authSelectors.isAutoLoggingIn);

  const [initialized, setInitialized] = useState(false);

  useOnMount(() => {
    const { jwt } = window.localStorage;
    if (jwt) {
      dispatch(authActions.jwtLogin(jwt));
    }

    ReactGA.initialize(GA_TRACKING_ID);
    setInitialized(true);
  });

  if (!initialized || isAutoLoggingIn) return <LoadingPage />;

  return (
    <Boundary>
      <Suspense fallback={<Loading />}>
        {userIsLoggedIn && (
          <>
            <Sockets />
            <Routines />
          </>
        )}
        <Router>
          <Analytics />
          {userIsLoggedIn && <GlobalInformation />}
          {userIsLoggedIn && <BouncedAlert />}
          {userIsLoggedIn && <Coupons />}

          <Switch>
            <Route
              exact
              path="/"
              render={() => (
                !userIsLoggedIn
                  ? <Landing />
                  : <SiteLayout><Home /></SiteLayout>
              )}
            />
            <AuthedRoute
              path="/logout"
              exact
              component={Logout}
            />
            <UnauthedRoute
              path="/login"
              exact
              component={Login}
            />
            <UnauthedRoute
              path="/signup"
              exact
              component={Signup}
            />
            <UnauthedRoute
              path="/forgot-password"
              exact
              component={ForgotPassword}
            />
            <UnauthedRoute
              path="/password-reset/:hash"
              exact
              component={ResetPassword}
            />
            <Route
              path="/unsubscribe/:key"
              exact
              component={Unsubscribe}
            />
            <Route path="/info/about" component={InfoAbout} />
            <Route path="/info/tos" component={InfoTOS} />
            <SiteLayout>
              <Switch>
                <AuthedRoute
                  path="/trending"
                  exact
                  component={Home}
                />
                <AuthedRoute
                  path="/media"
                  exact
                  component={Home}
                />
                <AuthedRoute
                  path="/hashtags/:hashtag"
                  exact
                  component={Hashtags}
                />
                <AuthedRoute
                  path="/publications/:publicationId"
                  exact
                  component={Publication}
                />
                <AuthedRoute
                  path="/new/:type"
                  exact
                  component={NewPublication}
                />
                <Route
                  path="/chat/:type?/:chatId?"
                  component={ChatDashboard}
                />
                <Route
                  path="/@:username([^/]+)"
                  component={Profile}
                />
                <AuthedRoute
                  path="/eventassistance/:eventId/:key"
                  component={EventAssistance}
                />
                <Route path="/connections">
                  <ConnectionsSwitch />
                </Route>
                <Route path="/user">
                  <UserSwitch />
                </Route>
                <Route path="/events">
                  <EventsSwitch />
                </Route>
                <CommunitiesSwitch />
              </Switch>
            </SiteLayout>
          </Switch>
        </Router>
      </Suspense>
    </Boundary>
  );
};

export default Sentry.withProfiler(App);
