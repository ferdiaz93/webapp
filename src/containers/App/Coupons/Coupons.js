import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import qs from 'qs';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as bankSelectors from 'state/bank/selectors';
import * as bankActions from 'state/bank/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import { TicketPercent } from 'components/Icons';

import locales from '../i18n';

const Wrapper = styled.div`
  text-align: center;

  > svg {
    width: 128px;
    height: 128px;

    path {
      fill: ${props => props.theme.colors.main};
    }
  }

  .label {
    font-weight: 500;
    font-size: 24px;
    margin-bottom: 16px;
  }

  .code {
    margin-bottom: 16px;
    > div:nth-child(2) {
      font-size: 24px;
      font-weight: 500;
    }
  }

  .description {
    font-size: 32px;
    background-color: ${props => props.theme.colors.warningBackground};
    border: 2px solid ${props => props.theme.colors.warningBorder};
    padding: 4px 16px;
    border-radius: 8px;
  }
`;
Wrapper.displayName = 'Wrapper';

const Coupons = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const coupon = useSelector(bankSelectors.selectUnacknowledgedCoupon, shallowEqual);

  const [loading, setLoading] = useState(false);

  const ack = useCallback(async () => {
    try {
      setLoading('ack');
      await dispatch(bankActions.acknowledgeCoupon(coupon.code));
      setLoading(false);
    } catch (error) {
      dispatch(appActions.addError(error));
    }
  }, [dispatch, coupon]);

  const buy = useCallback(async () => {
    try {
      setLoading('buy');
      await dispatch(bankActions.acknowledgeCoupon(coupon.code));
      setLoading(false);
      history.push({
        pathname: '/user/sades/buy',
        search: qs.stringify({ coupon: coupon.code }),
      });
    } catch (error) {
      dispatch(appActions.addError(error));
    }
  }, [dispatch, coupon, history]);

  if (!coupon) return null;

  const actions = [
    <Button key="new-coupon-ack" onClick={ack} loading={loading === 'ack'} color="white" fontColor="black">{t('Maybe later')}</Button>,
    <Button key="new-coupon-buy" onClick={buy} loading={loading === 'buy'}>{t('Buy now')}</Button>,
  ];

  return (
    <Modal actions={actions}>
      <Wrapper>
        <TicketPercent />

        <div className="label">{t('You have a new coupon for your next Sades purchase!')}</div>

        <div className="code">
          <div>{t('Code')}</div>
          <div>{coupon.code}</div>
        </div>

        <div className="description">
          {coupon.discount > 0 && <span>{`${coupon.discount}% off`}</span>}
          {!coupon.discount && coupon.increment > 0 && <span>{`+ ${coupon.increment}%`}</span>}
        </div>
      </Wrapper>
    </Modal>
  );
};

Coupons.propTypes = {
};

Coupons.defaultProps = {
};

export default Coupons;
