import React, { useContext, useCallback, useState } from 'react';
import styled, { ThemeContext } from 'styled-components';
import PropTypes from 'prop-types';
import { Trans } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as messengerSelectors from 'state/messengers/selectors';
import * as userActions from 'state/users/actions';
import * as messengerActions from 'state/messengers/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';
import { Fire } from 'components/Icons';

import locales from '../i18n';

const FireWrapper = styled.div`
  .avatars {
    display: flex;
    justify-content: space-evenly;
    align-items: center;

    svg {
      width: 200px;
      height: 200px;
    }
  }

  .label {
    margin: 32px 0;
    font-weight: bold;
    text-align: center;
    font-size: 28px;
  }
`;
FireWrapper.displayName = 'FireWrapper';

const MatchModal = ({ fire }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation(locales);
  const theme = useContext(ThemeContext);

  const meId = useSelector(authSelectors.selectId);
  const messengerId = useSelector(state => messengerSelectors.selectByUserId(state, fire.to));

  const [loading, setLoading] = useState(null);

  const ack = useCallback(async () => {
    try {
      if (!loading) setLoading('ack');
      await dispatch(userActions.acknowledgeFire(fire.id));
    } catch (error) {
      dispatch(appActions.addError(error));
      setLoading(null);
    }
  }, [dispatch, loading, fire]);

  const createChat = useCallback(async () => {
    setLoading('chat');
    await ack();

    if (messengerId) history.push(`/chat/messengers/${messengerId}`);
    else {
      try {
        const data = await dispatch(messengerActions.create([fire.to]));
        history.push(`/chat/messengers/${data.id}`);
      } catch (error) {
        dispatch(appActions.addError(error));
        setLoading(null);
      }
    }
  }, [dispatch, fire, history, messengerId, ack]);

  return (
    <Modal
      actions={[
        <Button key="close-match" color="white" fontColor="black" onClick={ack} disabled={!!loading} loading={loading === 'ack'}>{t('global:Close')}</Button>,
        <Button key="chat-match" onClick={createChat} loading={loading === 'chat'} disabled={!!loading}>{t('global:Chat')}</Button>,
      ]}
    >
      <FireWrapper>
        <div className="avatars">
          <UserAvatar userId={meId} size="128px" showOnline={false} />
          <Fire color={theme.colors.main} />
          <UserAvatar userId={fire.to} size="128px" showOnline={false} />
        </div>

        <div className="label">
          <Trans t={t} i18nKey="fire.label" ns="App">
            {t('There is fire with')}
            {' '}
            <UserDisplayName userId={fire.to} />
            !
          </Trans>
        </div>
      </FireWrapper>
    </Modal>
  );
};

MatchModal.propTypes = {
  fire: PropTypes.shape({
    id: PropTypes.string.isRequired,
    to: PropTypes.number.isRequired,
  }).isRequired,
};

MatchModal.defaultProps = {
};

export default MatchModal;
