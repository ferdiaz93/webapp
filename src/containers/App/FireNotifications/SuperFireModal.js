import React, { useContext, useCallback, useState } from 'react';
import styled, { ThemeContext } from 'styled-components';
import PropTypes from 'prop-types';
import { Trans } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as userActions from 'state/users/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import UserLink from 'components/UserLink';
import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';
import { Fire } from 'components/Icons';

import locales from '../i18n';

const FireWrapper = styled.div`
  .avatars {
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    flex-direction: column;
    position: relative;

    svg {
      position: absolute;
      width: 80px;
      height: 80px;
      bottom: -40px;
    }
  }

  .label {
    margin: 64px 0 32px;
    font-weight: bold;
    text-align: center;
    font-size: 28px;

    a {
      color: ${props => props.theme.colors.main};
    }
  }
`;
FireWrapper.displayName = 'FireWrapper';

const SuperFireModal = ({ fire }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation(locales);
  const theme = useContext(ThemeContext);

  const username = useSelector(userSelectors.getUsername(fire.from));

  const [loading, setLoading] = useState(null);

  const ack = useCallback(async () => {
    try {
      if (!loading) setLoading('ack');
      await dispatch(userActions.acknowledgeSuperFire(fire.id));
    } catch (error) {
      dispatch(appActions.addError(error));
      setLoading(null);
    }
  }, [dispatch, loading, fire]);

  const goToProfile = useCallback(async () => {
    setLoading('profile');
    await ack();
    history.push(`/@${username}`);
  }, [history, username, ack]);

  return (
    <Modal
      actions={[
        <Button key="close-match" color="white" fontColor="black" onClick={ack} disabled={!!loading} loading={loading === 'ack'}>{t('global:Close')}</Button>,
        <Button key="superfire-match" onClick={goToProfile} loading={loading === 'profile'} disabled={!!loading}>{t('Go to Profile')}</Button>,
      ]}
    >
      <FireWrapper>
        <div className="avatars">
          <UserAvatar userId={fire.from} size="128px" showOnline={false} />
          <Fire color={theme.colors.main} />
        </div>

        <div className="label">
          <Trans t={t} i18nKey="superfire.label" ns="App">
            <UserLink userId={fire.from}><UserDisplayName userId={fire.from} /></UserLink>
            {' '}
            ignited
            {' '}
            <u>super fire</u>
            {' '}
            with you!
          </Trans>
        </div>
      </FireWrapper>
    </Modal>
  );
};

SuperFireModal.propTypes = {
  fire: PropTypes.shape({
    id: PropTypes.string.isRequired,
    from: PropTypes.number.isRequired,
  }).isRequired,
};

SuperFireModal.defaultProps = {
};

export default SuperFireModal;
