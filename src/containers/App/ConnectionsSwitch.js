import React, { Suspense, lazy } from 'react';
import { Route, Switch } from 'react-router-dom';

import NotFound from 'containers/NotFound';
import Loading from 'components/Loading';

const AuthedRoute = lazy(() => import('./AuthedRoute' /* webpackChunkName: "authedroute" */));
const Lovense = lazy(() => import('containers/Connections/Lovense' /* webpackChunkName: "connectionslovense" */));

const ConnectionsSwitch = () => (
  <Suspense fallback={<Loading />}>
    <Switch>
      <AuthedRoute path="/connections/lovense" component={Lovense} />

      <Route component={NotFound} />
    </Switch>
  </Suspense>
);

ConnectionsSwitch.propTypes = {
};

ConnectionsSwitch.defaultProps = {
};

export default ConnectionsSwitch;
