import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import formatNumber from 'utils/formatNumber';
import * as communitySelectors from 'state/communities/selectors';

import { Account, MessageReply, Forum } from 'components/Icons';
import InfoItem from 'components/InfoItem';

const StatsWrapper = styled.div`
  font-size: 12px;
  color: #CACACA;
  margin-top: 8px;

  @media(max-width: 767px) {
    margin-top: 4px;
  }
`;
StatsWrapper.displayName = 'StatsWrapper';

const Stats = ({ communityId }) => {
  const memberCount = useSelector(
    state => communitySelectors.selectMembershipCount(state, communityId),
  );
  const threadCount = useSelector(
    state => communitySelectors.selectThreadCount(state, communityId),
  );
  const replyCount = useSelector(state => communitySelectors.selectReplyCount(state, communityId));

  return (
    <StatsWrapper>
      <InfoItem size="16px">
        <Account color="#DADADA" />
        {formatNumber(memberCount)}
      </InfoItem>
      <InfoItem size="16px">
        <Forum outline color="#DADADA" />
        {formatNumber(threadCount)}
      </InfoItem>
      <InfoItem size="16px">
        <MessageReply color="#DADADA" />
        {formatNumber(replyCount)}
      </InfoItem>
    </StatsWrapper>
  );
};

Stats.propTypes = {
  communityId: PropTypes.string.isRequired,
};

Stats.defaultProps = {
};

export default Stats;
