import React from 'react';
import PropTypes from 'prop-types';

import CommunityType from 'state/communities/type';

import Community from './Community';

const List = ({ data }) => data
  .sort((a, b) => b.membershipCount - a.membershipCount)
  .map(community => (
    <Community key={community._id} community={community} />
  ));

List.propTypes = {
  data: PropTypes.arrayOf(CommunityType).isRequired,
};

export default List;
