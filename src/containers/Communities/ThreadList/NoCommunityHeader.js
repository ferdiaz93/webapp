import React, { useCallback } from 'react';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import { Infinite, Menu } from 'components/Icons';
import Header from 'components/Header';

import MarkAllAsSeen from './MarkAllAsSeen';
import locales from './i18n';

const InfiniteWrapper = styled.div`
  border-radius: 4px;
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.16);
  width: 20px;
  height: 20px;
  padding: 10px;
`;

const NoCommunityHeader = React.memo(() => {
  const { t } = useTranslation(locales);

  const renderAvatar = useCallback(() => (
    <InfiniteWrapper><Infinite /></InfiniteWrapper>
  ), []);

  const renderIcon = useCallback(() => (
    <Menu color="white" />
  ), []);

  const renderActions = useCallback(() => (
    <MarkAllAsSeen communityId="recent" />
  ), []);

  return (
    <Header
      title={t('All threads')}
      renderAvatar={renderAvatar}
      renderIcon={renderIcon}
      renderActions={renderActions}
    />
  );
});

NoCommunityHeader.propTypes = {
};

NoCommunityHeader.defaultProps = {
  renderActions: null,
};

export default NoCommunityHeader;
