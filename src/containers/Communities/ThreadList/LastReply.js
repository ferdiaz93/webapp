import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';
import * as threadSelectors from 'state/threads/selectors';

import UserDisplayName from 'components/UserDisplayName';

import locales from './i18n';

const LastReplyWrapper = styled.div`
  font-size: 14px;
  line-height: 14px;
  font-weight: 400;
  margin: 4px 0 4px;
  display: flex;
  color: ${colors.grey};

  .avatar {
    margin: 0 4px 0 8px;
    opacity: 0.75;
  }
`;

const LastReply = ({ threadId }) => {
  const { t } = useTranslation(locales);
  const lastReplyAuthorId = useSelector(threadSelectors.getLastReplyAuthorId(threadId));
  const lastReplyAt = useSelector(threadSelectors.getLastReplyAt(threadId));
  const hasReplies = useSelector(threadSelectors.hasReplies(threadId));

  return (
    <LastReplyWrapper>
      <div>
        {t(hasReplies ? 'Replied by' : 'Created by')}
        {' '}
        <UserDisplayName userId={lastReplyAuthorId} />
        {' '}
        {moment(lastReplyAt).fromNow()}
      </div>
    </LastReplyWrapper>
  );
};

LastReply.propTypes = {
  threadId: PropTypes.string.isRequired,
};

export default LastReply;
