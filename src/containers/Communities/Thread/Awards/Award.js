import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as bankSelectors from 'state/bank/selectors';

import * as images from './img';
import { AWARDS } from './constants';
import locales from '../i18n';

const AwardWrapper = styled.div`
  text-align: center;
  cursor: pointer;
  padding: 8px;
  box-sizing: border-box;
  border-radius: 8px;

  &:hover {
    background-color: ${props => props.theme.colors.mainLight};
  }

  img {
    width: 64px;
    height: 64px;
  }

  .name {
    font-weight: bold;
  }

  .price {
    font-size: 14px;
    margin-top: 4px;
    color: ${props => props.theme.colors.secondary};
  }
`;
AwardWrapper.displayName = 'AwardWrapper';

const Award = ({ name, onClick }) => {
  const { t } = useTranslation(locales);

  const price = useSelector(state => bankSelectors.selectBuyableItemPrice(state, `AWARD.${name}`));

  return (
    <AwardWrapper key={`award-${name}`} onClick={onClick}>
      <img src={images[name]} alt={name} />
      <div className="name">{t(`AWARD.${name}`)}</div>
      <div className="price">{`§${price}`}</div>
    </AwardWrapper>
  );
};

Award.propTypes = {
  name: PropTypes.oneOf(Object.keys(AWARDS)).isRequired,
  onClick: PropTypes.func.isRequired,
};

Award.defaultProps = {
};

export default Award;
