import React, {
  useCallback, useRef, useEffect, useState,
} from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import Api from 'state/api';
import { useTranslation, useOpenClose } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as authSelectors from 'state/auth/selectors';

import Button from 'components/Button';

import ButtonBuy from './ButtonBuy';
import ButtonRSVP from './ButtonRSVP';
import Instructions from '../Instructions';
import locales from '../../i18n';

const EventActionButton = ({ eventId, startListening, stopListening }) => {
  const earlyTickets = useSelector(state => (
    eventSelectors.hasEarlyTicketsFeatureEnabled(state, eventId)
  ));
  const userId = useSelector(authSelectors.selectId);

  const [hasBeenInvited, setHasBeenInvited] = useState(false);

  useEffect(() => {
    const load = async () => {
      try {
        const invitation = await Api.req.get(`/events/${eventId}/invites/${userId}`);
        if (invitation) setHasBeenInvited(true);
      } catch (error) {
        // Do nothing
      }
    };

    if (earlyTickets && userId) load();
  }, [earlyTickets, eventId, userId]);

  if (earlyTickets && !hasBeenInvited) {
    return (
      <ButtonBuy eventId={eventId} startListening={startListening} stopListening={stopListening} />
    );
  }

  return (
    <ButtonRSVP eventId={eventId} startListening={startListening} />
  );
};

EventActionButton.propTypes = {
  eventId: PropTypes.string.isRequired,
  startListening: PropTypes.func.isRequired,
  stopListening: PropTypes.func.isRequired,
};

const ActionButton = ({ eventId }) => {
  const { t } = useTranslation(locales);

  const suggestShare = useRef(false);
  const listening = useRef(false);

  const rsvpd = useSelector(state => eventSelectors.hasRSVPd(state, eventId));
  const [instrModalOpened, openInstructionsModal, closeInstructionsModal] = useOpenClose(false);

  const startListening = useCallback(() => { listening.current = true; }, []);
  const stopListening = useCallback(() => { listening.current = false; }, []);

  const afterAction = useCallback(() => {
    suggestShare.current = true;
    listening.current = false;
    openInstructionsModal();
  }, [openInstructionsModal]);

  useEffect(() => {
    if (rsvpd && listening.current) {
      afterAction();
    }
  }, [rsvpd, afterAction]);

  return (
    <>
      {rsvpd ? (
        <Button onClick={openInstructionsModal} color="white" fontColor="black">{t('Instructions')}</Button>
      ) : (
        <EventActionButton
          eventId={eventId}
          startListening={startListening}
          stopListening={stopListening}
        />
      )}
      {instrModalOpened && (
        <Instructions
          eventId={eventId}
          close={closeInstructionsModal}
          suggestShare={suggestShare.current}
        />
      )}
    </>
  );
};

ActionButton.propTypes = {
  eventId: PropTypes.string.isRequired,
};

ActionButton.defaultProps = {
};

export default ActionButton;
