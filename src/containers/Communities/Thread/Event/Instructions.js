import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Loader } from '@googlemaps/js-api-loader';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import AddToCalendar from '@culturehq/add-to-calendar';
import '@culturehq/add-to-calendar/dist/styles.css';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Loading from 'components/Loading';
import Warning from 'components/Warning';

import { GMAPS_API_KEY } from '../../../../constants';
import locales from '../i18n';

const Wrapper = styled.div`
  text-align: center;

  h3 {
    font-weight: 300;
    color: #333;
    margin-bottom: 0;
  }

  > div {
    margin-bottom: 16px;
  }

  .address strong {
    font-size: 32px;
  }

  .map > div {
    width: 100%;
    height: 300px;
  }

  .tocalendar {
    button {
      color: ${props => props.theme.colors.main};
    }

    svg {
      display: none;
    }

    .chq-atc--dropdown a {
      color: #333;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const Instructions = ({ eventId, close, suggestShare }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const mapElement = useRef(null);

  const name = useSelector(state => eventSelectors.selectName(state, eventId));
  const address = useSelector(state => eventSelectors.selectAddressString(state, eventId));
  const verificationCode = useSelector(state => (
    eventSelectors.selectVerificationCode(state, eventId)
  ));
  const earlyTickets = useSelector(state => (
    eventSelectors.hasEarlyTicketsFeatureEnabled(state, eventId)
  ));
  const url = useSelector(state => eventSelectors.selectUrl(state, eventId));
  const instructions = useSelector(state => eventSelectors.selectInstructions(state, eventId));
  const date = useSelector(state => eventSelectors.selectDate(state, eventId));
  const lat = useSelector(state => eventSelectors.selectLat(state, eventId));
  const lng = useSelector(state => eventSelectors.selectLng(state, eventId));

  useEffect(() => {
    const load = async () => {
      const loader = new Loader({
        apiKey: GMAPS_API_KEY,
        version: 'weekly',
        libraries: ['places'],
      });

      await loader.load();

      const coordinates = new global.google.maps.LatLng(lat, lng);

      // Map
      const map = new global.google.maps.Map(mapElement.current, {
        center: coordinates,
        zoom: 17,
        disableDefaultUI: true,
      });

      // Marker
      const marker = new global.google.maps.Marker({
        position: coordinates,
        title: address,
      });

      marker.setMap(map);
    };

    if (mapElement.current) {
      load();
    }
  }, [lat, lng, address]);

  useEffect(() => {
    if (address && !lat && !lng) {
      dispatch(eventActions.fetch(eventId));
    }
  }, [address, lat, lng, dispatch, eventId]);

  const pubLink = `/new/url?url=${encodeURIComponent(document.location.href)}&content=${encodeURIComponent(t('share.body'))}`;
  const actions = [];
  if (suggestShare) {
    actions.push(<Button key="event-share" to={pubLink}>{t('Create Publication')}</Button>);
  }

  return (
    <Modal
      onClose={close}
      actions={actions}
    >
      <Wrapper>
        {verificationCode && earlyTickets && (
          <div className="address">
            <Warning>
              <h3>Código de Verificación</h3>
              <strong>{verificationCode}</strong>
              <p>
                Guarda este código en un lugar seguro.
                Será requerido al momento de ingresar al Evento.
              </p>
            </Warning>
          </div>
        )}

        {(address && !lat && !lng) && <Loading />}

        {(address && lat && lng) && (
          <>
            <div className="address">
              <h3>{t('Address')}</h3>
              <strong>{address}</strong>
            </div>

            <div className="map">
              <h3>{t('Map')}</h3>
              <div ref={mapElement} />
            </div>
          </>
        )}

        {!address && (
          <div className="address">
            <h3>{t('URL')}</h3>
            <strong><a href={url} target="_blank" rel="noreferrer">{url}</a></strong>
          </div>
        )}

        {instructions && instructions.length > 0 && (
          <div className="instructions">
            <h3>{t('Instructions')}</h3>
            <div>{instructions}</div>
          </div>
        )}

        <div className="tocalendar">
          <AddToCalendar
            event={{
              name,
              details: instructions,
              location: address || url,
              startsAt: new Date(date).toISOString(),
              endsAt: moment(date).add(6, 'hours').toISOString(),
            }}
          >
            {t('Add to calendar')}
          </AddToCalendar>
        </div>
      </Wrapper>
    </Modal>
  );
};

Instructions.propTypes = {
  eventId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  suggestShare: PropTypes.bool,
};

Instructions.defaultProps = {
  suggestShare: false,
};

export default Instructions;
