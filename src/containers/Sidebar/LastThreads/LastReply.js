import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import * as threadSelectors from 'state/threads/selectors';

import UserDisplayName from 'components/UserDisplayName';

const LastReplyWrapper = styled.div`
  font-size: 12px;
  font-weight: 400;
  margin: 4px 0 0;
  display: flex;
  color: ${colors.grey};

  .avatar {
    margin: 0 4px 0 8px;
    opacity: 0.75;
  }
`;

const LastReply = ({ threadId }) => {
  const lastReplyAuthorId = useSelector(threadSelectors.getLastReplyAuthorId(threadId));
  const lastReplyAt = useSelector(threadSelectors.getLastReplyAt(threadId));

  return (
    <LastReplyWrapper>
      <div>
        <UserDisplayName userId={lastReplyAuthorId} />
        {' '}
        {moment(lastReplyAt).fromNow()}
      </div>
    </LastReplyWrapper>
  );
};

LastReply.propTypes = {
  threadId: PropTypes.string.isRequired,
};

export default LastReply;
