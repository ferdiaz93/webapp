import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';
import UserCountryFlag from 'components/UserCountryFlag';
import UserLink from 'components/UserLink';
import UserFollowButton from 'components/UserFollowButton';
import { Close } from 'components/Icons';

import locales from '../i18n';

const Wrapper = styled.div`
  display: flex;
  margin-bottom: 8px;
  align-items: center;

  .userlink {
    display: flex;
    flex: 1;

    .avatar {
      margin-right: 8px;
    }

    > div:not(.avatar) {
      flex: 1;

      > div {
        display: flex;
        font-size: 12px;
        color: ${props => props.theme.colors.secondary};

        .country-flag {
          width: 16px;
          height: 11px;
          margin-right: 4px;
        }

        svg {
          width: 16px;
          height: 16px;
        }
      }
    }
  }

  > button {
    background-color: white;
    padding-right: 36px;

    svg path {
      fill: black;
    }

    > div {
      width: 0;
    }
  }

  > svg {
    width: 16px;
    height: 16px;
    opacity: 0.5;
    cursor: pointer;
    margin-left: 4px;

    path {
      fill: ${props => props.theme.colors.secondary};
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const User = ({ userId, remove }) => {
  const { t } = useTranslation(locales);

  const gender = useSelector(userSelectors.getGender(userId));
  const age = useSelector(userSelectors.getAge(userId));

  const removeUser = () => remove(userId);

  return (
    <Wrapper>
      <UserLink userId={userId}>
        <UserAvatar userId={userId} size="32px" />
        <div>
          <UserDisplayName userId={userId} />
          {gender && (
            <div>
              <UserCountryFlag userId={userId} />
              <span>
                {t(`global:GENDER.${gender}`)}
                {' '}
                {t('{{age}} years old', { age })}
              </span>
            </div>
          )}
        </div>
      </UserLink>
      <UserFollowButton userId={userId} />
      <Close onClick={removeUser} />
    </Wrapper>
  );
};

User.propTypes = {
  userId: PropTypes.number.isRequired,
  remove: PropTypes.func.isRequired,
};

User.defaultProps = {
};

export default User;
