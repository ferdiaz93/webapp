import styled from 'styled-components';

const TopFeedContainer = styled.div`
  padding: 16px 16px 0 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 100vw;
  box-sizing: border-box;
  flex-direction: column;
`;
TopFeedContainer.displayName = 'TopFeedContainer';

export default TopFeedContainer;
