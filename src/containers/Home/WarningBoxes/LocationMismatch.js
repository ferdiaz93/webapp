import React, { useState, useCallback } from 'react';
import moment from 'moment';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import Warning from 'components/Warning';
import Button from 'components/Button';
import { Close } from 'components/Icons';

import locales from '../i18n';

const Wrapper = styled.div`
  text-align: center;
  padding: 8px;
  position: relative;

  .close-button {
    width: 20px;
    height: 20px;
    position: absolute;
    right: -4px;
    top: -4px;
    cursor: pointer;

    svg path {
      fill: ${props => props.theme.colors.secondary};
    }
  }

  button {
    margin-top: 16px;
    margin-bottom: 8px;
  }
`;
Wrapper.displayName = 'Wrapper';

const LocationMismatch = () => {
  const { t } = useTranslation(locales);

  const dismissed = localStorage.getItem('locationmismatchwarning');
  const daysdismissed = dismissed ? moment().diff((new Date(0)).setUTCSeconds(parseInt(dismissed, 10)), 'days') : 14;

  const [closed, setClosed] = useState(daysdismissed < 14);

  const close = useCallback(() => {
    localStorage.setItem('locationmismatchwarning', ((new Date()).getTime() / 1000));
    setClosed(true);
  }, []);

  if (closed) return null;

  return (
    <Warning fullMargin>
      <Wrapper>
        <div
          className="close-button"
          role="button"
          onClick={close}
          tabIndex={-1}
          onKeyDown={close}
        >
          <Close />
        </div>
        <div>{t('Your location seems to be outdated. Do you want to update it?')}</div>
        <div className="buttons">
          <Button color="white" fontColor="black">{t('Update')}</Button>
        </div>
      </Wrapper>
    </Warning>
  );
};

LocationMismatch.propTypes = {
};

LocationMismatch.defaultProps = {
};

export default LocationMismatch;
