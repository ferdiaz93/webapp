import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import Toggle from 'components/Toggle';
import Input from 'components/Forms/Input';
import HelpHint from 'components/HelpHint';

import locales from '../i18n';

const FundingContainer = styled.div`
  .toggle {
    display: flex;

    > label {
      flex: inherit;
      margin-right: 16px;
    }
  }

  .details {
    max-height: 0;
    overflow: hidden;
    transform: scaleY(0);
    transform-origin: top;
    transition: transform 0.26s ease;
    margin: 16px 0 0 48px;

    ${props => props.active && `
      transform: scaleY(1);
      max-height: 100%;
    `}

    .amount {
      display: flex;
      align-items: baseline;

      input {
        line-height: 32px;
        margin: 0 8px;
        width: 50px;
      }
    }

    .crowd {
      margin-top: 8px;
    }
  }
`;
FundingContainer.displayName = 'FundingContainer';

const Funding = ({ funding, setFunding }) => {
  const { t } = useTranslation(locales);

  const changeFunding = useCallback(e => (
    setFunding(e.target.checked ? { amount: 10, isTarget: false } : null)
  ), [setFunding]);
  const changeAmount = useCallback(e => setFunding(cv => (
    { ...cv, amount: e.target.value }
  )), [setFunding]);
  const changeIsTarget = useCallback(e => setFunding(cv => (
    { ...cv, isTarget: e.target.checked }
  )), [setFunding]);

  return (
    <FundingContainer active={!!funding}>
      <div className="toggle">
        <Toggle position="left" label={t('Sell this content for Sades')} active={!!funding} onChange={changeFunding} />
        <HelpHint
          content={(
            <div>
              <p>{t('funding.line1')}</p>
              <p>{t('funding.line2')}</p>
              <p>{t('funding.line3')}</p>
            </div>
          )}
        />
      </div>

      <div className="details">
        <div className="amount">
          {t('Sell for')}
          <Input type="number" min={1} value={funding ? funding.amount : 10} onChange={changeAmount} />
          {t('global:Sades')}
        </div>

        <div className="crowd">
          <Toggle position="left" label={t('Make it crowdfunded')} active={!!funding && funding.isTarget} onChange={changeIsTarget} />
        </div>
      </div>
    </FundingContainer>
  );
};

Funding.propTypes = {
  funding: PropTypes.shape({
    amount: PropTypes.number.isRequired,
    isTarget: PropTypes.bool.isRequired,
  }),
  setFunding: PropTypes.func.isRequired,
};

Funding.defaultProps = {
  funding: null,
};

export default Funding;
