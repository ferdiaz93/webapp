import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';
import * as userActions from 'state/users/actions';

import Modal from 'components/Modal';
import UserList from 'components/UserList';

import locales from '../../i18n';

const Following = ({ userId, close }) => {
  const { t } = useTranslation(locales);

  const fetchAction = useCallback(() => userActions.getFollowing(userId), [userId]);

  return (
    <Modal
      fullHeight
      title={t('Following')}
      onClose={close}
    >
      <UserList fetchAction={fetchAction} onUserClick={close} />
    </Modal>
  );
};

Following.propTypes = {
  userId: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired,
};

Following.defaultProps = {
};

export default Following;
