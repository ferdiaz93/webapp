import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as messengerSelectors from 'state/messengers/selectors';
import * as userSelectors from 'state/users/selectors';
import * as messengerActions from 'state/messengers/actions';

import { MessageReply, DotsVertical } from 'components/Icons';
import { Action } from 'components/Header';
import Menu, { Item as MenuItem } from 'components/Menu';
import FireButton from 'components/FireButton';
import TransferModal from 'components/TransferButton/TransferModal';

import KnowModal from '../Modals/Know';
import UnknowModal from '../Modals/Unknow';
import BlockModal from '../Modals/Block';
import UnblockModal from '../Modals/Unblock';
import MuteModal from '../Modals/Mute';
import ReportModal from '../Modals/Report';
import locales from '../../i18n';

const ActionsOthers = ({ userId }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation(locales);

  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const pronoun = useSelector(userSelectors.getPronoun(userId));
  const messengerId = useSelector(state => messengerSelectors.selectByUserId(state, userId));
  const knowing = useSelector(state => authSelectors.selectDoIKnow(state, userId));
  const blocked = useSelector(state => authSelectors.selectIsBlocked(state, userId));

  const [showingContextMenu, openContextMenu, closeContextMenu] = useOpenClose();
  const [showingKnowModal, openKnowModal, closeKnowModal] = useOpenClose();
  const [showingUnknowModal, openUnknowModal, closeUnknowModal] = useOpenClose();
  const [showingBlockModal, openBlockModal, closeBlockModal] = useOpenClose();
  const [showingUnblockModal, openUnblockModal, closeUnblockModal] = useOpenClose();
  const [showingMuteModal, openMuteModal, closeMuteModal] = useOpenClose();
  const [showingReportModal, openReportModal, closeReportModal] = useOpenClose();
  const [showingTransferModal, openTransferModal, closeTransferModal] = useOpenClose();

  const message = useCallback(async () => {
    if (messengerId) history.push(`/chat/messengers/${messengerId}`);

    const messenger = await dispatch(messengerActions.create([userId]));
    history.push(`/chat/messengers/${messenger.id}`);
  }, [userId, dispatch, messengerId, history]);

  return (
    <>
      <Action onClick={openTransferModal} style={{ color: '#fff', fontWeight: '800' }}>§</Action>
      <FireButton userId={userId} />
      <Action onClick={message}><MessageReply color="white" /></Action>

      <Action onClick={openContextMenu} pressed={showingContextMenu}><DotsVertical className="more-actions" color="white" /></Action>
      <Menu open={showingContextMenu} onClose={closeContextMenu} className="nouserlink">
        {!isOrganization && !knowing && <MenuItem onClick={openKnowModal}>{t('I know this person')}</MenuItem>}
        {!isOrganization && knowing && <MenuItem onClick={openUnknowModal}>{t('Remove as known', { context: pronoun })}</MenuItem>}
        {!blocked && <MenuItem onClick={openBlockModal}>{t('Block')}</MenuItem>}
        {blocked && <MenuItem onClick={openUnblockModal}>{t('Unblock')}</MenuItem>}
        {<MenuItem onClick={openMuteModal}>{t('Mute/Unmute')}</MenuItem>}
        <MenuItem onClick={openReportModal}>{t('Report')}</MenuItem>
      </Menu>

      {/* Modals */}
      {showingKnowModal && <KnowModal userId={userId} close={closeKnowModal} />}
      {showingUnknowModal && <UnknowModal userId={userId} close={closeUnknowModal} />}
      {showingBlockModal && <BlockModal userId={userId} close={closeBlockModal} />}
      {showingUnblockModal && <UnblockModal userId={userId} close={closeUnblockModal} />}
      {showingMuteModal && <MuteModal userId={userId} close={closeMuteModal} />}
      {showingReportModal && <ReportModal userId={userId} close={closeReportModal} />}
      {showingTransferModal && <TransferModal to={{ type: 'USER', id: userId }} close={closeTransferModal} />}
    </>
  );
};

ActionsOthers.propTypes = {
  userId: PropTypes.number.isRequired,
};

ActionsOthers.defaultProps = {
};

export default ActionsOthers;
