import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import { GenderTransgender, CakeVariant } from 'components/Icons';

import locales from '../i18n';

const GenderAgeWrapper = styled.div.attrs({
  className: 'genderage',
})`
  svg {
    width: 24px;
    height: 24px;
    margin: -8px 4px 0 0;
  }
`;
GenderAgeWrapper.displayName = 'GenderAgeWrapper';

const GenderAge = ({ userId }) => {
  const { t } = useTranslation(locales);

  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const gender = useSelector(userSelectors.getGender(userId));
  const age = useSelector(userSelectors.getAge(userId));

  if (isOrganization) return null;

  return (
    <GenderAgeWrapper>
      {gender && (
        <span>
          <GenderTransgender color="#a7a7a7" />
          <span>{t(`global:GENDER.${gender}`)}</span>
        </span>
      )}
      <span>
        <CakeVariant color="#a7a7a7" />
        <span>{t('{{age}} years old', { age })}</span>
      </span>
    </GenderAgeWrapper>
  );
};

GenderAge.propTypes = {
  userId: PropTypes.number.isRequired,
};

GenderAge.defaultProps = {
};

export default GenderAge;
