import React, {
  useState, useRef, useCallback, useEffect,
} from 'react';
import { useParams } from 'react-router-dom';
import fastdom from 'fastdom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as feedActions from 'state/feed/actions';

import Publication from 'components/Publication';
import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import EmptyState from 'components/EmptyState';
import Ad from 'components/Ad';

import locales from '../i18n';

const Photos = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const params = useParams();
  const userId = useSelector(userSelectors.getByUsername(params.username));

  const [publicationIds, setPublicationIds] = useState([]);
  const [fullyLoaded, setFullyLoaded] = useState(false);
  const isLoading = useRef(false);
  const publicationsCount = useRef(0);

  const load = useCallback(async () => {
    if (!isLoading.current && !fullyLoaded) {
      isLoading.current = true;
      const ids = await dispatch(feedActions.loadByAuthor(
        userId, publicationsCount.current, true,
      ));
      publicationsCount.current += ids.length;

      if (!ids.length) {
        setFullyLoaded(true);
      } else {
        setPublicationIds(prevPublications => [
          ...prevPublications,
          ...ids,
        ]);
      }

      isLoading.current = false;
    }
  }, [userId, dispatch, fullyLoaded]);

  useEffect(() => {
    const el = document.documentElement;

    const loadMoreScrollChanged = () => {
      fastdom.measure(() => {
        if (el.scrollHeight - el.scrollTop < 2500) {
          load();
        }
      });
    };

    document.addEventListener('scroll', loadMoreScrollChanged);
    load();

    return () => {
      document.removeEventListener('scroll', loadMoreScrollChanged);
    };
  }, [load]);

  return (
    <div>
      <div>
        {publicationIds.map((publicationId, index) => (
          <React.Fragment key={`pub-${publicationId}`}>
            <Publication publicationId={publicationId} />
            {index > 0 && !(index % 8) && <Ad id="In Feed" />}
          </React.Fragment>
        ))}
      </div>

      {!fullyLoaded && (
        <SpinnerWrapper>
          <Spinner color="#999" />
        </SpinnerWrapper>
      )}

      {fullyLoaded && publicationIds.length === 0 && (
        <EmptyState subtitle={t('No publications to show')} />
      )}
    </div>
  );
};

Photos.propTypes = {
};

Photos.defaultProps = {
  onlyMedia: false,
};

export default Photos;
