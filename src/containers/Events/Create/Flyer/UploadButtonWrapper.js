import styled from 'styled-components';

const UploadButtonWrapper = styled.div`
  padding: 40px;
  display: flex;
  justify-content: center;
  border: 3px dashed ${props => props.theme.colors.disabledBackground};
`;
UploadButtonWrapper.displayName = 'UploadButtonWrapper';

export default UploadButtonWrapper;
