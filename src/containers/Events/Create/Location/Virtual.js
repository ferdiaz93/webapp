import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import Input from 'components/Forms/Input';

import locales from '../i18n';

const Virtual = ({ url, instructions }) => {
  const { t } = useTranslation(locales);
  const urlElement = useRef(null);

  useEffect(() => {
    if (urlElement.current) urlElement.current.focus();
  }, []);

  return (
    <div>
      <h3>{t('Event virtual location')}</h3>

      <Input ref={urlElement} placeholder={t('URL')} type="text" {...url} />

      <div className="instructions">
        <Input placeholder={t('Instructions (optional)')} {...instructions} />
      </div>
    </div>
  );
};

Virtual.propTypes = {
  url: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  instructions: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

Virtual.defaultProps = {
};

export default Virtual;
