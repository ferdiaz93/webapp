import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import ToggleButtonGroup from 'components/ToggleButtonGroup';

import Wrapper from './Wrapper';
import Map from './Map';
import Virtual from './Virtual';
import locales from '../i18n';

const Location = ({
  location, setLocation, url, instructions,
}) => {
  const { t } = useTranslation(locales);
  const [selected, setSelected] = useState('map');

  return (
    <Wrapper>
      <div className="button-group">
        <ToggleButtonGroup
          buttons={[
            { key: 'map', label: t('Physical') },
            { key: 'virtual', label: t('Virtual') },
          ]}
          selected={selected}
          onChange={setSelected}
        />
      </div>

      {selected === 'map'
        ? <Map location={location} setLocation={setLocation} instructions={instructions} />
        : <Virtual url={url} instructions={instructions} />
      }
    </Wrapper>
  );
};

Location.propTypes = {
  location: PropTypes.shape({
    route: PropTypes.string,
    streetNumber: PropTypes.string,
    intersection: PropTypes.string,
    adminArea: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
  }),
  setLocation: PropTypes.func.isRequired,
  url: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  instructions: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

Location.defaultProps = {
  location: null,
};

export default Location;
