import React, {
  useRef, useEffect, useState, useCallback,
} from 'react';
import PropTypes from 'prop-types';
import { Loader } from '@googlemaps/js-api-loader';

import { useTranslation } from 'hooks';

import Input from 'components/Forms/Input';
import Button from 'components/Button';

import { GMAPS_API_KEY } from '../../../../constants';
import locales from '../i18n';

const Map = ({ location, setLocation, instructions }) => {
  const { t } = useTranslation(locales);

  const mapElement = useRef(null);
  const autocompleteElement = useRef(null);
  const autocomplete = useRef(null);

  const [address, setAddress] = useState('');
  const [error, setError] = useState(false);

  const addressChanged = useCallback(e => setAddress(e.target.value), []);
  const clearLocation = useCallback(() => {
    setLocation(null);
    setError(false);
    setAddress('');
    setTimeout(() => autocompleteElement.current.focus(), 0);
  }, [setLocation]);

  useEffect(() => {
    const load = async () => {
      const loader = new Loader({
        apiKey: GMAPS_API_KEY,
        version: 'weekly',
        libraries: ['places'],
      });

      await loader.load();

      // Map
      const map = new global.google.maps.Map(mapElement.current, {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 10,
        disableDefaultUI: true,
      });

      // Marker
      const marker = new global.google.maps.Marker({
        map,
        anchorPoint: new global.google.maps.Point(0, -29),
      });

      // Autocomplete
      autocomplete.current = new global.google.maps.places.Autocomplete(
        autocompleteElement.current,
        { types: ['geocode'] },
      );
      autocomplete.current.setFields(['address_components', 'geometry', 'icon', 'name']);
      autocomplete.current.addListener('place_changed', () => {
        marker.setVisible(false);
        const place = autocomplete.current.getPlace();

        if (!place.geometry) {
          setError(true);
          return;
        }

        map.setCenter(place.geometry.location);
        map.setZoom(17);
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        const streetNumber = place.address_components.find(c => c.types.includes('street_number'));
        const route = place.address_components.find(c => c.types.includes('route'));
        const intersection = place.address_components.find(c => c.types.includes('intersection'));
        const adminArea = place.address_components.find(c => c.types.includes('administrative_area_level_1'));
        const country = place.address_components.find(c => c.types.includes('country'));

        if ((!intersection && (!streetNumber || !route)) || !adminArea || !country) {
          setError(true);
          setLocation(null);
        } else {
          const newLocation = {
            adminArea: adminArea.short_name,
            country: country.short_name,
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
          };

          if (intersection) newLocation.intersection = intersection.short_name;
          if (route) newLocation.route = route.short_name;
          if (streetNumber) newLocation.streetNumber = streetNumber.short_name;

          setLocation(newLocation);
        }
      });
    };

    if (mapElement.current) {
      load();
    }

    if (autocompleteElement.current) {
      autocompleteElement.current.focus();
    }
  }, [setLocation]);

  return (
    <div>
      <h3>{t('Event address')}</h3>

      <Input
        ref={autocompleteElement}
        type="text"
        value={address}
        onChange={addressChanged}
        style={{ display: (location === null && !error) ? 'block' : 'none' }}
      />

      {location !== null && (
        <div className="address">
          <h2>{`${location.streetNumber ? `${location.route} ${location.streetNumber}` : location.intersection}, ${location.adminArea}, ${location.country}`}</h2>
          <Button onClick={clearLocation} color="white" fontColor="black">{t('Change')}</Button>
        </div>
      )}

      <div ref={mapElement} style={{ width: '100%', height: location === null ? '0px' : '500px' }} />

      {error && (
        <div className="error">
          {t('We couldn\'t find a correct address for the location you entered. Are you sure you entered it correctly?')}
          <br />
          <br />
          <Button onClick={clearLocation}>{t('Change')}</Button>
        </div>
      )}

      {!error && location && (
        <div className="instructions">
          <Input placeholder={t('Instructions (optional)')} {...instructions} />
        </div>
      )}
    </div>
  );
};

Map.propTypes = {
  location: PropTypes.shape({
    route: PropTypes.string,
    streetNumber: PropTypes.string,
    intersection: PropTypes.string,
    adminArea: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
  }),
  setLocation: PropTypes.func.isRequired,
  instructions: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

Map.defaultProps = {
  location: null,
};

export default Map;
