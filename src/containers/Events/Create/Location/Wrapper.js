import styled from 'styled-components';

const MapWrapper = styled.div`
  .button-group {
    margin-bottom: 64px;
  }

  h3 {
    color: #666;
    margin-bottom: 8px;
    font-weight: 300;
  }

  .address {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .instructions {
    margin-top: 32px;
  }

  .error {
    font-size: 24px;
    text-align: center;
    padding: 16px;
    color: ${props => props.theme.colors.secondaryText};
  }
`;
MapWrapper.displayName = 'MapWrapper';

export default MapWrapper;
