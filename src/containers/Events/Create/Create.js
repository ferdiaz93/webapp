import React, {
  useEffect, useState, useCallback, useRef,
} from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';

import ComposerRef from 'utils/ComposerRef';
import {
  useTitle, useTranslation, useInputValue, useOpenClose,
} from 'hooks';
import * as appSelectors from 'state/app/selectors';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';
import * as eventActions from 'state/events/actions';

import Title from 'components/Main/Title';
import Subtitle from 'components/Main/Subtitle';
import Main from 'components/Forms/Main';
import Wrapper from 'components/Forms/Wrapper';
import TitleWrapper from 'components/Forms/TitleWrapper';
import { FormWrapper, ButtonsWrapper } from 'components/Forms/Wrappers';
import Button from 'components/Button';
import Modal from 'components/Modal';

import TOS from './TOS';
import Basics from './Basics';
import Flyer from './Flyer';
import Location from './Location';
import Description from './Description';
import Features from './Features';
import WaitingThread from './WaitingThread';
import locales from './i18n';

const Create = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  useTitle(t('Create Event'));

  const flyerElement = useRef(null);
  const composerId = 'eventcreate';

  const [step, setStep] = useState(0);
  const [creating, setCreating] = useState(false);
  const [openedHLUnactive, openHLUnactive, closeHLUnactive] = useOpenClose(false);

  const descriptionReady = useSelector(
    state => appSelectors.selectIsComposerReadyToSubmit(state, composerId),
  );
  const userId = useSelector(authSelectors.selectUserId);

  // Values
  const [isPublic, setIsPublic] = useState(false);
  const [showLocation, setShowLocation] = useState(true);
  const name = useInputValue('');
  const [date, setDate] = useState(moment().add(7, 'days'));
  const { change: timechange, ...time } = useInputValue('23:00');
  const price = useInputValue(500);
  const [sponsors, setSponsors] = useState([]);

  const [flyer, setFlyer] = useState(null);
  const [croppedFlyer, setCroppedFlyer] = useState(null);

  const instructions = useInputValue('');
  const url = useInputValue('');
  const [location, setLocation] = useState(null);

  const [communityId, setCommunityId] = useState(null);

  const [featureHighlight, setFeatureHighlight] = useState(false);
  const [featureAssistance, setFeatureAssistance] = useState(false);
  const [featureTransactional, setFeatureTransactional] = useState(false);
  const [featureEarly, setFeatureEarly] = useState(false);
  //

  const [eventId, setEventId] = useState(null);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [dispatch]);

  const addStep = useCallback(q => async () => {
    if (step === 2) {
      const croppedImg = await flyerElement.current.crop();
      setCroppedFlyer(croppedImg);
    }

    if (step === 5 && (featureAssistance || featureTransactional) && !featureHighlight) {
      openHLUnactive();
    } else {
      setStep(currentValue => currentValue + q);
    }
  }, [step, featureAssistance, featureTransactional, featureHighlight, openHLUnactive]);

  const create = async () => {
    try {
      if ((featureAssistance || featureTransactional) && !featureHighlight) {
        openHLUnactive();
      } else {
        setCreating(true);

        const uploadedFlyer = await dispatch(eventActions.uploadFlyer(croppedFlyer));
        const composerRef = ComposerRef.getRef(composerId);
        const description = composerRef.getValue();

        const eventTime = moment(time.value, 'HH:mm');
        const eventDate = moment(date);
        eventDate.set({
          hour: eventTime.get('hour'),
          minute: eventTime.get('minute'),
          seconds: 0,
        });

        const payload = {
          name: name.value,
          date: eventDate,
          description,
          communityId,
          instructions: instructions.value,
          cover: uploadedFlyer.filename,
          organizers: [{ userId }],
          price: price.value,
          isPublic,
          showLocation,
          features: {},
          sponsors,
        };

        if (location) {
          payload.address = {
            streetNumber: location.streetNumber,
            route: location.route,
            adminArea: location.adminArea,
            country: location.country,
          };
          payload.coordinates = [location.lat, location.lng];
        } else if (url.value) {
          payload.url = url.value;
        }

        if (featureHighlight) {
          payload.features.highlight = true;
        }
        if (featureAssistance) {
          payload.features.assistance = true;
        }
        if (featureTransactional) {
          payload.features.transactional = true;
        }
        if (featureEarly) {
          payload.features.earlyTickets = true;
        }

        const event = await dispatch(eventActions.create(payload));

        setEventId(event.id);
        setStep(currentValue => currentValue + 1);
      }
    } catch (error) {
      setCreating(false);
      dispatch(appActions.addError(error));
    }
  };

  const nextDisabled = (
    (step === 1 && name.value.length === 0)
    || (step === 2 && !flyer)
    || (step === 3 && !location && !url.value)
    || (step === 4 && (!communityId || !descriptionReady))
  );

  return (
    <>
      <Main hasTitle>
        <Wrapper>
          <TitleWrapper>
            <Title>{t('Create Event')}</Title>
            {step < 5 && <Subtitle>{t('Step {{step}}/{{total}}', { step: step + 1, total: 5 })}</Subtitle>}
          </TitleWrapper>

          <FormWrapper>
            <div>
              {step === 0 && (
                <TOS />
              )}

              {step === 1 && (
                <Basics
                  isPublic={isPublic}
                  setIsPublic={setIsPublic}
                  showLocation={showLocation}
                  setShowLocation={setShowLocation}
                  date={date}
                  setDate={setDate}
                  name={name}
                  time={time}
                  price={price}
                  sponsors={sponsors}
                  setSponsors={setSponsors}
                />
              )}

              {step === 2 && (
                <Flyer
                  ref={flyerElement}
                  flyer={flyer}
                  setFlyer={setFlyer}
                />
              )}

              {step === 3 && (
                <Location
                  location={location}
                  setLocation={setLocation}
                  url={url}
                  instructions={instructions}
                />
              )}

              {step === 4 && (
                <Description
                  composerId={composerId}
                  communityId={communityId}
                  setCommunityId={setCommunityId}
                />
              )}

              {step === 5 && (
                <Features
                  price={price.value}
                  highlight={featureHighlight}
                  setHighlight={setFeatureHighlight}
                  assistance={featureAssistance}
                  setAssistance={setFeatureAssistance}
                  transactional={featureTransactional}
                  setTransactional={setFeatureTransactional}
                  early={featureEarly}
                  setEarly={setFeatureEarly}
                />
              )}

              {step === 6 && (
                <WaitingThread eventId={eventId} />
              )}
            </div>

            {step < 6 && (
              <ButtonsWrapper style={{ marginTop: '40px' }}>
                <Button className="empty" to="/events">{t('global:Cancel')}</Button>

                {step > 0 && (
                  <Button className="empty" onClick={addStep(-1)}>{t('Previous')}</Button>
                )}

                {step === 0 && (
                  <Button onClick={addStep(1)} disabled={nextDisabled}>{t('global:Accept')}</Button>
                )}

                {step > 0 && step < 5 && (
                  <Button onClick={addStep(1)} disabled={nextDisabled}>{t('Next')}</Button>
                )}

                {step === 5 && (
                  <Button onClick={create} disabled={nextDisabled} loading={creating}>{t('Accept and Create')}</Button>
                )}
              </ButtonsWrapper>
            )}
          </FormWrapper>
        </Wrapper>
      </Main>


      {/* Modals */}
      {openedHLUnactive && (
        <Modal
          title={t('Premium options')}
          onClose={closeHLUnactive}
        >
          {t('Highlight option is required in order to activate any other premium option.')}
        </Modal>
      )}
    </>
  );
};

export default Create;
