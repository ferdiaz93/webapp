import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as threadSelectors from 'state/threads/selectors';

import EmptyState from 'components/EmptyState';
import Spinner from 'components/Spinner';
import Button from 'components/Button';

import locales from '../i18n';

const SpinnerWrapper = styled.div`
  margin: 32px auto;
  text-align: center;

  > div {
    width: 25px;
  }
`;
SpinnerWrapper.displayName = 'SpinnerWrapper';

const ButtonsWrapper = styled.div`
  margin-top: 16px;

  > div {
    margin: 32px 0 0;
  }
`;
ButtonsWrapper.displayName = 'ButtonsWrapper';

const WaitingThread = ({ eventId }) => {
  const { t } = useTranslation(locales);
  const history = useHistory();

  const preferenceInitPoint = useSelector(
    state => eventSelectors.selectPreferenceInitPoint(state, eventId),
  );
  const eventUrl = useSelector(threadSelectors.getThreadUrlByEventId(eventId));

  useEffect(() => {
    if (eventUrl && !preferenceInitPoint) history.push(eventUrl);
  }, [eventUrl, preferenceInitPoint, history]);

  if (eventUrl && preferenceInitPoint) {
    return (
      <EmptyState title={t('Your Event has been created!')} subtitle={t('To activate your premium features, you must complete the payment first.')}>
        <ButtonsWrapper>
          <div><Button to={preferenceInitPoint}>{t('Complete payment')}</Button></div>
          <div><Button className="empty" to={eventUrl}>{t('Skip payment for the moment')}</Button></div>
        </ButtonsWrapper>
      </EmptyState>
    );
  }

  return (
    <EmptyState title={t('Your Event has been created!')} subtitle={t('The associated Thread is being created. You will be redirected in a moment.')}>
      <SpinnerWrapper>
        <Spinner color="#666" />
      </SpinnerWrapper>
    </EmptyState>
  );
};

WaitingThread.propTypes = {
  eventId: PropTypes.string.isRequired,
};

WaitingThread.defaultProps = {
};

export default WaitingThread;
