import React from 'react';

import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import Layout from 'components/Layout';
import Sidebar from 'containers/Sidebar';

import Content from './Content';

const Landing = () => (
  <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
    <FlexWrapper canOverflow>
      <FlexInnerWrapper>
        <FlexContainer framed>
          <Content />
        </FlexContainer>
      </FlexInnerWrapper>
    </FlexWrapper>

    <Sidebar />
  </Layout>
);

Landing.propTypes = {
};

Landing.defaultProps = {
};

export default Landing;
