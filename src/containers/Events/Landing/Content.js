import React, { useState, useEffect, useContext } from 'react';
import styled, { ThemeContext } from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as eventSelectors from 'state/events/selectors';
import * as appSelectors from 'state/app/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import ErrorState from 'components/ErrorState';
import Spinner from 'components/Spinner';
import SpinnerWrapper from 'components/Spinner/Wrapper';
import Plus from 'components/Icons/Plus';
import FloatingButton from 'components/Button/Floating';

import Event from './Event';

const Wrapper = styled.div`
  position: relative;

  ${FloatingButton} {
    position: fixed;
    margin-left: 580px;
    right: auto;

    @media(max-width: 767px) {
      right: 16px;
    }
  }

  .grid {
    display: grid;
    grid-template-columns: repeat(3, 1fr);

    > div {
      box-sizing: border-box;
      padding: 4px;

      img {
        width: 100%;
        height: auto;
      }
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const Content = () => {
  const dispatch = useDispatch();
  const theme = useContext(ThemeContext);

  const ids = useSelector(eventSelectors.selectAllIdsWithAssistance, shallowEqual);
  const uiLeftColumn = useSelector(appSelectors.selectIsUiLeftColumnActive);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const load = async () => {
      try {
        setLoading(true);
        await dispatch(eventActions.fetchAll());
      } catch (err) {
        setError(err);
      }

      setLoading(false);
    };

    load();
  }, [dispatch]);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [uiLeftColumn, dispatch]);

  if (loading) return <SpinnerWrapper><Spinner color="#666" /></SpinnerWrapper>;
  if (error) return <ErrorState />;

  return (
    <Wrapper>
      <div className="grid">
        {ids.map(id => <Event key={`event-${id}`} eventId={id} />)}
      </div>

      <FloatingButton
        to="/events/create"
        color={theme.colors.main}
        distance="16px"
        negative
      >
        <Plus outline color="white" />
      </FloatingButton>
    </Wrapper>
  );
};

Content.propTypes = {
};

Content.defaultProps = {
};

export default Content;
