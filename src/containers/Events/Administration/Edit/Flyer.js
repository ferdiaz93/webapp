import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import loadImage from 'blueimp-load-image';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';

import Button from 'components/Button';
import FileSelector from 'components/FileSelector';

import FlyerModal from './FlyerModal';
import locales from '../i18n';

const FlyerContainer = styled.div`
  margin: 0 auto 64px;

  img {
    width: 187px;
    height: 250px;
  }
`;
FlyerContainer.displayName = 'FlyerContainer';

const Flyer = ({ eventId }) => {
  const { t } = useTranslation(locales);

  const flyer = useSelector(state => eventSelectors.selectFlyer(state, eventId));
  const name = useSelector(state => eventSelectors.selectName(state, eventId));

  const [newFlyer, setFlyer] = useState(null);
  const closeFlyerModal = useCallback(() => {
    setFlyer(null);
  }, []);

  const onFlyerChange = useCallback((e) => {
    loadImage(
      e.target.files[0],
      (img) => {
        const base64data = img.toDataURL('image/jpeg');
        setFlyer({
          src: base64data,
          img,
        });
      },
      {
        maxWidth: 600, maxHeight: 700, orientation: true, canvas: true,
      },
    );
  }, []);

  return (
    <>
      <FlyerContainer>
        <img src={flyer} alt={name} />

        <div className="edit-button">
          <FileSelector onChange={onFlyerChange}>
            <Button color="white" fontColor="black">{t('global:Change')}</Button>
          </FileSelector>
        </div>
      </FlyerContainer>

      {/* Modals */}
      {newFlyer && <FlyerModal eventId={eventId} close={closeFlyerModal} flyer={newFlyer} />}
    </>
  );
};

Flyer.propTypes = {
  eventId: PropTypes.string.isRequired,
};

Flyer.defaultProps = {
};

export default Flyer;
