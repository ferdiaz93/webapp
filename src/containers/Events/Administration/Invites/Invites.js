import React, { useState, useCallback, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useOpenClose, useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import SearchUsersModal from 'components/SearchUsersModal';
import Loading from 'components/Loading';
import EmptyState from 'components/EmptyState';

import Wrapper from '../Assistance/Wrapper';
import User from '../Assistance/User';
import locales from '../i18n';

const Invites = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const { eventId } = useParams();

  const invites = useSelector(state => eventSelectors.selectInvites(state, eventId), shallowEqual);
  const hasAccess = useSelector(
    state => eventSelectors.hasAssistanceFeatureEnabled(state, eventId),
  );

  const [isFetching, setIsFetching] = useState(true);
  const [addLoading, setAddLoading] = useState(false);
  const [removing, setRemoving] = useState([]);
  const [isOpenSearch, openSearch, closeSearch] = useOpenClose(false);

  useEffect(() => {
    const load = async () => {
      await dispatch(eventActions.loadInvites(eventId));
      setIsFetching(false);
    };

    load();
  }, [dispatch, eventId]);

  const add = useCallback(async (userId) => {
    try {
      setAddLoading(true);
      await dispatch(eventActions.addInvite(eventId, userId));
      dispatch(appActions.addToast(t('Invite sent')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    closeSearch();
    setAddLoading(false);
  }, [dispatch, t, eventId, closeSearch]);

  const remove = useCallback(inviteId => async () => {
    try {
      setRemoving(cv => [...cv, inviteId]);
      await dispatch(eventActions.removeInvite(eventId, inviteId));
      dispatch(appActions.addToast(t('Invite removed')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setRemoving(cv => cv.filter(v => v !== inviteId));
  }, [dispatch, t, eventId]);

  if (!hasAccess) return <EmptyState title={t('This Event doesn\'t have assistance tracking enabled')} />;

  return (
    <>
      <Wrapper>
        <Button onClick={openSearch}>{t('Send invite')}</Button>

        <br />
        <br />

        {invites.length > 0 ? (
          <table>
            <thead>
              <tr>
                <th>{t('User')}</th>
                <th>{t('Status')}</th>
                <th>{t('Action')}</th>
              </tr>
            </thead>
            <tbody>
              {invites.map(invite => (
                <tr key={`invite-${eventId}-${invite.userId}`}>
                  <td>
                    <User userId={invite.userId} />
                  </td>
                  <td>{invite.status}</td>
                  <td>
                    {invite.status === 'PENDING' && (
                      <Button className="empty" onClick={remove(invite.id)} loading={removing.includes(invite.userId)}>
                        {t('global:Remove')}
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            {isFetching && <Loading />}
          </>
        )}
      </Wrapper>

      {/* Modals */}
      {isOpenSearch && (
        <SearchUsersModal
          title={t('Send invite')}
          close={closeSearch}
          confirmLoading={addLoading}
          confirm={add}
          bypassBlocks
        />
      )}
    </>
  );
};

Invites.propTypes = {
};

Invites.defaultProps = {
};

export default Invites;
