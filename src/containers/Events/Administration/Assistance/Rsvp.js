import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';

import User from './User';
import locales from '../i18n';

const Rsvp = ({ rsvp, filter }) => {
  const { t } = useTranslation(locales);
  const { id: rsvpId, event: eventId, userId } = rsvp;

  const username = useSelector(userSelectors.getUsername(userId));
  const displayname = useSelector(userSelectors.getDisplayName(userId));

  const dispatch = useDispatch();
  const [loading, setLoading] = useState([]);

  const assist = useCallback(async () => {
    try {
      setLoading(cv => [...cv, userId]);

      await dispatch(eventActions.adminAssist(eventId, userId));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(cv => cv.filter(id => id !== userId));
  }, [dispatch, eventId, userId]);

  const unassist = useCallback(async () => {
    try {
      setLoading(cv => [...cv, userId]);

      await dispatch(eventActions.adminUnassist(eventId, userId));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(cv => cv.filter(id => id !== userId));
  }, [dispatch, eventId, userId]);

  if (!username.includes(filter) && !displayname.includes(filter)) return null;

  return (
    <tr key={`assistance-${rsvpId}`}>
      <td>{rsvp.verificationCode}</td>
      <td className={!rsvp.assistedAt ? 'unassisted' : ''}>
        <User userId={rsvp.userId} />
      </td>
      <td>
        {!rsvp.assistedAt ? (
          <Button className="empty" loading={loading.includes(rsvp.userId)} onClick={assist}>{t('Add assistance')}</Button>
        ) : (
          <Button className="empty" loading={loading.includes(rsvp.userId)} onClick={unassist}>{t('Remove assistance')}</Button>
        )}
      </td>
    </tr>
  );
};

Rsvp.propTypes = {
  rsvp: PropTypes.shape({
    id: PropTypes.string.isRequired,
    userId: PropTypes.number.isRequired,
    event: PropTypes.string.isRequired,
    assistedAt: PropTypes.string,
    verificationCode: PropTypes.number.isRequired,
  }).isRequired,
  filter: PropTypes.string.isRequired,
};

Rsvp.defaultProps = {
};

export default Rsvp;
