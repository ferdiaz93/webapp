import React, { useState, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useOpenClose, useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import SearchUsersModal from 'components/SearchUsersModal';

import Wrapper from '../Assistance/Wrapper';
import User from '../Assistance/User';
import locales from '../i18n';

const Bans = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const { eventId } = useParams();

  const bans = useSelector(state => eventSelectors.selectBans(state, eventId), shallowEqual);

  const [addLoading, setAddLoading] = useState(false);
  const [removing, setRemoving] = useState([]);
  const [isopenSearch, openSearch, closeSearch] = useOpenClose(false);

  const add = useCallback(async (userId) => {
    try {
      setAddLoading(true);
      await dispatch(eventActions.addBan(eventId, userId));
      dispatch(appActions.addToast(t('Ban added')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    closeSearch();
    setAddLoading(false);
  }, [dispatch, t, eventId, closeSearch]);

  const remove = useCallback(userId => async () => {
    try {
      setRemoving(cv => [...cv, userId]);
      await dispatch(eventActions.removeBan(eventId, userId));
      dispatch(appActions.addToast(t('Ban removed')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setRemoving(cv => cv.filter(v => v !== userId));
  }, [dispatch, t, eventId]);

  return (
    <>
      <Wrapper>
        <Button onClick={openSearch}>{t('Add ban')}</Button>

        <br />
        <br />

        {bans.length > 0 && (
          <table>
            <thead>
              <tr>
                <th>{t('User')}</th>
                <th>{t('Action')}</th>
              </tr>
            </thead>
            <tbody>
              {bans.map(ban => (
                <tr key={`ban-${eventId}-${ban.userId}`}>
                  <td>
                    <User userId={ban.userId} />
                  </td>
                  <td>
                    <Button className="empty" onClick={remove(ban.userId)} loading={removing.includes(ban.userId)}>{t('global:Remove')}</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </Wrapper>

      {/* Modals */}
      {isopenSearch && (
        <SearchUsersModal
          title={t('Add ban')}
          close={closeSearch}
          confirmLoading={addLoading}
          confirm={add}
          bypassBlocks
        />
      )}
    </>
  );
};

Bans.propTypes = {
};

Bans.defaultProps = {
};

export default Bans;
