import React, { useMemo, useCallback } from 'react';
import { useParams, useHistory, useRouteMatch } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';

import { TabsList, TabsWrapper } from 'components/Tabs';
import PageTitle from 'components/PageTitle';

import locales from './i18n';

const Header = () => {
  const { t } = useTranslation(locales);
  const history = useHistory();
  const { eventId } = useParams();
  const match = useRouteMatch('/events/:eventId/:key');

  const isLoaded = useSelector(state => eventSelectors.isLoaded(state, eventId));
  const isOrganizer = useSelector(state => eventSelectors.isOrganizer(state, eventId));
  const name = useSelector(state => eventSelectors.selectName(state, eventId));

  const tabs = useMemo(() => ([
    { key: 'assistance', label: t('Assistance') },
    { key: 'qr', label: t('QR') },
    { key: 'edit', label: t('Edit') },
    { key: 'invites', label: t('Invites') },
    { key: 'bans', label: t('Bans') },
  ]), [t]);

  const onTabChange = useCallback((index) => {
    history.replace(`/events/${match.params.eventId}/${tabs[index].key}`);
  }, [history, tabs, match]);

  if (!match || !isLoaded || !isOrganizer) return null;

  const tabIndex = tabs.findIndex(tab => tab.key === match.params.key);

  return (
    <>
      <PageTitle>{name}</PageTitle>

      <TabsWrapper sticky>
        <TabsList data={tabs} selected={tabIndex} onSelect={onTabChange} />
      </TabsWrapper>
    </>
  );
};

Header.propTypes = {
};

Header.defaultProps = {
};

export default Header;
