import React from 'react';
import ReactDOM from 'react-dom';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import ReactGA from 'react-ga';

import rootReducer from 'state/reducers';

import App from 'containers/App';
import ServiceWorkerWrapper from 'containers/App/ServiceWorkerWrapper';

import reportWebVitals from './reportWebVitals';
import './i18n';

if (process.env.REACT_APP_SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    // release: `webapp@${process.env.npm_package_version}`,
    integrations: [new Integrations.BrowserTracing()],
    environment: process.env.REACT_APP_SENTRY_ENV,

    tracesSampleRate: 0.1,
  });
}

const sentryReduxEnhancer = Sentry.createReduxEnhancer({});
const composeEnhancers = (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk), sentryReduxEnhancer),
);

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line global-require
  const whyDidYouRender = require('@welldone-software/why-did-you-render');
  whyDidYouRender(React, {
    trackAllPureComponents: true,
    // include: [/.*/],
    exclude: [/Boundary/, /Socket/, /Routines/, /styled/, /Styled/, /LinkAnchor/],
    logOwnerReasons: true,
    collapseGroups: true,
    trackExtraHooks: [
      // eslint-disable-next-line global-require
      [require('react-redux/lib'), 'useSelector'],
    ],
  });
}

ReactDOM.render(
  <Provider store={store}>
    <ServiceWorkerWrapper />
    <App />
  </Provider>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
function sendToGoogleAnalytics({ name, delta, id }) {
  // Assumes the global `ga()` function exists, see:
  // https://developers.google.com/analytics/devguides/collection/analyticsjs
  ReactGA.event({
    category: 'Web Vitals',
    action: name,
    // The `id` value will be unique to the current page load. When sending
    // multiple values from the same page (e.g. for CLS), Google Analytics can
    // compute a total by grouping on this ID (note: requires `eventLabel` to
    // be a dimension in your report).
    label: id,
    // Google Analytics metrics must be integers, so the value is rounded.
    // For CLS the value is first multiplied by 1000 for greater precision
    // (note: increase the multiplier for greater precision if needed).
    value: Math.round(name === 'CLS' ? delta * 1000 : delta),
    // Use a non-interaction event to avoid affecting bounce rate.
    nonInteraction: true,
    // Use `sendBeacon()` if the browser supports it.
    transport: 'beacon',
  });
}

reportWebVitals(sendToGoogleAnalytics);
