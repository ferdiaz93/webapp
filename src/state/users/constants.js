export const ADD = 'mazmo/users/ADD';

export const ADD_FAILED_USERNAME = 'mazmo/users/ADD_FAILED_USERNAME';

export const ONLINE_LIST = 'mazmo/users/ONLINE_LIST';
export const ONLINE = 'mazmo/users/ONLINE';
export const OFFLINE = 'mazmo/users/OFFLINE';

export const FIRE_ADDED = 'mazmo/users/FIRE_ADDED';
export const FIRE_REMOVED = 'mazmo/users/FIRE_REMOVED';
export const FIRE_ACKNOWLEDGED = 'mazmo/users/FIRE_ACKNOWLEDGED';

export const SUGGESTIONS_LOAD = 'mazmo/users/SUGGESTIONS_LOAD';
export const SUGGESTIONS_CLEAR = 'mazmo/users/SUGGESTIONS_CLEAR';
