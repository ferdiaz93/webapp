export const LOAD_PERIOD = 'mazmo/bank/LOAD_PERIOD';
export const LOAD_COUPONS = 'mazmo/bank/LOAD_COUPONS';
export const UPDATE_COUPON = 'mazmo/bank/UPDATE_COUPON';

export const WIPE = 'mazmo/bank/WIPE';
