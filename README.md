# MAZMO webapp for mazmo.net

[Mazmo](https://mazmo.net) es una aplicación de código abierto. No sólo cualquiera es bienvenide a contribuir y discutir sobre el código, sino que los alentamos a hacerlo.

En su concepción y visión, Mazmo es una herramienta para que cada une desarrolle su sexualidad libremente, y con ese objetivo está pensada cada sección y cada decisión que se toma desde el producto.

Y por ese motivo, para ser fiel a esos conceptos, es que el frontend de Mazmo es 100% abierto. La mejor forma de crear una comunidad libre, es darle la libertad a sus miembros para ser parte de la construcción.

**¡Recuerda que si colaboras con el código de Mazmo tendrás un [badge especial en tu perfil](https://nerds.mazmo.net/contributor-badge)!**


## Manos a la obra

Primero, lo fundamental: el link al [repositorio del código](https://gitlab.com/mazmo/webapp).


## El Stack

La aplicación está construida sobre las siguientes herramientas:

* [React](https://reactjs.org) para construir la UI
* [Redux](https://redux.js.org/) para manejar el estado global de la aplicación
* [Styled components](https://styled-components.com/) para manejar estilos y visualización de los componentes


## Estructura

La aplicación se divide principalmente en tres grandes carpetas.

En la carpeta `state` se encuentra todo lo relacionado al estado global. Podrás encontrar que está dividido en subcarpetas por cada entidad del sitio. Cada una, conteniendo archivos para el manejo de _actions_, _reducers_ y _selectors_.

En `components` viven todos los componentes que son compartidos a lo largo de la aplicación. Aquí encontrarás botones, formularios, headings, etcétera.

Y por último, en `containers` están los componentes de las secciones del sitio, con su lógica y sus componentes internos. Fuertemente conectados al estado, y por lo general con un manejo de lógica superior a los que se encuentran en `components`.


## Cómo correr localmente la aplicación

```
git clone https://gitlab.com/mazmo/webapp.git
cd webapp
npm install
npm start
```

La versión de node requerida está especificada en el `package.json`. Al momento de escribir este artículo, era la versión 12.17.0

```warning
¡IMPORTANTE!

Cuando tengas funcionando la aplicación localmente, ten en cuenta que si bien puedes modificar y jugar con el código, todas las interacciones con el backend serán contra el servidor de **producción**. Es decir, puedes localmente chatear, comentar y crear contenido en general, y éste se verá reflejado en producción.
```


## Links útiles

* El [repositorio de Mazmo](https://gitlab.com/mazmo/webapp) en Gitlab
* Lista de [bugs conocidos](https://gitlab.com/mazmo/webapp/-/boards/900538?scope=all&utf8=%E2%9C%93&label_name[]=Bug)
* Canal de [chat para devs](https://mazmo.net/chat/channels/5dc991db1dcf380025464021)
* [Blog para nerds](https://nerds.mazmo.net)
